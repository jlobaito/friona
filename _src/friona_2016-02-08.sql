# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 162.209.94.254 (MySQL 5.5.44-MariaDB-log)
# Database: friona
# Generation Time: 2016-02-08 09:23:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assetfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetfiles`;

CREATE TABLE `assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` int(11) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `assetfiles_sourceId_fk` (`sourceId`),
  KEY `assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assetfiles` WRITE;
/*!40000 ALTER TABLE `assetfiles` DISABLE KEYS */;

INSERT INTO `assetfiles` (`id`, `sourceId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(74,4,5,'FI-Ethics-Letter-January-2016-NewLogo-pd.pdf','pdf',NULL,NULL,29980,'2016-02-01 16:16:18','2016-01-27 15:43:49','2016-02-05 20:21:53','bec5dfe5-357d-4852-98b2-671550556417'),
	(82,1,1,'FI-Employment-Application.pdf','pdf',NULL,NULL,121572,'2016-02-01 16:16:18','2016-01-27 16:39:39','2016-02-05 20:21:51','63a9fca3-026f-4c11-9b73-d8c82923e081'),
	(89,3,3,'pasture-cattle-portrait.jpg','image',1000,667,146363,'2016-02-01 16:16:18','2016-01-27 17:59:39','2016-02-05 20:21:52','dd83e3fb-b51b-4b3c-bc95-b8a4e848af5b'),
	(91,3,3,'friona-cowboy.jpg','image',1188,792,179178,'2016-02-01 16:16:18','2016-01-27 20:33:02','2016-02-05 20:21:51','2af0a40e-24af-4b14-8930-8c2db6ca4a39'),
	(93,3,3,'randall-cattle-with-mill.jpg','image',1301,867,378804,'2016-02-01 16:16:19','2016-01-27 20:33:29','2016-02-05 20:21:52','8f27181a-6ca4-4fe3-aa0c-1ce14bdce0b3'),
	(135,4,5,'BQA-Compliance-Position-Statement.pdf','pdf',NULL,NULL,69324,'2016-02-05 15:32:18','2016-02-05 15:32:18','2016-02-05 20:21:52','a05029a5-3acf-4aa7-93b8-62ccd9f9aa26'),
	(139,3,3,'friona-manager.jpg','image',1313,876,228066,'2016-02-05 18:50:31','2016-02-05 18:50:31','2016-02-05 20:21:52','05f38b4f-6f09-4228-beed-7e58a5564811'),
	(140,3,3,'jared-randel-manager.jpg','image',1313,876,178959,'2016-02-05 20:21:16','2016-02-05 20:21:16','2016-02-05 20:21:52','f7865a1e-0f83-43d9-b13b-2626f7318349'),
	(141,3,3,'ronnie-gonzales-manager.jpg','image',1142,762,72808,'2016-02-05 20:21:27','2016-02-05 20:21:27','2016-02-05 20:21:52','8a46ed13-4ca8-4874-b79c-80e10887f054'),
	(142,3,3,'cattle-at-feeder.jpg','image',1000,1500,284387,'2016-02-05 20:39:47','2016-02-05 20:39:47','2016-02-05 20:39:47','d6a61772-9942-4035-a2d6-c9e1b721c873'),
	(143,3,3,'feedyard-1.jpg','image',1313,876,232176,'2016-02-05 20:39:48','2016-02-05 20:39:48','2016-02-05 20:39:48','285a33fb-18aa-4fbb-8909-d55d2bba90b1'),
	(144,3,3,'feedyard-2.jpg','image',1313,876,298700,'2016-02-05 20:39:48','2016-02-05 20:39:48','2016-02-05 20:39:48','a70628f2-991d-4223-9a15-d57f09c8ed5c'),
	(145,3,3,'fi-pasture-1.jpg','image',1175,783,283810,'2016-02-05 20:39:48','2016-02-05 20:39:48','2016-02-05 20:39:48','b3590cdb-a11c-42b7-9fc7-90fc06082f13'),
	(146,3,3,'fi-pasture-3.jpg','image',1158,772,248497,'2016-02-05 20:39:49','2016-02-05 20:39:49','2016-02-05 20:39:49','4836fb02-6462-47c4-a56a-293b2ca96152'),
	(147,3,3,'fibrand.jpg','image',1000,667,185138,'2016-02-05 20:39:49','2016-02-05 20:39:49','2016-02-05 20:39:49','afe99c1d-7e73-4452-80dd-0ff324a8841f'),
	(148,3,3,'friona-1.jpg','image',1313,876,218141,'2016-02-05 20:39:49','2016-02-05 20:39:49','2016-02-05 20:39:49','76cd1aa0-5b0a-4bd9-a609-fab780fe0f58'),
	(149,3,3,'friona-2.jpg','image',1313,876,180682,'2016-02-05 20:39:50','2016-02-05 20:39:50','2016-02-05 20:39:50','ffee0ac4-86dd-490c-a5ef-e490b464388c'),
	(150,3,3,'friona-3.jpg','image',1313,876,246921,'2016-02-05 20:39:50','2016-02-05 20:39:50','2016-02-05 20:39:50','d12c89e9-ac9b-4654-85af-1e15142438ed'),
	(151,3,3,'friona-4.jpg','image',1313,876,319345,'2016-02-05 20:39:50','2016-02-05 20:39:50','2016-02-05 20:39:50','4504dd20-90f3-4852-9637-ea6b6cdbb5c4'),
	(152,3,3,'friona-5.jpg','image',1313,876,315495,'2016-02-05 20:39:50','2016-02-05 20:39:51','2016-02-05 20:39:51','54a3002c-1440-40bc-a78d-d8ceb485dccf'),
	(153,3,3,'friona-assistant-manager.jpg','image',1313,876,122241,'2016-02-05 20:39:51','2016-02-05 20:39:51','2016-02-05 20:39:51','58e7ed7a-33fc-43cd-9334-5269ef99de90'),
	(154,3,3,'friona-cowboy_160205_143951.jpg','image',1188,792,179328,'2016-02-05 20:39:51','2016-02-05 20:39:51','2016-02-05 20:39:51','1c861509-444f-4652-bc28-b3b254d160c2'),
	(155,3,3,'friona-day.jpg','image',1175,783,231408,'2016-02-05 20:39:51','2016-02-05 20:39:51','2016-02-05 20:39:51','107fd2ba-3006-44a2-a4e0-3c239fc69a07'),
	(156,3,3,'friona-manager_160205_143952.jpg','image',1313,876,228788,'2016-02-05 20:39:52','2016-02-05 20:39:52','2016-02-05 20:39:52','76b035e7-ae65-40f3-8cf2-19ad7431d378'),
	(157,3,3,'friona-office-manager.jpg','image',1313,876,224378,'2016-02-05 20:39:52','2016-02-05 20:39:52','2016-02-05 20:39:52','beba9c08-f494-4849-9296-90dd62febba5'),
	(158,3,3,'friona1.jpg','image',1000,667,138658,'2016-02-05 20:39:52','2016-02-05 20:39:52','2016-02-05 20:39:52','2e84942e-3ef9-4956-8ecd-eb48ca780a8d'),
	(159,3,3,'jared-randel-manager_160205_143953.jpg','image',1313,876,178959,'2016-02-05 20:39:53','2016-02-05 20:39:53','2016-02-05 20:39:53','580710aa-1f4f-47cd-94ae-1c34d28ba817'),
	(160,3,3,'littlefield-assistant-manager.jpg','image',1313,876,182422,'2016-02-05 20:39:53','2016-02-05 20:39:53','2016-02-05 20:39:53','1dbb3393-5f40-47f5-8da1-20b9d3a49522'),
	(161,3,3,'littlefield-office-manager.jpg','image',876,1313,279806,'2016-02-05 20:39:53','2016-02-05 20:39:53','2016-02-05 20:39:53','926c14fb-4480-43fa-9c8d-1c1b090d46a4'),
	(162,3,3,'pasture-cattle-portrait_160205_143954.jpg','image',1000,667,146836,'2016-02-05 20:39:54','2016-02-05 20:39:54','2016-02-05 20:39:54','d71c043b-c1d5-491f-9527-0d1923f09bc8'),
	(163,3,3,'pasture-heifer.jpg','image',1175,783,219320,'2016-02-05 20:39:54','2016-02-05 20:39:54','2016-02-05 20:39:54','77359f60-7de7-488c-9f1e-7432d3979f1b'),
	(164,3,3,'randal-ass-manager.jpg','image',876,1313,185370,'2016-02-05 20:39:54','2016-02-05 20:39:54','2016-02-05 20:39:54','ae3a0c54-d78a-488b-adfc-258443b744e8'),
	(165,3,3,'randall-cattle-1.jpg','image',1313,876,262929,'2016-02-05 20:39:54','2016-02-05 20:39:55','2016-02-05 20:39:55','f9d92acc-0290-46da-b928-c6b22d91f63d'),
	(166,3,3,'randall-cattle-2.jpg','image',1313,876,314851,'2016-02-05 20:39:55','2016-02-05 20:39:55','2016-02-05 20:39:55','1bcccd0f-44a3-4319-8a00-f94435ddcae0'),
	(167,3,3,'randall-cattle-with-mill_160205_143955.jpg','image',1301,867,379114,'2016-02-05 20:39:55','2016-02-05 20:39:55','2016-02-05 20:39:55','14a5f50f-a51b-428f-a7f2-fe656bec4d4d'),
	(168,3,3,'randall-cowboy.jpg','image',1000,667,106687,'2016-02-05 20:39:55','2016-02-05 20:39:55','2016-02-05 20:39:55','6bd880b3-4fa4-4f8b-a729-a22010d1f5a9'),
	(169,3,3,'randall-office-manager.jpg','image',851,1277,174489,'2016-02-05 20:39:56','2016-02-05 20:39:56','2016-02-05 20:39:56','03dd501b-35a7-44b4-a35a-8c3f156143ae'),
	(170,3,3,'randall-yard-cattle.jpg','image',1313,876,326274,'2016-02-05 20:39:56','2016-02-05 20:39:56','2016-02-05 20:39:56','21d2917c-c606-496d-9592-40f379117ceb'),
	(171,3,3,'ronnie-gonzales-manager_160205_143956.jpg','image',1142,762,72808,'2016-02-05 20:39:56','2016-02-05 20:39:56','2016-02-05 20:39:56','fc2b6527-08a2-486a-96b7-f1ebaa5d00ff'),
	(172,3,3,'swisher-ass-manager.jpg','image',1313,876,168460,'2016-02-05 20:39:57','2016-02-05 20:39:57','2016-02-05 20:39:57','e3e41095-e224-4007-a34b-a8f06c6dadca'),
	(173,3,3,'swisher-manager.jpg','image',1313,876,89341,'2016-02-05 20:39:57','2016-02-05 20:39:57','2016-02-05 20:39:57','1d803c04-1cf1-4840-af68-47ec1ad4db27'),
	(174,3,3,'swisher-office-manager.jpg','image',1000,667,137131,'2016-02-05 20:39:57','2016-02-05 20:39:57','2016-02-05 20:39:57','6eb6d62e-e07a-4969-b792-1fb0d00d1226'),
	(175,3,3,'trucker.jpg','image',1313,876,318730,'2016-02-05 20:39:57','2016-02-05 20:39:58','2016-02-05 20:39:58','7826e4cc-188c-4304-85f3-7000790aa6a2');

/*!40000 ALTER TABLE `assetfiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assetfolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetfolders`;

CREATE TABLE `assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `assetfolders_parentId_fk` (`parentId`),
  KEY `assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assetfolders` WRITE;
/*!40000 ALTER TABLE `assetfolders` DISABLE KEYS */;

INSERT INTO `assetfolders` (`id`, `parentId`, `sourceId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,1,'Careers','','2015-11-23 14:57:20','2015-11-23 14:57:20','97063f24-f32d-416e-90b7-ca29f9c38f7b'),
	(2,NULL,2,'News','','2015-12-09 16:52:51','2015-12-09 16:52:51','2ca89c96-5335-427e-a1e8-194216cd3119'),
	(3,NULL,3,'Images','','2015-12-09 16:59:39','2015-12-09 16:59:39','55d5e429-db28-45ab-b7f5-062280e27ba7'),
	(4,3,3,'news','news/','2016-01-25 17:38:00','2016-01-25 17:38:00','3259d562-19e1-4fac-b8ea-5af2aa0d5336'),
	(5,NULL,4,'PDFs','','2016-01-27 15:49:08','2016-01-27 15:49:08','6337263a-d309-4906-a19a-e9b90773a2ff');

/*!40000 ALTER TABLE `assetfolders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetindexdata`;

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assetsources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetsources`;

CREATE TABLE `assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `assetsources_handle_unq_idx` (`handle`),
  KEY `assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assetsources` WRITE;
/*!40000 ALTER TABLE `assetsources` DISABLE KEYS */;

INSERT INTO `assetsources` (`id`, `name`, `handle`, `type`, `settings`, `sortOrder`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Careers','careers','Local','{\"path\":\"{basePath}media\\/documents\\/careers\\/\",\"url\":\"\\/media\\/documents\\/careers\\/\"}',1,26,'2015-11-23 14:57:20','2015-11-23 14:57:20','c19db933-2ca2-4f0e-883d-a12bd39d377c'),
	(2,'News','news','Local','{\"path\":\"{basePath}media\\/images\\/news\\/\",\"url\":\"\\/media\\/images\\/news\\/\"}',2,64,'2015-12-09 16:52:51','2015-12-09 16:59:21','c1d4c883-4fd0-49af-ae3e-7790de88161d'),
	(3,'Images','images','Local','{\"path\":\"{basePath}media\\/images\\/\",\"url\":\"\\/media\\/images\\/\"}',3,65,'2015-12-09 16:59:39','2015-12-09 16:59:39','d4060460-bb73-411f-b125-01dea056d3e9'),
	(4,'PDFs','pdfs','Local','{\"path\":\"{basePath}media\\/pdfs\\/\",\"url\":\"\\/media\\/pdfs\\/\"}',4,159,'2016-01-27 15:49:08','2016-01-27 15:52:23','ec114834-1638-438d-8c5d-9eae88ee89d9');

/*!40000 ALTER TABLE `assetsources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assettransformindex`;

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assettransformindex` WRITE;
/*!40000 ALTER TABLE `assettransformindex` DISABLE KEYS */;

INSERT INTO `assettransformindex` (`id`, `fileId`, `filename`, `format`, `location`, `sourceId`, `fileExists`, `inProgress`, `dateIndexed`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,89,'pasture-cattle-portrait.jpg',NULL,'_newsFeatureImage',3,1,0,'2016-02-03 19:13:37','2016-02-03 19:13:37','2016-02-03 19:13:38','c8b83076-beb5-4a04-992f-409a24e9429a'),
	(2,91,'friona-cowboy.jpg',NULL,'_newsFeatureImage',3,1,0,'2016-02-03 19:13:37','2016-02-03 19:13:37','2016-02-03 19:13:38','ea1c7a18-b20b-4360-b28e-d89d8335c97d'),
	(3,93,'randall-cattle-with-mill.jpg',NULL,'_newsFeatureImage',3,1,0,'2016-02-03 19:13:37','2016-02-03 19:13:37','2016-02-03 19:13:38','b5cdd211-87b5-4c5e-9023-69774f5ef59c'),
	(4,89,'pasture-cattle-portrait.jpg',NULL,'_newsThumbnailImage',3,1,0,'2016-02-03 19:13:37','2016-02-03 19:13:37','2016-02-03 19:13:38','d3b7996b-973c-44fb-ad62-d675c8c6a949'),
	(5,91,'friona-cowboy.jpg',NULL,'_newsThumbnailImage',3,1,0,'2016-02-03 19:13:37','2016-02-03 19:13:37','2016-02-03 19:13:38','4fa24a94-030c-4848-bb1f-1b5829264b2b'),
	(6,93,'randall-cattle-with-mill.jpg',NULL,'_newsThumbnailImage',3,1,0,'2016-02-03 19:13:37','2016-02-03 19:13:37','2016-02-03 19:13:38','adeaaf5e-45ea-43c8-9d80-d798106d859e'),
	(9,93,'randall-cattle-with-mill.jpg',NULL,'_locationContact',3,1,0,'2016-02-04 17:31:19','2016-02-04 17:31:19','2016-02-04 17:31:19','7249898f-8e05-4784-8c18-721168da2153'),
	(10,139,'friona-manager.jpg',NULL,'_locationContact',3,1,0,'2016-02-05 22:44:36','2016-02-05 22:44:36','2016-02-05 22:44:37','fe3386ae-bef7-424b-b113-0897aa7d0a0d'),
	(11,153,'friona-assistant-manager.jpg',NULL,'_locationContact',3,1,0,'2016-02-05 22:44:36','2016-02-05 22:44:37','2016-02-05 22:44:37','39308683-9ab6-466a-9ac1-da075cdca580'),
	(12,157,'friona-office-manager.jpg',NULL,'_locationContact',3,1,0,'2016-02-05 22:44:37','2016-02-05 22:44:37','2016-02-05 22:44:37','e7e078b4-8a6a-4acc-9bfb-4adc9183e998');

/*!40000 ALTER TABLE `assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assettransforms`;

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;

INSERT INTO `assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `height`, `width`, `format`, `quality`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'News Feature Image','newsFeatureImage','crop','center-center',1000,1000,NULL,NULL,'2015-12-09 18:37:40','2015-12-09 16:36:14','2015-12-09 18:37:40','f429d0eb-f312-4808-91dd-f96f65efa466'),
	(2,'News Thumbnail Image','newsThumbnailImage','crop','center-center',172,172,NULL,NULL,'2015-12-09 16:37:31','2015-12-09 16:37:31','2015-12-09 16:37:31','a5396c36-833a-42bf-8131-4586219b76a8'),
	(3,'Content Image','contentImage','crop','center-center',350,350,NULL,NULL,'2016-01-25 17:24:01','2016-01-25 17:23:26','2016-01-25 17:24:01','0659f5a4-4a0f-4187-a0d1-73d8593ab99d'),
	(4,'Location Contact','locationContact','crop','center-center',300,200,NULL,NULL,'2016-02-04 17:29:45','2016-02-04 17:29:45','2016-02-04 17:29:45','0914c739-8755-455c-b12a-fdb039f4f3d5');

/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_fk` (`groupId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `groupId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(20,1,'2015-11-19 18:05:27','2016-02-05 22:38:20','e69a3a36-1e7e-4569-bf22-e08986463215'),
	(21,1,'2015-11-19 18:05:44','2016-02-05 22:37:36','50ba12e3-1ae1-420b-89b6-95bc040cc726'),
	(22,1,'2015-11-19 18:05:53','2016-02-05 22:40:23','26d66f6c-4179-423d-98f0-ce3ea61c1649'),
	(23,1,'2015-11-19 18:06:05','2016-02-05 22:42:30','3447c6c2-0cf9-49cf-a04b-282da5cb9230'),
	(24,1,'2015-11-19 18:06:56','2016-02-04 15:08:21','ce108feb-ef85-4860-866d-ec1e942d0243');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorygroups`;

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `categorygroups_handle_unq_idx` (`handle`),
  KEY `categorygroups_structureId_fk` (`structureId`),
  KEY `categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;

INSERT INTO `categorygroups` (`id`, `structureId`, `fieldLayoutId`, `name`, `handle`, `hasUrls`, `template`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,5,178,'Locations','locations',1,'locations/_entry','2015-11-19 18:04:55','2016-02-04 17:30:16','6e5e24e5-af87-43db-8dac-9159b6672423');

/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categorygroups_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorygroups_i18n`;

CREATE TABLE `categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categorygroups_i18n` WRITE;
/*!40000 ALTER TABLE `categorygroups_i18n` DISABLE KEYS */;

INSERT INTO `categorygroups_i18n` (`id`, `groupId`, `locale`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us','locations/{slug}','{parent.uri}/{slug}','2015-11-19 18:04:55','2015-11-19 18:04:55','3f8305d6-5ebe-4f61-b29d-dd6b63990228');

/*!40000 ALTER TABLE `categorygroups_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contactform
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contactform`;

CREATE TABLE `contactform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content`;

CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_email` text COLLATE utf8_unicode_ci,
  `field_fullName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_careerSummary` text COLLATE utf8_unicode_ci,
  `field_pageDescription` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_address1` text COLLATE utf8_unicode_ci,
  `field_city` text COLLATE utf8_unicode_ci,
  `field_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_zip` text COLLATE utf8_unicode_ci,
  `field_address2` text COLLATE utf8_unicode_ci,
  `field_primaryPhoneNumber` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_primaryEmail` text COLLATE utf8_unicode_ci,
  `field_newsSummary` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `content_title_idx` (`title`),
  KEY `content_locale_fk` (`locale`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;

INSERT INTO `content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_email`, `field_fullName`, `field_careerSummary`, `field_pageDescription`, `field_address1`, `field_city`, `field_state`, `field_zip`, `field_address2`, `field_primaryPhoneNumber`, `field_primaryEmail`, `field_newsSummary`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 21:02:49','2015-11-18 21:02:49','0f7091ff-bfdd-4eb0-91ca-61fdcaa411e6'),
	(2,2,'en_us','Homepage','<p>Our Mission<br>DEDICATED EMPLOYEES.INNOVATIVE BEEF. SATISFIED CONSUMER</p>',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 21:02:52','2016-02-05 15:32:53','6884c9ec-6741-4b2f-a3d7-af3dce79cb84'),
	(4,4,'en_us','About Us','',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:38:05','2016-02-01 20:55:02','fcb512bf-d242-44c6-ae90-9bec2c0578a0'),
	(5,5,'en_us','History/Legacy','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat mauris ut ultrices commodo. Praesent vel gravida tortor, sed porttitor ligula. Praesent imperdiet nibh eleifend ligula viverra, ac volutpat urna rhoncus. Aliquam congue vulputate auctor. Vestibulum bibendum pulvinar viverra. Vivamus imperdiet hendrerit dictum. Mauris pellentesque interdum ante, vel semper mi molestie id. Sed tristique laoreet nunc, eget pulvinar eros pharetra tempus.</p><p>Nunc pharetra, urna vitae cursus vulputate, tortor lectus cursus elit, eget sodales nisi ligula at lectus. Praesent imperdiet diam a sodales euismod. Sed facilisis scelerisque libero, suscipit cursus purus mollis nec. Quisque auctor sem mauris, vitae sollicitudin nunc cursus sit amet. Ut ut auctor turpis. Morbi lacinia eros sed lectus tincidunt egestas. Fusce turpis justo, egestas elementum purus eu, consectetur fermentum felis.</p>',NULL,NULL,NULL,'Friona Industries, L. P. began as single small feedyard in Amarillo, TX in 1962 in the Texas panhandle. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:39:29','2016-01-27 15:57:30','0f200a92-981f-4952-afc5-9d7af3373b6a'),
	(7,7,'en_us','Affiliations','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat mauris ut ultrices commodo. Praesent vel gravida tortor, sed porttitor ligula. Praesent imperdiet nibh eleifend ligula viverra, ac volutpat urna rhoncus. Aliquam congue vulputate auctor. Vestibulum bibendum pulvinar viverra. Vivamus imperdiet hendrerit dictum. Mauris pellentesque interdum ante, vel semper mi molestie id. Sed tristique laoreet nunc, eget pulvinar eros pharetra tempus.</p><p>Nunc pharetra, urna vitae cursus vulputate, tortor lectus cursus elit, eget sodales nisi ligula at lectus. Praesent imperdiet diam a sodales euismod. Sed facilisis scelerisque libero, suscipit cursus purus mollis nec. Quisque auctor sem mauris, vitae sollicitudin nunc cursus sit amet. Ut ut auctor turpis. Morbi lacinia eros sed lectus tincidunt egestas. Fusce turpis justo, egestas elementum purus eu, consectetur fermentum felis.</p>',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:39:48','2016-02-04 16:22:24','5cef0f5a-c849-448d-ab2f-3da86eda2184'),
	(8,8,'en_us','Locations','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel sollicitudin sapien, in commodo nisl. Vestibulum id mi id magna tincidunt vehicula. Praesent accumsan erat non dui commodo, et congue purus sollicitudin. Praesent non sollicitudin lectus, in lobortis odio. Nulla eget porta neque, ut ultricies tortor. Aliquam gravida elit ligula, quis blandit velit ullamcorper sodales. Donec nec fermentum massa. In eu nulla a dolor laoreet egestas vel in nibh. Donec eget blandit massa, quis aliquam risus. Fusce urna dolor, maximus vitae ligula ultricies, porta sodales sapien. Nam ut ligula nisi. Sed nec risus et nisi faucibus bibendum non a ligula. Pellentesque eu turpis metus.</p>',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:40:12','2016-02-04 15:09:22','0a976aff-3f63-413e-9dfc-15a3bb83ef6c'),
	(14,14,'en_us','Careers','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut ornare quam. Praesent et mi eu magna tristique molestie at id erat. Curabitur eget dictum dui, fermentum luctus leo. Nunc at mi massa. Suspendisse fermentum consequat elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae ipsum ante. Integer ultrices pellentesque est eget bibendum.</p>',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:41:55','2016-02-04 14:28:30','aab202a2-d66c-41e2-90e1-20798164b694'),
	(15,15,'en_us','Stewardship','',NULL,NULL,NULL,'A large part of the beef industry’s job involves making sure that beef is safe and wholesome for consumers.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:43:52','2016-02-01 20:39:37','3cb4ceaf-03c3-4490-a083-202bb7aed6ec'),
	(16,16,'en_us','Contact Us','',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:44:37','2016-02-05 22:48:08','906279dd-0d1e-4d9b-bae7-a7c44d7b4e44'),
	(17,17,'en_us','Animal Health','<p>Friona Industries is one of the founding members of the <a href=\"https://www.usrsb.org/about.aspx\" target=\"true\">U.S. Roundtable on Sustainable Beef</a>&nbsp;(USRSB), which is a multi-stakeholder initiative developed to advance, support and communicate continuous improvement in sustainability of the U.S. beef value chain.</p>\r\n\r\n\r\n\r\n<p>Our representative to the group is Blaine Rotramel, our commodity procurement manager at Friona Industries. “We’re here to feed the world,” Rotramel says. “We’re using our knowledge to look at better ways to do things in our business.”</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>USRSB focuses on four areas: Social (employees involved in the community), Economic (community impact), Environmental and Animal Health & Welfare.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Friona Industries is an active supporter in each of those areas. For example, Animal Health & Welfare are addressed through many of the BQA activities&nbsp;at each of the feedyards; and a recent economic impact study showed the Randall County Feedyard provided $7 million in business activity within a 100-mile radius of the location.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>The USRSB is working in collaboration with the <a href=\"http://grsbeef.org/\" target=\"true\">Global Roundtable for Sustainable Beef</a>&nbsp;(GRSB) and other sustainability initiatives whose aims are consistent with its mission and vison.</p>',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:44:50','2016-02-05 15:35:35','4913b6e2-724a-434b-89ec-c12d7b5c0bde'),
	(18,18,'en_us','Community',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:44:55','2016-02-01 20:36:54','85cf1414-1e06-4d4d-bec6-865753537f5b'),
	(19,19,'en_us','ENOUGH','<p>The <a href=\"https://www.enoughmovement.com/learn/index.aspx\" target=\"true\">ENOUGH Movement</a>&nbsp;is a global community working together to ensure that each person has access to nutritious, affordable food, both today and in the coming decades. The global effort is being led by <a href=\"https://www.enoughmovement.com/report/\" target=\"true\">Elanco</a>, one of our business partners.</p>',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-18 22:45:02','2016-02-05 15:36:36','7c12e6e9-7016-41ec-a479-08f4a8b515d5'),
	(20,20,'en_us','Friona Feedyard','<p><a href=\"{asset:135:url}\">Sell your cattle to Friona Feedyard</a>.</p>','john@frionaindustries.com','John Smith',NULL,'','2370 FM3140','Friona','TX','79035',NULL,'','',NULL,'2015-11-19 18:05:27','2016-02-05 22:38:20','e61ab171-1884-4f5b-a6be-7413b34778a3'),
	(21,21,'en_us','Littlefield Feedyard','<p><a href=\"{asset:135:url}\">Sell your cattle to Littlefield Feedyard</a>.</p>','','',NULL,'','FM 37','Littlefield','TX','79339',NULL,'','',NULL,'2015-11-19 18:05:44','2016-02-05 22:37:36','41f4c148-e4ff-4536-9d15-9d7a6cb6c9df'),
	(22,22,'en_us','Randall County Feedyard','<p><a href=\"{asset:135:url}\">Sell your cattle to Randall County Feedyard</a>.</p>','','',NULL,'','15000 FM2219','Amarillo','TX','79119',NULL,'','',NULL,'2015-11-19 18:05:53','2016-02-05 22:40:23','bb8cfc81-0dfc-4bdb-b025-39bf42b9a7b8'),
	(23,23,'en_us','Swisher County Cattle Company','<p><a href=\"{asset:135:url}\">Sell your cattle to Swisher County Feedyard</a>.</p>','','',NULL,'','6589 FM214','Happy','TX','79042',NULL,'','',NULL,'2015-11-19 18:06:05','2016-02-05 22:42:30','24272645-a34c-40bd-ac5d-04eba1afb651'),
	(24,24,'en_us','Friona Industries Corporate Office','<p>The <a href=\"{entry:16:url}\">corporate office</a>&nbsp;for Friona Industries is located in Amarillo, Texas. Additionally, we have four state-of-the-art feedyards in north Texas, with a total feeding capacity that ranks us in the top 10 feedyards worldwide.</p>','','',NULL,'','500 S. Taylor, Suite 601','Amarillo','TX','79105','P.O. Box 15568','(806) 374-1811','fi@frionaind.com',NULL,'2015-11-19 18:06:56','2016-02-04 15:08:21','090c3d40-f614-4f55-bed9-5bed46810436'),
	(44,74,'en_us','Fi Ethics Letter January 2016 New Logo Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-01-27 15:43:49','2016-02-05 20:21:53','c2b8e57f-06bc-47f0-8630-0d2483c35051'),
	(47,82,'en_us','Fi Employment Application',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-01-27 16:39:39','2016-02-05 20:21:51','ed9f1fd7-7627-40ff-b202-90fc34e8dcc0'),
	(48,84,'en_us','News',NULL,NULL,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-01-27 17:37:15','2016-01-27 17:37:15','2d49bd72-363f-44e6-88ce-6c9fb6c04ecf'),
	(52,89,'en_us','Pasture Cattle Portrait',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-01-27 17:59:39','2016-02-05 20:21:52','507cd9e2-41bd-4427-a051-1f87e2c367bf'),
	(53,91,'en_us','Friona Cowboy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-01-27 20:33:02','2016-02-05 20:21:51','9a5a6d3f-bad7-4e1f-b582-8a3789a426e9'),
	(54,93,'en_us','Randall Cattle With Mill',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-01-27 20:33:29','2016-02-05 20:21:52','3b14a8c5-5b0b-416e-9576-1f8606564bf5'),
	(66,114,'en_us','USRSB',NULL,NULL,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-03 22:46:29','2016-02-03 22:46:29','58048139-9a85-4c25-aac5-1708636b5e6b'),
	(67,135,'en_us','Bqa Compliance Position Statement',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 15:32:18','2016-02-05 20:21:52','48b877e3-ee04-4247-a212-4b5850c9bbc0'),
	(68,137,'en_us','BQA',NULL,NULL,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 15:41:29','2016-02-05 15:41:29','a92d6d52-7817-4317-8f8d-1131d812113f'),
	(69,139,'en_us','Friona Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 18:50:31','2016-02-05 20:21:52','f8a8bb70-1e65-46c0-a3d8-b42b3c6386a5'),
	(70,140,'en_us','Jared Randel Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:21:16','2016-02-05 20:21:52','ebe2192a-a358-4bcc-a077-10352bc31b8f'),
	(71,141,'en_us','Ronnie Gonzales Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:21:27','2016-02-05 20:21:52','6d6bdbd7-2650-476b-92db-47c42bdabb10'),
	(72,142,'en_us','Cattle At Feeder',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:47','2016-02-05 20:39:47','995ea9d0-919b-4c11-9dd6-59e1d57df8de'),
	(73,143,'en_us','Feedyard 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:48','2016-02-05 20:39:48','b1777550-6d1e-4537-a637-a9e632a1f65b'),
	(74,144,'en_us','Feedyard 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:48','2016-02-05 20:39:48','23b32d8a-d935-431b-b317-b435f4a3104f'),
	(75,145,'en_us','Fi Pasture 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:48','2016-02-05 20:39:48','3cf82c0b-20a3-4363-8f8e-da758a5450aa'),
	(76,146,'en_us','Fi Pasture 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:49','2016-02-05 20:39:49','7b325c1e-97ea-4fba-9df9-4e54e7f73d70'),
	(77,147,'en_us','Fibrand',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:49','2016-02-05 20:39:49','51e71337-4e07-4822-a168-fea86e26997a'),
	(78,148,'en_us','Friona 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:49','2016-02-05 20:39:49','67f89e01-e2fb-41b3-ab58-67c4c0dd1053'),
	(79,149,'en_us','Friona 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:50','2016-02-05 20:39:50','7c4cddea-bc00-4018-9d1d-80dfd6c2ba3b'),
	(80,150,'en_us','Friona 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:50','2016-02-05 20:39:50','6384059b-2fdc-407a-92d6-375b22b794b2'),
	(81,151,'en_us','Friona 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:50','2016-02-05 20:39:50','c0df24e3-e986-4f67-a25d-9f44bac10f46'),
	(82,152,'en_us','Friona 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:51','2016-02-05 20:39:51','fdcc1665-61f9-48d2-88ca-4d225c98002e'),
	(83,153,'en_us','Friona Assistant Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:51','2016-02-05 20:39:51','2aa0e34a-e348-4d8b-b71a-f675fc54865c'),
	(84,154,'en_us','Friona Cowboy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:51','2016-02-05 20:39:51','5bf4c9b3-d513-4029-ac4a-b644352638ac'),
	(85,155,'en_us','Friona Day',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:51','2016-02-05 20:39:51','a2a5d80c-c3c7-4b2d-9b1d-a5506c185e2c'),
	(86,156,'en_us','Friona Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:52','2016-02-05 20:39:52','0668df78-3d99-4035-89a1-7e14053fbabb'),
	(87,157,'en_us','Friona Office Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:52','2016-02-05 20:39:52','6abad4c7-62c8-429c-9244-55ce9bd8854b'),
	(88,158,'en_us','Friona1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:52','2016-02-05 20:39:52','35fe004c-c741-405c-b951-5b5f80663163'),
	(89,159,'en_us','Jared Randel Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:53','2016-02-05 20:39:53','f41f2e80-7e78-40c8-bb99-9093925dea40'),
	(90,160,'en_us','Littlefield Assistant Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:53','2016-02-05 20:39:53','67b9fd8a-08dd-4780-a9ce-45881ff01d74'),
	(91,161,'en_us','Littlefield Office Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:53','2016-02-05 20:39:53','dc5565b5-cf35-4fe3-97ef-c99ed00cdadf'),
	(92,162,'en_us','Pasture Cattle Portrait',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:54','2016-02-05 20:39:54','a85fbfb1-4dee-4be9-a18a-aaf5907b564c'),
	(93,163,'en_us','Pasture Heifer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:54','2016-02-05 20:39:54','7c10177b-b098-4927-b534-4eaa357d8529'),
	(94,164,'en_us','Randal Ass Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:54','2016-02-05 20:39:54','140f461d-b1f3-4441-92ca-716e32238e62'),
	(95,165,'en_us','Randall Cattle 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:55','2016-02-05 20:39:55','50266c71-bc96-4bc8-b22b-77a385dc0650'),
	(96,166,'en_us','Randall Cattle 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:55','2016-02-05 20:39:55','28ad0abe-2e75-4734-aad0-ed84eed0834d'),
	(97,167,'en_us','Randall Cattle With Mill',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:55','2016-02-05 20:39:55','0a512768-8ab8-4e3d-a93b-013afd45d7c1'),
	(98,168,'en_us','Randall Cowboy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:55','2016-02-05 20:39:55','91e3a787-cc11-49b8-a9d7-f5a1e8a3f8ed'),
	(99,169,'en_us','Randall Office Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:56','2016-02-05 20:39:56','3c85f546-89d8-4600-a45d-17bca168212a'),
	(100,170,'en_us','Randall Yard Cattle',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:56','2016-02-05 20:39:56','fb142793-08bc-42f1-b512-fa5a13f4ad6f'),
	(101,171,'en_us','Ronnie Gonzales Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:56','2016-02-05 20:39:56','065b5b21-c6f0-4ecc-a0c1-d5d994da504d'),
	(102,172,'en_us','Swisher Ass Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:57','2016-02-05 20:39:57','615d916d-4e74-422f-9e41-798cc97a790f'),
	(103,173,'en_us','Swisher Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:57','2016-02-05 20:39:57','fbd49ef7-72eb-4182-9242-d4070eee5ab1'),
	(104,174,'en_us','Swisher Office Manager',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:57','2016-02-05 20:39:57','d43f5885-0bc6-4b91-94eb-ae8bbbe9c4a5'),
	(105,175,'en_us','Trucker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2016-02-05 20:39:58','2016-02-05 20:39:58','d4bad960-4f20-4069-9b27-5aaa2cb17271');

/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deprecationerrors`;

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elementindexsettings`;

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;

INSERT INTO `elementindexsettings` (`id`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Category','{\"sources\":{\"group:1\":{\"tableAttributes\":[]}}}','2015-12-04 18:00:21','2015-12-04 18:00:21','ef6c45ef-6eae-46a4-bedf-76d8a5108772'),
	(2,'Entry','{\"sources\":{\"singles\":{\"tableAttributes\":{\"1\":\"uri\"}},\"section:2\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"uri\"}},\"*\":{\"tableAttributes\":{\"1\":\"section\",\"2\":\"postDate\",\"3\":\"expiryDate\",\"4\":\"uri\"}},\"section:4\":{\"tableAttributes\":{\"1\":\"uri\"}},\"section:8\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"uri\"}},\"section:12\":{\"tableAttributes\":{\"1\":\"uri\"}},\"section:10\":{\"tableAttributes\":{\"1\":\"uri\"}}}}','2015-12-07 14:50:19','2015-12-09 16:34:56','36e310e5-b85c-43ea-8a4c-805c9426f0a3');

/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements`;

CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;

INSERT INTO `elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'User',1,0,'2015-11-18 21:02:49','2015-11-18 21:02:49','379021b9-4734-4123-9c31-ada23ba50da7'),
	(2,'Entry',1,0,'2015-11-18 21:02:52','2016-02-05 15:32:53','7fdfc90e-7640-41f9-9b0b-dcad58d2bee6'),
	(4,'Entry',1,0,'2015-11-18 22:38:05','2016-02-01 20:55:02','cc4e00af-f8da-408f-914f-d03da3755cd4'),
	(5,'Entry',1,0,'2015-11-18 22:39:29','2016-01-27 15:57:30','c3e89de4-afae-4b76-a179-b3041194203a'),
	(7,'Entry',1,0,'2015-11-18 22:39:48','2016-02-04 16:22:24','d629877f-92a6-4e5e-9e24-cf84ecb371e9'),
	(8,'Entry',1,0,'2015-11-18 22:40:12','2016-02-04 15:09:22','a94de208-5ccc-4518-92b5-1e5a97ff6431'),
	(14,'Entry',1,0,'2015-11-18 22:41:55','2016-02-04 14:28:30','414998e1-c4f5-4f6f-8695-be7bd617a3b0'),
	(15,'Entry',1,0,'2015-11-18 22:43:52','2016-02-01 20:39:37','efa7a8f9-d569-4f6a-8d30-f2fe920f978f'),
	(16,'Entry',1,0,'2015-11-18 22:44:37','2016-02-05 22:48:08','0cce4e00-7004-429d-9283-939fb0e883d7'),
	(17,'Entry',1,0,'2015-11-18 22:44:50','2016-02-05 15:35:35','dbb7f74a-ba9e-4eb4-bfec-2be3e5c4de5e'),
	(18,'Entry',1,0,'2015-11-18 22:44:55','2016-02-01 20:36:54','22ce5bcb-08ed-49a9-8c1d-d9af5a396091'),
	(19,'Entry',1,0,'2015-11-18 22:45:02','2016-02-05 15:36:36','574e5393-55fb-44a6-b15f-931104012012'),
	(20,'Category',1,0,'2015-11-19 18:05:27','2016-02-05 22:38:20','b5ef2ace-6d28-4df9-9bdd-6c1a11af41de'),
	(21,'Category',1,0,'2015-11-19 18:05:44','2016-02-05 22:37:36','94db82fd-ef60-4ce0-9a38-2d12b3b0f915'),
	(22,'Category',1,0,'2015-11-19 18:05:53','2016-02-05 22:40:23','df903cb0-8ccd-4d87-9fb5-edb8a787f6aa'),
	(23,'Category',1,0,'2015-11-19 18:06:05','2016-02-05 22:42:30','889faa43-5789-4a4a-9fff-3e33d9f2f9bf'),
	(24,'Category',1,0,'2015-11-19 18:06:56','2016-02-04 15:08:21','eb2e1814-232a-445e-a850-21bef0bc97b2'),
	(44,'MatrixBlock',1,0,'2015-12-09 20:31:25','2016-02-01 20:55:03','7880d542-789b-4efa-bac2-fa15e519f265'),
	(46,'MatrixBlock',1,0,'2015-12-09 20:48:53','2016-02-05 22:48:08','86c1ac6e-5951-46ca-80e2-8c8bfb1ab8d0'),
	(48,'MatrixBlock',1,0,'2015-12-09 20:52:35','2016-02-01 20:39:37','c1feac11-2c1f-4272-b178-0127c7bcd9f0'),
	(50,'MatrixBlock',1,0,'2016-01-25 16:53:01','2016-02-05 15:32:53','34a1a9f6-1df6-4d17-b979-9d398f78fb17'),
	(72,'MatrixBlock',1,0,'2016-01-27 15:42:44','2016-02-01 20:55:03','b1170060-80b4-4252-a682-7e4e5e4ea36a'),
	(73,'MatrixBlock',1,0,'2016-01-27 15:42:44','2016-02-01 20:55:03','cde0a34d-b968-4fc7-ad36-fbb56ae67f5f'),
	(74,'Asset',1,0,'2016-01-27 15:43:49','2016-02-05 20:21:53','18200964-67ea-48a4-96f4-832f85ed5558'),
	(76,'MatrixBlock',1,0,'2016-01-27 15:56:18','2016-02-04 14:28:30','838aced5-10a5-485f-a6d8-3e4eb7f81a2f'),
	(77,'MatrixBlock',1,0,'2016-01-27 15:57:30','2016-01-27 15:57:30','63b8da49-27c9-422a-8be3-627126d6869c'),
	(78,'MatrixBlock',1,0,'2016-01-27 16:26:27','2016-02-04 14:28:30','c51b25d9-db0d-49b7-9efc-ab116d563a47'),
	(80,'MatrixBlock',1,0,'2016-01-27 16:36:30','2016-02-04 14:28:30','c14a2ba9-bda8-4db5-8201-4935cb798e0d'),
	(81,'MatrixBlock',1,0,'2016-01-27 16:36:30','2016-02-04 14:28:30','767310c7-0ec4-49ee-8b3f-e9eaf889de74'),
	(82,'Asset',1,0,'2016-01-27 16:39:39','2016-02-05 20:21:51','605533d3-f22b-4c59-914f-93cae1812bf4'),
	(83,'MatrixBlock',1,0,'2016-01-27 16:59:45','2016-02-05 22:48:08','85889ab6-b871-4195-aee4-09997cc68023'),
	(84,'Entry',1,0,'2016-01-27 17:37:15','2016-01-27 17:37:15','b5f302b1-de8f-4e90-8ac1-87d8fd1c5ea1'),
	(85,'MatrixBlock',1,0,'2016-01-27 17:37:15','2016-01-27 17:37:15','cf00ba23-e8a6-41b6-bbab-e079e4d40805'),
	(89,'Asset',1,0,'2016-01-27 17:59:39','2016-02-05 20:21:52','f78c81af-1efd-4bf9-8a93-060873b87b82'),
	(91,'Asset',1,0,'2016-01-27 20:33:02','2016-02-05 20:21:51','f69a20d2-c432-42ea-a344-cc85eb20530f'),
	(93,'Asset',1,0,'2016-01-27 20:33:29','2016-02-05 20:21:52','a71aa81f-caf9-4efa-96de-e15fec161c2a'),
	(96,'MatrixBlock',1,0,'2016-02-01 20:36:54','2016-02-01 20:36:54','fb6e3576-2f36-49cf-bace-35a278d920ce'),
	(97,'MatrixBlock',1,0,'2016-02-01 20:36:54','2016-02-01 20:36:54','6edba87c-5401-442c-8d45-5d6bbf1409b3'),
	(99,'MatrixBlock',1,0,'2016-02-01 20:38:22','2016-02-05 15:36:36','33fe2ffd-2f07-4d17-b186-23cb9a34896b'),
	(111,'MatrixBlock',1,0,'2016-02-03 19:49:07','2016-02-05 15:32:53','8b3d6727-7fbb-438a-843b-10ce4cf9e9a6'),
	(112,'MatrixBlock',1,0,'2016-02-03 20:40:13','2016-02-05 15:32:53','a15c9853-af93-4365-bb19-a5ddfdaedca3'),
	(113,'MatrixBlock',1,0,'2016-02-03 20:40:56','2016-02-05 15:32:53','dd28b50c-9788-4c47-ae2c-ea354cdc99e9'),
	(114,'Entry',1,0,'2016-02-03 22:46:29','2016-02-03 22:46:29','1917047a-ec6a-490d-965a-cb8f7c77bf18'),
	(115,'MatrixBlock',1,0,'2016-02-03 22:46:29','2016-02-03 22:46:29','5c382069-bdb7-4899-b550-9934c2f33918'),
	(116,'MatrixBlock',1,0,'2016-02-03 22:46:29','2016-02-03 22:46:29','c43ec454-c33e-4542-80ca-ad4ebaaf6426'),
	(117,'MatrixBlock',1,0,'2016-02-04 14:28:30','2016-02-04 14:28:30','d01ed1a8-a3bd-45f6-acb5-5c6188eed93a'),
	(118,'MatrixBlock',1,0,'2016-02-04 14:28:30','2016-02-04 14:28:30','bcfab5c9-2ebc-47fb-a2b0-9d27e4a9f9ab'),
	(119,'MatrixBlock',1,0,'2016-02-04 14:28:30','2016-02-04 14:28:30','78e4ca8b-cca4-478f-b9f7-a643744d8726'),
	(120,'MatrixBlock',1,0,'2016-02-04 14:28:30','2016-02-04 14:28:30','fe62aa4c-7bf0-4725-b361-ee52a9ebddfe'),
	(121,'MatrixBlock',1,0,'2016-02-04 14:28:30','2016-02-04 14:28:30','022b938b-aaab-4f1b-ab9f-49683238f6c7'),
	(122,'MatrixBlock',1,0,'2016-02-04 14:28:30','2016-02-04 14:28:30','eef1c4ab-8401-4009-8a58-283c982ab711'),
	(123,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','ec9f525e-2da0-4499-aee9-8e142f771bd7'),
	(124,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','212f76c2-a8a6-445e-83b0-21e4cc065bf4'),
	(125,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','ad9d5e0f-8370-4426-8466-8919723c8c4f'),
	(126,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','a3af350e-3e5c-41d7-b7dc-652f6af3eb73'),
	(127,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','2b04301b-8eda-43c6-a3af-4086ddb46537'),
	(128,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','0d26f64f-46cd-42d7-8634-8f548c546d3e'),
	(129,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','74074290-8947-4c95-9187-dab83f6ae687'),
	(130,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','dbffd3c8-5b69-49a2-8f95-6128a46f130e'),
	(131,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','4bf7f733-eb87-45ad-8454-02220867bd75'),
	(132,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','32d2ebbf-e725-481d-a8ba-2c99cea48772'),
	(133,'MatrixBlock',1,0,'2016-02-04 16:44:13','2016-02-05 22:48:08','a25802d0-83dc-41bb-a3f3-c3c936262c2a'),
	(134,'MatrixBlock',1,0,'2016-02-04 17:30:53','2016-02-05 22:38:20','613b10e0-8180-467b-bea7-469dfb76c6f6'),
	(135,'Asset',1,0,'2016-02-05 15:32:18','2016-02-05 20:21:52','bfafcf6e-86ce-4d7d-8a2d-d76f08ab9b40'),
	(136,'MatrixBlock',1,0,'2016-02-05 15:35:14','2016-02-05 15:35:35','d9d9f906-a4dc-408b-8a5f-ef279775e815'),
	(137,'Entry',1,0,'2016-02-05 15:41:29','2016-02-05 15:41:29','59a62997-dce8-47f1-95a6-c952369c5773'),
	(138,'MatrixBlock',1,0,'2016-02-05 15:41:29','2016-02-05 15:41:29','9dd47da9-c7e6-4db8-8e66-23746516a156'),
	(139,'Asset',1,0,'2016-02-05 18:50:31','2016-02-05 20:21:52','dd624fe4-a8d7-4477-a632-f3fb65a1d422'),
	(140,'Asset',1,0,'2016-02-05 20:21:16','2016-02-05 20:21:52','a22bf0fb-fc86-40c0-b2f2-f9e2d83d61fc'),
	(141,'Asset',1,0,'2016-02-05 20:21:27','2016-02-05 20:21:52','a38ebb4a-6e38-4b5f-b7f6-495e68e99073'),
	(142,'Asset',1,0,'2016-02-05 20:39:47','2016-02-05 20:39:47','977e9d23-ee46-44db-97d1-f5de8386258f'),
	(143,'Asset',1,0,'2016-02-05 20:39:48','2016-02-05 20:39:48','e27739ba-4810-4aec-a25a-05b46f418912'),
	(144,'Asset',1,0,'2016-02-05 20:39:48','2016-02-05 20:39:48','12d68a3a-cafc-46b8-a9f4-f9ecdf70077e'),
	(145,'Asset',1,0,'2016-02-05 20:39:48','2016-02-05 20:39:48','9a39e3b3-0166-44f2-ac88-f826911bb095'),
	(146,'Asset',1,0,'2016-02-05 20:39:49','2016-02-05 20:39:49','71a86668-ba92-46d2-83bd-66085b418e97'),
	(147,'Asset',1,0,'2016-02-05 20:39:49','2016-02-05 20:39:49','fd619a4c-3537-4567-a41a-cf5c196f9d21'),
	(148,'Asset',1,0,'2016-02-05 20:39:49','2016-02-05 20:39:49','2c89df76-f189-4414-9915-af143b84cdd3'),
	(149,'Asset',1,0,'2016-02-05 20:39:50','2016-02-05 20:39:50','3f8e074c-e164-4ea0-b595-02950614e197'),
	(150,'Asset',1,0,'2016-02-05 20:39:50','2016-02-05 20:39:50','68fd9f85-7f74-4911-9bc2-f489ab3ac1c2'),
	(151,'Asset',1,0,'2016-02-05 20:39:50','2016-02-05 20:39:50','c1302c8d-c09d-490b-8fbb-123bd04fef56'),
	(152,'Asset',1,0,'2016-02-05 20:39:51','2016-02-05 20:39:51','ce59ffb7-1fc4-48b4-adc6-448e2e4248c2'),
	(153,'Asset',1,0,'2016-02-05 20:39:51','2016-02-05 20:39:51','5b4668f9-5228-420a-bf6a-02e78b603e76'),
	(154,'Asset',1,0,'2016-02-05 20:39:51','2016-02-05 20:39:51','2c0865ce-ff75-4d28-bb56-f52aa9278308'),
	(155,'Asset',1,0,'2016-02-05 20:39:51','2016-02-05 20:39:51','cc3f3262-67bd-4bbd-8619-6032890b79a7'),
	(156,'Asset',1,0,'2016-02-05 20:39:52','2016-02-05 20:39:52','ff960ec7-e5a5-4d8e-acc6-79e9521b9756'),
	(157,'Asset',1,0,'2016-02-05 20:39:52','2016-02-05 20:39:52','fab97774-c887-4dd0-a985-188957482648'),
	(158,'Asset',1,0,'2016-02-05 20:39:52','2016-02-05 20:39:52','90db279c-34d9-4bf4-956c-9e9266954483'),
	(159,'Asset',1,0,'2016-02-05 20:39:53','2016-02-05 20:39:53','4d91af8b-4ec1-4578-8ca4-9e6ebe18506c'),
	(160,'Asset',1,0,'2016-02-05 20:39:53','2016-02-05 20:39:53','b833442c-49de-4310-b5ae-0accf087d6ab'),
	(161,'Asset',1,0,'2016-02-05 20:39:53','2016-02-05 20:39:53','684de488-ed30-4ee2-8a88-085fe9fe0b12'),
	(162,'Asset',1,0,'2016-02-05 20:39:54','2016-02-05 20:39:54','d3e8e8bc-fd46-43c0-80b2-c4a33f885e4b'),
	(163,'Asset',1,0,'2016-02-05 20:39:54','2016-02-05 20:39:54','204705b9-62ef-4b8b-b603-7f12180c1251'),
	(164,'Asset',1,0,'2016-02-05 20:39:54','2016-02-05 20:39:54','1a9862e9-0ea4-4716-a574-2cf372570846'),
	(165,'Asset',1,0,'2016-02-05 20:39:55','2016-02-05 20:39:55','79692c5c-9d78-416c-bc29-0a5e778e753e'),
	(166,'Asset',1,0,'2016-02-05 20:39:55','2016-02-05 20:39:55','72dbe0ca-8b4c-4840-9886-59444fb48a2a'),
	(167,'Asset',1,0,'2016-02-05 20:39:55','2016-02-05 20:39:55','19bc93c4-d207-417b-baf1-5385e5c70825'),
	(168,'Asset',1,0,'2016-02-05 20:39:55','2016-02-05 20:39:55','05fd2226-285a-4ff6-862f-be04f4d88267'),
	(169,'Asset',1,0,'2016-02-05 20:39:56','2016-02-05 20:39:56','596ac50f-5c7c-464e-8a27-70b382f27aa8'),
	(170,'Asset',1,0,'2016-02-05 20:39:56','2016-02-05 20:39:56','55d9f4f4-283a-49dd-94e9-921e7fc8933a'),
	(171,'Asset',1,0,'2016-02-05 20:39:56','2016-02-05 20:39:56','b3e61af7-c262-43db-950c-92b8a199e888'),
	(172,'Asset',1,0,'2016-02-05 20:39:57','2016-02-05 20:39:57','87cc2fc3-ee65-4c9c-9d1e-afa14d234407'),
	(173,'Asset',1,0,'2016-02-05 20:39:57','2016-02-05 20:39:57','dc992dbe-ccc5-4ed6-b999-16e335422463'),
	(174,'Asset',1,0,'2016-02-05 20:39:57','2016-02-05 20:39:57','f151c6cc-5f66-4fb5-b824-75239ec4cd87'),
	(175,'Asset',1,0,'2016-02-05 20:39:58','2016-02-05 20:39:58','44199fbb-b0a7-4ae4-b1b5-e34c4bfc96a1'),
	(176,'MatrixBlock',1,0,'2016-02-05 22:32:51','2016-02-05 22:38:20','79a6b4f2-51d8-4f80-ba75-0355ee236b76'),
	(177,'MatrixBlock',1,0,'2016-02-05 22:32:51','2016-02-05 22:38:20','71204c60-0fbc-437a-a1d9-c17ee2b9dbae'),
	(178,'MatrixBlock',1,0,'2016-02-05 22:37:36','2016-02-05 22:37:36','c3ce7a4a-1f36-4009-acfa-9b5392c7832b'),
	(179,'MatrixBlock',1,0,'2016-02-05 22:37:36','2016-02-05 22:37:36','a579997b-3c8f-4d7a-9fae-9411532b7e92'),
	(180,'MatrixBlock',1,0,'2016-02-05 22:37:36','2016-02-05 22:37:36','c30babac-c954-465e-9848-94d34bba8a54'),
	(181,'MatrixBlock',1,0,'2016-02-05 22:40:23','2016-02-05 22:40:23','ff374fe0-27e6-4811-9d54-7f86af24f393'),
	(182,'MatrixBlock',1,0,'2016-02-05 22:40:23','2016-02-05 22:40:23','0b88d1c7-f625-4a3b-8301-4922930008d6'),
	(183,'MatrixBlock',1,0,'2016-02-05 22:40:23','2016-02-05 22:40:23','c456a430-739c-4cdc-ade6-2c81f52b6f5d'),
	(184,'MatrixBlock',1,0,'2016-02-05 22:42:30','2016-02-05 22:42:30','6cca6e0a-f378-423b-8604-7c0adf9712c6'),
	(185,'MatrixBlock',1,0,'2016-02-05 22:42:30','2016-02-05 22:42:30','49b77b5a-35e7-472f-9305-03065f4224cf'),
	(186,'MatrixBlock',1,0,'2016-02-05 22:42:30','2016-02-05 22:42:30','af39fcc2-5dd4-4a14-b952-fd2dab466f7c');

/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table elements_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements_i18n`;

CREATE TABLE `elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `elements_i18n_enabled_idx` (`enabled`),
  KEY `elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elements_i18n` WRITE;
/*!40000 ALTER TABLE `elements_i18n` DISABLE KEYS */;

INSERT INTO `elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us','',NULL,1,'2015-11-18 21:02:49','2015-11-18 21:02:49','7a812ce4-12a6-4764-b591-07a1af34b954'),
	(2,2,'en_us','homepage','__home__',1,'2015-11-18 21:02:52','2016-02-05 15:32:53','1c7a97ce-bb0f-45db-a277-3cee732c8fbf'),
	(4,4,'en_us','about-us','about',1,'2015-11-18 22:38:05','2016-02-01 20:55:03','c1659b93-9573-4976-89b4-b098c0995e00'),
	(5,5,'en_us','history','about/history',1,'2015-11-18 22:39:29','2016-01-27 15:57:30','f7991cce-b39a-4000-8ba4-076728b2f405'),
	(7,7,'en_us','affilliations','about/affilliations',1,'2015-11-18 22:39:48','2016-02-04 16:22:24','7cd8fd7a-86c9-4709-9701-dc91890fa5f4'),
	(8,8,'en_us','locations','locations',1,'2015-11-18 22:40:12','2016-02-04 15:09:22','c8e78f95-beb6-4b01-a978-aca64f1240d2'),
	(14,14,'en_us','careers','careers',1,'2015-11-18 22:41:55','2016-02-04 14:28:30','2cf0d29b-c05d-4eb3-918f-78c57f1695aa'),
	(15,15,'en_us','sustainability','stewardship',1,'2015-11-18 22:43:52','2016-02-01 20:39:37','c50a42ea-e4b3-4ce7-879a-c3af97503d26'),
	(16,16,'en_us','contact-us','contact',1,'2015-11-18 22:44:37','2016-02-05 22:48:08','ba02c1c9-f129-4489-b813-3fd8466c23b4'),
	(17,17,'en_us','animal-health','stewardship/animal-health',1,'2015-11-18 22:44:50','2016-02-05 15:35:35','e000e724-1274-4c3a-b477-eee9f85805b4'),
	(18,18,'en_us','community','stewardship/community',1,'2015-11-18 22:44:55','2016-02-01 20:36:54','8957d2b3-608b-43fa-b54e-2e107a801d4f'),
	(19,19,'en_us','enough-movement','stewardship/enough-movement',1,'2015-11-18 22:45:02','2016-02-05 15:36:36','e7de7b13-40a6-43b4-a6f3-8ae2a9c7efb9'),
	(20,20,'en_us','friona-feedyard','locations/friona-feedyard',1,'2015-11-19 18:05:27','2016-02-05 22:38:20','fbf24101-c1d4-4df2-87e9-012725753871'),
	(21,21,'en_us','littlefield-feedyard','locations/littlefield-feedyard',1,'2015-11-19 18:05:44','2016-02-05 22:37:36','23587d1c-9da5-4c95-a125-17ccc4ee8113'),
	(22,22,'en_us','randall-county-feedyard','locations/randall-county-feedyard',1,'2015-11-19 18:05:53','2016-02-05 22:40:23','596749a2-315e-4f59-9f39-fe399e6f5d69'),
	(23,23,'en_us','swisher-county-cattle-company','locations/swisher-county-cattle-company',1,'2015-11-19 18:06:05','2016-02-05 22:42:30','601f5453-aeb1-4824-a152-db18fa6722e8'),
	(24,24,'en_us','corporate-office','locations/corporate-office',1,'2015-11-19 18:06:56','2016-02-04 15:08:47','d904a794-0b02-45fe-993d-2d66a55ffc80'),
	(44,44,'en_us','',NULL,1,'2015-12-09 20:31:25','2016-02-01 20:55:03','e6572b4c-02bf-46b1-8899-4e11fc0d63d8'),
	(46,46,'en_us','',NULL,1,'2015-12-09 20:48:53','2016-02-05 22:48:08','ec4c9caa-7e1a-473c-b2db-7270b2f90bc2'),
	(48,48,'en_us','',NULL,1,'2015-12-09 20:52:35','2016-02-01 20:39:37','e3581c9e-455a-4d31-a731-1e490e807912'),
	(50,50,'en_us','',NULL,1,'2016-01-25 16:53:01','2016-02-05 15:32:53','5c867389-c8b7-405d-9a46-56487226d48f'),
	(72,72,'en_us','',NULL,1,'2016-01-27 15:42:44','2016-02-01 20:55:03','fbdcd0e5-1c2b-41b6-b4bc-602a9fdd1575'),
	(73,73,'en_us','',NULL,1,'2016-01-27 15:42:44','2016-02-01 20:55:03','ad5812aa-6324-416f-b1eb-87584c387967'),
	(74,74,'en_us','fi-ethics-letter-january-2016-new-logo-pd',NULL,1,'2016-01-27 15:43:49','2016-02-05 20:21:53','78cb30c1-d4f9-4f73-a275-9259656096e1'),
	(76,76,'en_us','',NULL,1,'2016-01-27 15:56:18','2016-02-04 14:28:30','b1100645-097a-4c22-94f2-27b99a6a70e6'),
	(77,77,'en_us','',NULL,1,'2016-01-27 15:57:30','2016-01-27 15:57:30','b1108c1a-bf3b-428e-9d86-74bc2c8b96e7'),
	(78,78,'en_us','',NULL,1,'2016-01-27 16:26:27','2016-02-04 14:28:30','f2dcf4a5-08d5-40a9-9288-9e506cf723e3'),
	(80,80,'en_us','',NULL,1,'2016-01-27 16:36:30','2016-02-04 14:28:30','997cd473-6f06-45ab-b619-61bb7e01be99'),
	(81,81,'en_us','',NULL,1,'2016-01-27 16:36:30','2016-02-04 14:28:30','dc433638-0a13-4140-b54d-41c1793bfbd8'),
	(82,82,'en_us','fi-employment-application',NULL,1,'2016-01-27 16:39:39','2016-02-05 20:21:51','4082589e-b935-4a3c-9d4a-6be92f467ece'),
	(83,83,'en_us','',NULL,1,'2016-01-27 16:59:45','2016-02-05 22:48:08','2ab5ec8e-e154-4c9d-b6b2-ea957c867544'),
	(84,84,'en_us','news','about/news',1,'2016-01-27 17:37:15','2016-01-27 17:37:16','f372eb4e-b8a0-46f7-9ae0-4f61d45f8115'),
	(85,85,'en_us','',NULL,1,'2016-01-27 17:37:15','2016-01-27 17:37:15','baefda1e-f302-46ab-98e7-4da4f0106965'),
	(89,89,'en_us','pasture-cattle-portrait',NULL,1,'2016-01-27 17:59:39','2016-02-05 20:21:52','f5172864-36dc-4406-a2b2-6c81174134df'),
	(91,91,'en_us','friona-cowboy',NULL,1,'2016-01-27 20:33:02','2016-02-05 20:21:51','02468453-0a31-48fc-82d6-a869d4ced946'),
	(93,93,'en_us','randall-cattle-with-mill',NULL,1,'2016-01-27 20:33:29','2016-02-05 20:21:52','1d44d49b-cc91-4c85-97b7-504158fdd6e4'),
	(96,96,'en_us','',NULL,1,'2016-02-01 20:36:54','2016-02-01 20:36:54','a9e47500-4075-4da9-887b-cf9d584abdc4'),
	(97,97,'en_us','',NULL,1,'2016-02-01 20:36:54','2016-02-01 20:36:54','e9be935c-c659-4cdf-90e2-e180196f1901'),
	(99,99,'en_us','',NULL,1,'2016-02-01 20:38:22','2016-02-05 15:36:36','9260b2fb-d13c-4545-aeba-e9af212cca9a'),
	(111,111,'en_us','',NULL,1,'2016-02-03 19:49:07','2016-02-05 15:32:53','1ac008b0-369b-47cf-b71b-b9c81fdb9952'),
	(112,112,'en_us','',NULL,1,'2016-02-03 20:40:13','2016-02-05 15:32:53','2f3449b6-0f4e-46da-a99a-760da316e7ff'),
	(113,113,'en_us','',NULL,1,'2016-02-03 20:40:56','2016-02-05 15:32:53','abdb6e76-22a6-4d1a-8640-a40edaf8ae15'),
	(114,114,'en_us','usrsb','stewardship/usrsb',1,'2016-02-03 22:46:29','2016-02-03 22:46:30','2b27b097-3b4d-4c99-8ba2-131dea8b6d4d'),
	(115,115,'en_us','',NULL,1,'2016-02-03 22:46:29','2016-02-03 22:46:29','4189ad1b-7e4b-4334-9e15-9c2930f8b3da'),
	(116,116,'en_us','',NULL,1,'2016-02-03 22:46:29','2016-02-03 22:46:29','b5f80549-12d7-4f62-8cdb-c834b46e9358'),
	(117,117,'en_us','',NULL,1,'2016-02-04 14:28:30','2016-02-04 14:28:30','1a0fdf02-ec7c-49bf-a483-68afadddad6d'),
	(118,118,'en_us','',NULL,1,'2016-02-04 14:28:30','2016-02-04 14:28:30','2064c19b-559e-4a16-b6c1-467e17989b32'),
	(119,119,'en_us','',NULL,1,'2016-02-04 14:28:30','2016-02-04 14:28:30','14fc469e-bd90-4b0f-8c4b-2bef7b6335ad'),
	(120,120,'en_us','',NULL,1,'2016-02-04 14:28:30','2016-02-04 14:28:30','eb49b5f4-a39e-43a2-a8f5-0726513ef0be'),
	(121,121,'en_us','',NULL,1,'2016-02-04 14:28:30','2016-02-04 14:28:30','047f7ce6-1a41-4cd1-b95d-45d6aac78017'),
	(122,122,'en_us','',NULL,1,'2016-02-04 14:28:30','2016-02-04 14:28:30','d0d0bf8d-6739-48a0-8f13-b39a59362ce4'),
	(123,123,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','ff3084a2-0677-47d6-8672-b0440e64c694'),
	(124,124,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','44f5a5b8-d970-42e5-8596-5afbd191f60e'),
	(125,125,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','b2c8efde-6518-470a-b7a0-61027b5070d5'),
	(126,126,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','25abc0e1-daa3-4055-a2b6-9669e7a2e485'),
	(127,127,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','3e220b98-c692-46f9-b795-bc0163b8861b'),
	(128,128,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','83986aed-9538-4fec-9a84-e67dae00d162'),
	(129,129,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','f6837ad5-1c9b-4fa5-a9c0-22b9e30b1a7a'),
	(130,130,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','97d527b9-3a4f-4da4-8d15-66c1b0f64069'),
	(131,131,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','2c535ea2-1215-4737-b700-413627296e09'),
	(132,132,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','366fcd54-28cf-4021-ae8e-2aed9e267dbd'),
	(133,133,'en_us','',NULL,1,'2016-02-04 16:44:13','2016-02-05 22:48:08','9b2c5665-2d28-430e-b3ca-530dc298dddd'),
	(134,134,'en_us','',NULL,1,'2016-02-04 17:30:53','2016-02-05 22:38:20','85ee4d58-84c9-4a45-b5ed-2da32b7ea7ac'),
	(135,135,'en_us','bqa-compliance-position-statement',NULL,1,'2016-02-05 15:32:18','2016-02-05 20:21:52','e72a5a2c-9b9d-4f1a-9bde-ac978075751a'),
	(136,136,'en_us','',NULL,1,'2016-02-05 15:35:14','2016-02-05 15:35:35','8951ea84-483b-4487-99d7-b6198df86508'),
	(137,137,'en_us','bqa','stewardship/bqa',1,'2016-02-05 15:41:29','2016-02-05 15:41:30','d88555c8-086b-416b-9594-0c4159d33fae'),
	(138,138,'en_us','',NULL,1,'2016-02-05 15:41:29','2016-02-05 15:41:29','0d14ec16-5054-40a5-94c3-7637bf3d86ef'),
	(139,139,'en_us','friona-manager',NULL,1,'2016-02-05 18:50:31','2016-02-05 20:21:52','94e306e7-02d4-47fa-b976-21df341aa4a8'),
	(140,140,'en_us','jared-randel-manager',NULL,1,'2016-02-05 20:21:16','2016-02-05 20:21:52','42641e30-c028-4e8f-8fb1-f502050ed3bf'),
	(141,141,'en_us','ronnie-gonzales-manager',NULL,1,'2016-02-05 20:21:27','2016-02-05 20:21:52','b278b742-cbab-4b22-a996-bdee9b1f5798'),
	(142,142,'en_us','cattle-at-feeder',NULL,1,'2016-02-05 20:39:47','2016-02-05 20:39:47','df9029ea-7f29-4439-9217-a52200235df9'),
	(143,143,'en_us','feedyard-1',NULL,1,'2016-02-05 20:39:48','2016-02-05 20:39:48','2d8d3264-0cca-488b-b3ac-dde6243c7b82'),
	(144,144,'en_us','feedyard-2',NULL,1,'2016-02-05 20:39:48','2016-02-05 20:39:48','d8a54f28-1fda-4601-83c4-c192f69c4f87'),
	(145,145,'en_us','fi-pasture-1',NULL,1,'2016-02-05 20:39:48','2016-02-05 20:39:48','7cff1c49-bfa1-4d80-97ba-a02b6e0af837'),
	(146,146,'en_us','fi-pasture-3',NULL,1,'2016-02-05 20:39:49','2016-02-05 20:39:49','cf245715-add0-4f06-ae02-289665d00a0c'),
	(147,147,'en_us','fibrand',NULL,1,'2016-02-05 20:39:49','2016-02-05 20:39:49','47c1c365-9959-4ce6-bd5c-bb5227dd09de'),
	(148,148,'en_us','friona-1',NULL,1,'2016-02-05 20:39:49','2016-02-05 20:39:49','5f8edf01-f594-4d12-a680-99edf69db854'),
	(149,149,'en_us','friona-2',NULL,1,'2016-02-05 20:39:50','2016-02-05 20:39:50','09ad50d9-623e-443b-9429-7b7181923817'),
	(150,150,'en_us','friona-3',NULL,1,'2016-02-05 20:39:50','2016-02-05 20:39:50','92ba2977-fda3-49ce-af4e-5e0451504888'),
	(151,151,'en_us','friona-4',NULL,1,'2016-02-05 20:39:50','2016-02-05 20:39:50','94d5a09a-d524-40b6-b39e-a70ec87bdab4'),
	(152,152,'en_us','friona-5',NULL,1,'2016-02-05 20:39:51','2016-02-05 20:39:51','215eee51-9688-4519-9efa-6360f8fee87a'),
	(153,153,'en_us','friona-assistant-manager',NULL,1,'2016-02-05 20:39:51','2016-02-05 20:39:51','036cf9ae-c9fd-4f5b-9667-efa381ae7ae6'),
	(154,154,'en_us','friona-cowboy',NULL,1,'2016-02-05 20:39:51','2016-02-05 20:39:51','3bc91427-1957-4766-9045-c8b75d172804'),
	(155,155,'en_us','friona-day',NULL,1,'2016-02-05 20:39:51','2016-02-05 20:39:51','bb0b0a17-c424-4663-9d26-cef9419e6203'),
	(156,156,'en_us','friona-manager',NULL,1,'2016-02-05 20:39:52','2016-02-05 20:39:52','eb74afb4-1560-4423-a060-ab36702f8aa2'),
	(157,157,'en_us','friona-office-manager',NULL,1,'2016-02-05 20:39:52','2016-02-05 20:39:52','e56ef2fe-8486-4b71-ae59-96bded0ae8b6'),
	(158,158,'en_us','friona1',NULL,1,'2016-02-05 20:39:52','2016-02-05 20:39:52','e22fc294-9c13-42ac-86c8-59e6d924e669'),
	(159,159,'en_us','jared-randel-manager',NULL,1,'2016-02-05 20:39:53','2016-02-05 20:39:53','1c0caff1-afdd-4909-b4d1-abf345a55eda'),
	(160,160,'en_us','littlefield-assistant-manager',NULL,1,'2016-02-05 20:39:53','2016-02-05 20:39:53','414ac51a-7010-466e-b8d1-ef0a0cebcde4'),
	(161,161,'en_us','littlefield-office-manager',NULL,1,'2016-02-05 20:39:53','2016-02-05 20:39:53','69c4a99b-0178-44b8-a960-fa073713ccb6'),
	(162,162,'en_us','pasture-cattle-portrait',NULL,1,'2016-02-05 20:39:54','2016-02-05 20:39:54','6a458568-07d9-43d8-8da3-93519e047b12'),
	(163,163,'en_us','pasture-heifer',NULL,1,'2016-02-05 20:39:54','2016-02-05 20:39:54','111af7b0-d29b-45c4-bc61-c4f3040d7425'),
	(164,164,'en_us','randal-ass-manager',NULL,1,'2016-02-05 20:39:54','2016-02-05 20:39:54','3e779d79-eac8-4c0e-8b7c-6dd259d7afb3'),
	(165,165,'en_us','randall-cattle-1',NULL,1,'2016-02-05 20:39:55','2016-02-05 20:39:55','d827490f-b292-4320-8d5c-abb5e5782492'),
	(166,166,'en_us','randall-cattle-2',NULL,1,'2016-02-05 20:39:55','2016-02-05 20:39:55','f7951fac-c7e9-4326-aa14-b62a7f9129c8'),
	(167,167,'en_us','randall-cattle-with-mill',NULL,1,'2016-02-05 20:39:55','2016-02-05 20:39:55','ecbd78ec-c166-41fb-bbef-8b09858f6768'),
	(168,168,'en_us','randall-cowboy',NULL,1,'2016-02-05 20:39:55','2016-02-05 20:39:55','85a84eab-6823-4d07-8091-70697b83644e'),
	(169,169,'en_us','randall-office-manager',NULL,1,'2016-02-05 20:39:56','2016-02-05 20:39:56','1a01f4be-44df-49f5-9578-cf19d6985ccc'),
	(170,170,'en_us','randall-yard-cattle',NULL,1,'2016-02-05 20:39:56','2016-02-05 20:39:56','7479c375-fa1d-407a-ae0e-4cffce839423'),
	(171,171,'en_us','ronnie-gonzales-manager',NULL,1,'2016-02-05 20:39:56','2016-02-05 20:39:56','626c3a59-bba5-4038-a37a-fe3e188e3a1e'),
	(172,172,'en_us','swisher-ass-manager',NULL,1,'2016-02-05 20:39:57','2016-02-05 20:39:57','3daedc6d-6801-4eaf-8b90-220fbddcf87f'),
	(173,173,'en_us','swisher-manager',NULL,1,'2016-02-05 20:39:57','2016-02-05 20:39:57','87d4efa6-9772-44cf-a89e-49fe689b639e'),
	(174,174,'en_us','swisher-office-manager',NULL,1,'2016-02-05 20:39:57','2016-02-05 20:39:57','3d846dd8-9b5c-4bfd-8d57-ed5e434437da'),
	(175,175,'en_us','trucker',NULL,1,'2016-02-05 20:39:58','2016-02-05 20:39:58','6fa3aede-6d97-488b-957b-77fcf6aea274'),
	(176,176,'en_us','',NULL,1,'2016-02-05 22:32:51','2016-02-05 22:38:20','522101dc-b62f-474c-9615-4e33157db764'),
	(177,177,'en_us','',NULL,1,'2016-02-05 22:32:51','2016-02-05 22:38:20','409cd23d-dd9d-49a1-ac91-05ded97b87c9'),
	(178,178,'en_us','',NULL,1,'2016-02-05 22:37:36','2016-02-05 22:37:36','e1f4929c-8e05-42b8-9bc8-fc27eef64f96'),
	(179,179,'en_us','',NULL,1,'2016-02-05 22:37:36','2016-02-05 22:37:36','18b4443e-9b05-4423-a805-56d50d686af0'),
	(180,180,'en_us','',NULL,1,'2016-02-05 22:37:36','2016-02-05 22:37:36','337a295d-0e31-4ce0-aaaa-70b2a85e8c9c'),
	(181,181,'en_us','',NULL,1,'2016-02-05 22:40:23','2016-02-05 22:40:23','1ecb0358-10da-4a2c-82c4-b82c1aef2b79'),
	(182,182,'en_us','',NULL,1,'2016-02-05 22:40:23','2016-02-05 22:40:23','e3c5de04-e13c-47b0-b40b-565901b65313'),
	(183,183,'en_us','',NULL,1,'2016-02-05 22:40:23','2016-02-05 22:40:23','baa33828-b4ac-4e90-bd4c-d70681a0b5ae'),
	(184,184,'en_us','',NULL,1,'2016-02-05 22:42:30','2016-02-05 22:42:30','32499bef-6211-47c4-8165-58ef1994464e'),
	(185,185,'en_us','',NULL,1,'2016-02-05 22:42:30','2016-02-05 22:42:30','96717fab-be6b-4e43-a448-d1e0e4f8b3d9'),
	(186,186,'en_us','',NULL,1,'2016-02-05 22:42:30','2016-02-05 22:42:30','8e1ebac8-95ea-4afb-86c9-438b4af424a7');

/*!40000 ALTER TABLE `elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table emailmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailmessages`;

CREATE TABLE `emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `emailmessages_locale_fk` (`locale`),
  CONSTRAINT `emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entries`;

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_fk` (`authorId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;

INSERT INTO `entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,NULL,'2015-11-18 21:02:51',NULL,'2015-11-18 21:02:52','2016-02-05 15:32:53','eb2b37be-0dfe-42e1-92c2-6d32f1825c33'),
	(4,3,3,NULL,'2015-11-19 21:05:40',NULL,'2015-11-18 22:38:05','2016-02-01 20:55:03','36e511d9-7463-4610-91c3-5a3e18c35730'),
	(5,4,4,1,'2015-11-18 22:39:00',NULL,'2015-11-18 22:39:29','2016-01-27 15:57:30','496ae5eb-30ff-440b-a1ad-cb7fa621cb35'),
	(7,4,4,1,'2015-11-18 22:39:00',NULL,'2015-11-18 22:39:48','2016-02-04 16:22:24','bb0fbf81-cc70-4e79-bf56-11c624420f6f'),
	(8,5,NULL,NULL,'2015-11-18 22:40:12',NULL,'2015-11-18 22:40:12','2016-02-04 15:09:22','941ca49e-4489-4872-9d0f-c32bb6c83c2f'),
	(14,7,NULL,NULL,'2015-11-18 22:41:55',NULL,'2015-11-18 22:41:55','2016-02-04 14:28:30','881dc1e0-7894-4ef2-80a2-0d60226ab430'),
	(15,9,9,NULL,'2016-01-27 16:29:03',NULL,'2015-11-18 22:43:52','2016-02-01 20:39:37','e5b57842-f99e-4a5e-80dc-3d2e8f4cedde'),
	(16,11,11,NULL,'2015-11-19 21:07:29',NULL,'2015-11-18 22:44:37','2016-02-05 22:48:08','3f79cbe6-de32-4a0e-b009-d7a0d52f8691'),
	(17,10,10,1,'2015-11-18 22:44:00',NULL,'2015-11-18 22:44:50','2016-02-05 15:35:35','1c8caa03-8f30-41f7-89e2-b4e6008d6409'),
	(18,10,10,1,'2015-11-18 22:44:00',NULL,'2015-11-18 22:44:55','2016-02-01 20:36:54','ced75b78-0dab-49b3-8021-3ee928e0158a'),
	(19,10,10,1,'2015-11-18 22:45:00',NULL,'2015-11-18 22:45:02','2016-02-05 15:36:36','cd999ff1-a5df-4674-bdce-bd26e8eced85'),
	(84,4,4,1,'2016-01-27 17:37:15',NULL,'2016-01-27 17:37:15','2016-01-27 17:37:15','cf9f0462-370b-4d85-b8f2-3182bd270aa6'),
	(114,10,10,1,'2016-02-03 22:46:29',NULL,'2016-02-03 22:46:29','2016-02-03 22:46:29','cf353df5-86de-437e-924e-a14615c45ff6'),
	(137,10,10,1,'2016-02-05 15:41:29',NULL,'2016-02-05 15:41:29','2016-02-05 15:41:29','9c21bd40-36f6-4040-9d55-c165fa6efb5e');

/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entrydrafts`;

CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `entrydrafts_sectionId_fk` (`sectionId`),
  KEY `entrydrafts_creatorId_fk` (`creatorId`),
  KEY `entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entrytypes`;

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_fk` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;

INSERT INTO `entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,167,'Homepage','homepage',1,'Page Title',NULL,NULL,'2015-11-18 21:02:51','2016-02-03 19:43:13','4bc4b7ff-efae-4012-a22f-96caa58dca6b'),
	(2,2,157,'News','news',1,'Page Title',NULL,NULL,'2015-11-18 21:02:52','2016-01-25 16:59:45','71206a31-72a0-4f24-8b2d-ac999667cb7f'),
	(3,3,110,'About Us','aboutUs',1,'Page Title',NULL,NULL,'2015-11-18 22:38:05','2015-12-09 20:30:35','7b161077-a260-4e88-860e-96c375d03708'),
	(4,4,116,'About','about',1,'Page Title',NULL,NULL,'2015-11-18 22:38:20','2015-12-09 20:38:02','a87624dc-b0a0-46fd-9d69-86a233da7005'),
	(5,5,121,'Locations','locations',1,'Page Title',NULL,NULL,'2015-11-18 22:40:12','2015-12-09 20:49:26','437fcf86-aa77-430c-8f75-d61062284f81'),
	(7,7,117,'Careers','careers',1,'Page Title',NULL,NULL,'2015-11-18 22:41:55','2015-12-09 20:38:18','626874d6-ee8c-4ad6-bb13-25d5aa64569b'),
	(8,8,35,'Career','career',1,'Position Title',NULL,NULL,'2015-11-18 22:42:26','2015-11-30 16:05:45','967121e8-29d6-40dd-ad69-62fc401d2c59'),
	(9,9,122,'Stewardship Home','SustainabilityHome',1,'Page Title',NULL,NULL,'2015-11-18 22:43:52','2016-01-27 16:15:21','b1093109-02f1-40d5-af4e-9f818c1facca'),
	(10,10,162,'Stewardship','stewardship',1,'',NULL,NULL,'2015-11-18 22:44:09','2016-01-27 16:32:20','4fe0c04e-e027-479c-b959-9d283120bd14'),
	(11,11,175,'Contact Us','contactUs',1,'Page Title',NULL,NULL,'2015-11-18 22:44:37','2016-02-04 16:41:40','8cd7ff25-af9a-4aac-a7ab-7756057e72fe'),
	(12,12,118,'Contact','contact',1,'Page Title',NULL,NULL,'2015-11-19 21:07:47','2015-12-09 20:41:32','8775a16e-da48-4c90-967b-2b3777551446'),
	(14,2,165,'News Asset','newsAsset',1,'Title',NULL,NULL,'2016-02-03 19:12:11','2016-02-03 19:12:48','caf25f22-2053-4876-a92c-81ca4e150761');

/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entryversions`;

CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `entryversions_sectionId_fk` (`sectionId`),
  KEY `entryversions_creatorId_fk` (`creatorId`),
  KEY `entryversions_locale_fk` (`locale`),
  CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entryversions` WRITE;
/*!40000 ALTER TABLE `entryversions` DISABLE KEYS */;

INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,4,3,1,'en_us',1,NULL,'{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447886285,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:38:05','2015-11-18 22:38:05','05035c96-43cc-4228-b02e-edcf7e5cb8d2'),
	(2,5,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"History\\/Legacy\",\"slug\":\"history-legacy\",\"postDate\":1447886369,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:39:29','2015-11-18 22:39:29','967782cd-8d7e-4ceb-a0ba-f418f047a7cb'),
	(4,7,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Affilliations\",\"slug\":\"affilliations\",\"postDate\":1447886388,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:39:48','2015-11-18 22:39:48','8be979fc-38c2-454d-8631-d6fda909ba04'),
	(5,8,5,1,'en_us',1,NULL,'{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Locations\",\"slug\":\"locations\",\"postDate\":1447886412,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:40:12','2015-11-18 22:40:12','ef8fbd01-112c-4308-89d2-76e787920e06'),
	(11,14,7,1,'en_us',1,NULL,'{\"typeId\":\"7\",\"authorId\":null,\"title\":\"Careers\",\"slug\":\"careers\",\"postDate\":1447886515,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:41:55','2015-11-18 22:41:55','9e610332-727b-483d-8fea-4134f0aa9e0c'),
	(12,15,9,1,'en_us',1,NULL,'{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Sustainability\",\"slug\":\"sustainability\",\"postDate\":1447886632,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:43:52','2015-11-18 22:43:52','586743bd-e029-4054-bc66-825b66ccdc59'),
	(13,16,11,1,'en_us',1,NULL,'{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447886677,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:44:37','2015-11-18 22:44:37','48d7922e-eee7-4b42-aa6e-b0d0d5841ba7'),
	(14,17,10,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Animal Health\\/BQA\",\"slug\":\"animal-health-bqa\",\"postDate\":1447886690,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:44:50','2015-11-18 22:44:50','b6cea848-1317-43b4-b837-854da4c4e8ba'),
	(15,18,10,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Community\",\"slug\":\"community\",\"postDate\":1447886695,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:44:55','2015-11-18 22:44:55','6d6ec8e1-5470-44f6-bf5f-585115e843bd'),
	(16,19,10,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"ENOUGH Movement\",\"slug\":\"enough-movement\",\"postDate\":1447886702,\"expiryDate\":null,\"enabled\":1,\"fields\":[]}','2015-11-18 22:45:02','2015-11-18 22:45:02','253f1a76-56f9-4c7e-b7b0-84737fb6e6e0'),
	(17,2,1,1,'en_us',1,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Friona Industries!\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.frionaindustries.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2015-11-18 22:45:32','2015-11-18 22:45:32','5f24d5a5-8809-477a-9b0d-88b92f19d23d'),
	(24,5,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"History\\/Legacy\",\"slug\":\"history-legacy\",\"postDate\":1447886340,\"expiryDate\":null,\"enabled\":1,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat mauris ut ultrices commodo. Praesent vel gravida tortor, sed porttitor ligula. Praesent imperdiet nibh eleifend ligula viverra, ac volutpat urna rhoncus. Aliquam congue vulputate auctor. Vestibulum bibendum pulvinar viverra. Vivamus imperdiet hendrerit dictum. Mauris pellentesque interdum ante, vel semper mi molestie id. Sed tristique laoreet nunc, eget pulvinar eros pharetra tempus.<\\/p><p>Nunc pharetra, urna vitae cursus vulputate, tortor lectus cursus elit, eget sodales nisi ligula at lectus. Praesent imperdiet diam a sodales euismod. Sed facilisis scelerisque libero, suscipit cursus purus mollis nec. Quisque auctor sem mauris, vitae sollicitudin nunc cursus sit amet. Ut ut auctor turpis. Morbi lacinia eros sed lectus tincidunt egestas. Fusce turpis justo, egestas elementum purus eu, consectetur fermentum felis.<\\/p>\"}}','2015-11-20 15:03:05','2015-11-20 15:03:05','edebe539-5b8f-41ea-873c-ad74001e9daf'),
	(26,7,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Affilliations\",\"slug\":\"affilliations\",\"postDate\":1447886340,\"expiryDate\":null,\"enabled\":1,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat mauris ut ultrices commodo. Praesent vel gravida tortor, sed porttitor ligula. Praesent imperdiet nibh eleifend ligula viverra, ac volutpat urna rhoncus. Aliquam congue vulputate auctor. Vestibulum bibendum pulvinar viverra. Vivamus imperdiet hendrerit dictum. Mauris pellentesque interdum ante, vel semper mi molestie id. Sed tristique laoreet nunc, eget pulvinar eros pharetra tempus.<\\/p><p>Nunc pharetra, urna vitae cursus vulputate, tortor lectus cursus elit, eget sodales nisi ligula at lectus. Praesent imperdiet diam a sodales euismod. Sed facilisis scelerisque libero, suscipit cursus purus mollis nec. Quisque auctor sem mauris, vitae sollicitudin nunc cursus sit amet. Ut ut auctor turpis. Morbi lacinia eros sed lectus tincidunt egestas. Fusce turpis justo, egestas elementum purus eu, consectetur fermentum felis.<\\/p>\"}}','2015-11-20 15:03:16','2015-11-20 15:03:16','433d9f0c-1805-4ef1-bfab-5b6ff9f8371b'),
	(27,14,7,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Careers\",\"slug\":\"careers\",\"postDate\":1447886515,\"expiryDate\":null,\"enabled\":1,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut ornare quam. Praesent et mi eu magna tristique molestie at id erat. Curabitur eget dictum dui, fermentum luctus leo. Nunc at mi massa. Suspendisse fermentum consequat elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae ipsum ante. Integer ultrices pellentesque est eget bibendum.<\\/p>\"}}','2015-11-20 19:45:28','2015-11-20 19:45:28','cc03751f-7f51-41c5-9554-d18430b802bf'),
	(37,5,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"History\\/Legacy\",\"slug\":\"history\",\"postDate\":1447886340,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat mauris ut ultrices commodo. Praesent vel gravida tortor, sed porttitor ligula. Praesent imperdiet nibh eleifend ligula viverra, ac volutpat urna rhoncus. Aliquam congue vulputate auctor. Vestibulum bibendum pulvinar viverra. Vivamus imperdiet hendrerit dictum. Mauris pellentesque interdum ante, vel semper mi molestie id. Sed tristique laoreet nunc, eget pulvinar eros pharetra tempus.<\\/p><p>Nunc pharetra, urna vitae cursus vulputate, tortor lectus cursus elit, eget sodales nisi ligula at lectus. Praesent imperdiet diam a sodales euismod. Sed facilisis scelerisque libero, suscipit cursus purus mollis nec. Quisque auctor sem mauris, vitae sollicitudin nunc cursus sit amet. Ut ut auctor turpis. Morbi lacinia eros sed lectus tincidunt egestas. Fusce turpis justo, egestas elementum purus eu, consectetur fermentum felis.<\\/p>\"}}','2015-11-30 14:54:11','2015-11-30 14:54:11','6a71e3e5-2a30-4022-9a44-b7262ea8cef0'),
	(39,4,3,1,'en_us',2,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tincidunt semper massa a elementum. Aliquam molestie eros at nisi ultrices auctor. Nullam vestibulum luctus arcu id bibendum. Praesent quis nibh tellus. Phasellus bibendum scelerisque iaculis. Aenean volutpat nibh volutpat vestibulum semper. Integer malesuada sodales erat, faucibus eleifend nulla sodales sed. Quisque pellentesque quis leo nec fermentum. Quisque condimentum neque sit amet urna cursus, at molestie massa imperdiet.<\\/p>\"}}','2015-11-30 14:56:41','2015-11-30 14:56:41','62f25086-538d-41b6-ac76-b7dd0684cdb6'),
	(40,15,9,1,'en_us',2,'','{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Sustainability Home\",\"slug\":\"sustainability\",\"postDate\":1447886655,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tincidunt semper massa a elementum. Aliquam molestie eros at nisi ultrices auctor. Nullam vestibulum luctus arcu id bibendum. Praesent quis nibh tellus. Phasellus bibendum scelerisque iaculis. Aenean volutpat nibh volutpat vestibulum semper. Integer malesuada sodales erat, faucibus eleifend nulla sodales sed. Quisque pellentesque quis leo nec fermentum. Quisque condimentum neque sit amet urna cursus, at molestie massa imperdiet.<\\/p><p>Nullam vel tempor diam. Ut ullamcorper suscipit ex sed blandit. Ut ac hendrerit orci. Vestibulum urna ante, hendrerit vitae ornare quis, aliquam nec neque. Suspendisse a tempus lacus, ut porta eros. Phasellus lectus lectus, tempus vitae metus sit amet, interdum aliquam augue. Sed id mollis leo, id ullamcorper mi. Maecenas ultrices vitae ex luctus vulputate. Proin dolor elit, hendrerit ac enim sit amet, iaculis laoreet nibh. Nunc id neque id elit vestibulum tempus. Proin vehicula mauris sed orci facilisis, ac ullamcorper neque consectetur.<\\/p>\"}}','2015-11-30 15:43:32','2015-11-30 15:43:32','d841a9a2-a466-4fbc-bd0f-3d7052db0ebd'),
	(41,17,10,1,'en_us',2,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Animal Health\\/BQA\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Nullam aliquet orci sed sem feugiat, quis gravida nunc tempus. Praesent ut dapibus tortor. Integer ut eleifend urna. Maecenas viverra semper orci in rutrum. Curabitur volutpat blandit diam quis porta. Sed maximus sem a varius fringilla. Sed bibendum tempus sapien, malesuada gravida urna aliquam nec. Mauris accumsan, sem et placerat pharetra, odio lorem viverra quam, ac tempor enim dolor at sem. Nulla pharetra libero ut efficitur ornare. Phasellus a odio a nulla venenatis vulputate eu et libero. Duis et magna a lacus interdum posuere ut vel orci. Donec purus diam, scelerisque id felis nec, posuere vulputate magna.<\\/p>\"}}','2015-11-30 15:46:30','2015-11-30 15:46:30','bb69bc8c-0ec8-49f8-9cf0-5812447e3697'),
	(42,19,10,1,'en_us',2,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"ENOUGH Movement\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Ut ex nisl, dictum nec finibus vitae, commodo eget leo. Aliquam erat volutpat. Sed a laoreet massa. Vivamus sit amet laoreet risus. Cras massa augue, condimentum vitae tellus vel, dignissim auctor neque. Etiam ut hendrerit urna. Duis ut ex id mauris aliquam egestas.<\\/p><p>Nullam aliquet orci sed sem feugiat, quis gravida nunc tempus. Praesent ut dapibus tortor. Integer ut eleifend urna. Maecenas viverra semper orci in rutrum. Curabitur volutpat blandit diam quis porta. Sed maximus sem a varius fringilla. Sed bibendum tempus sapien, malesuada gravida urna aliquam nec. Mauris accumsan, sem et placerat pharetra, odio lorem viverra quam, ac tempor enim dolor at sem. Nulla pharetra libero ut efficitur ornare. Phasellus a odio a nulla venenatis vulputate eu et libero. Duis et magna a lacus interdum posuere ut vel orci. Donec purus diam, scelerisque id felis nec, posuere vulputate magna.<\\/p>\"}}','2015-11-30 15:46:49','2015-11-30 15:46:49','0860df65-5265-48d0-9a9e-bae6fd895d5a'),
	(43,16,11,1,'en_us',2,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Ut tincidunt posuere rhoncus. Ut interdum tincidunt neque, eu blandit urna tempus eget. Mauris tempus, nulla vitae luctus porttitor, urna lacus dapibus leo, sed tristique ex arcu in augue. Curabitur gravida velit eleifend risus iaculis, vitae feugiat leo tincidunt. Sed pharetra ullamcorper enim quis blandit. Ut fermentum, elit id vestibulum lobortis, leo erat varius mauris, eu dictum justo ligula at velit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.<\\/p>\"}}','2015-11-30 15:54:28','2015-11-30 15:54:28','a9346ba3-efe8-4a3e-992b-602f3603be48'),
	(44,8,5,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Locations\",\"slug\":\"locations\",\"postDate\":1447886412,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel sollicitudin sapien, in commodo nisl. Vestibulum id mi id magna tincidunt vehicula. Praesent accumsan erat non dui commodo, et congue purus sollicitudin. Praesent non sollicitudin lectus, in lobortis odio. Nulla eget porta neque, ut ultricies tortor. Aliquam gravida elit ligula, quis blandit velit ullamcorper sodales. Donec nec fermentum massa. In eu nulla a dolor laoreet egestas vel in nibh. Donec eget blandit massa, quis aliquam risus. Fusce urna dolor, maximus vitae ligula ultricies, porta sodales sapien. Nam ut ligula nisi. Sed nec risus et nisi faucibus bibendum non a ligula. Pellentesque eu turpis metus.<\\/p>\",\"8\":\"\"}}','2015-12-07 14:50:12','2015-12-07 14:50:12','c6144b96-87aa-4e02-9279-3af3fc9c9796'),
	(75,2,1,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Our Mission<br>DEDICATED EMPLOYEES.INNOVATIVE BEEF. SATISFIED CONSUMER<\\/p>\",\"8\":\"\"}}','2015-12-09 19:30:06','2015-12-09 19:30:06','a8073903-8891-47c6-820f-2274def6dcfc'),
	(76,2,1,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Our Mission<\\/p><h2><br>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h2>\"}}},\"8\":\"\"}}','2015-12-09 19:40:38','2015-12-09 19:40:38','79d94bbd-9cef-441b-b47d-6347e26b5ee5'),
	(77,2,1,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"43\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h2><br>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h2>\"}}},\"8\":\"\"}}','2015-12-09 19:41:10','2015-12-09 19:41:10','3bb27177-d4f5-4679-9738-cbf7369188da'),
	(78,2,1,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"43\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h2><br>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h2>\"}}},\"8\":\"\"}}','2015-12-09 19:42:39','2015-12-09 19:42:39','e9cfd731-d3a9-49e2-8daa-c41af8867e5b'),
	(79,4,3,1,'en_us',3,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"8\":\"\"}}','2015-12-09 20:30:20','2015-12-09 20:30:20','778fd173-0144-449c-aadd-1cc60d0cc0d0'),
	(80,4,3,1,'en_us',4,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tincidunt semper massa a elementum. Aliquam molestie eros at nisi ultrices auctor. Nullam vestibulum luctus arcu id bibendum. Praesent quis nibh tellus. Phasellus bibendum scelerisque iaculis. Aenean volutpat nibh volutpat vestibulum semper. Integer malesuada sodales erat, faucibus eleifend nulla sodales sed. Quisque pellentesque quis leo nec fermentum. Quisque condimentum neque sit amet urna cursus, at molestie massa imperdiet.<\\/p>\"}}},\"8\":\"\"}}','2015-12-09 20:31:25','2015-12-09 20:31:25','d2e62b0b-44d4-48f0-a695-6c11995bb7d2'),
	(81,4,3,1,'en_us',5,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"44\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tincidunt semper massa a elementum. Aliquam molestie eros at nisi ultrices auctor. Nullam vestibulum luctus arcu id bibendum. Praesent quis nibh tellus. Phasellus bibendum scelerisque iaculis. Aenean volutpat nibh volutpat vestibulum semper. Integer malesuada sodales erat, faucibus eleifend nulla sodales sed. Quisque pellentesque quis leo nec fermentum. Quisque condimentum neque sit amet urna cursus, at molestie massa imperdiet.<\\/p>\"}},\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"contentHeadline\":\"Howdy!\"}}},\"8\":\"\"}}','2015-12-09 20:34:16','2015-12-09 20:34:16','c3a0b340-042a-41fb-b6ce-827d635c724e'),
	(82,4,3,1,'en_us',6,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"44\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tincidunt semper massa a elementum. Aliquam molestie eros at nisi ultrices auctor. Nullam vestibulum luctus arcu id bibendum. Praesent quis nibh tellus. Phasellus bibendum scelerisque iaculis. Aenean volutpat nibh volutpat vestibulum semper. Integer malesuada sodales erat, faucibus eleifend nulla sodales sed. Quisque pellentesque quis leo nec fermentum. Quisque condimentum neque sit amet urna cursus, at molestie massa imperdiet.<\\/p>\"}}},\"8\":\"\"}}','2015-12-09 20:34:29','2015-12-09 20:34:29','1716c160-099f-4a11-80c4-f9dc3044ce63'),
	(83,16,11,1,'en_us',3,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"8\":\"\"}}','2015-12-09 20:42:48','2015-12-09 20:42:48','835e2686-c0fe-4ef1-97a1-cda3bb115b34'),
	(84,16,11,1,'en_us',4,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra, velit vel gravida interdum, tortor orci luctus elit, vitae finibus urna libero at est. Cras in leo ex. Nunc congue lectus diam, ut fringilla metus convallis vitae. Sed vitae interdum urna. Cras lobortis, diam ut tincidunt mollis, est ante euismod erat, vel aliquet leo nisi in mauris. Maecenas convallis auctor nisi, sed blandit mauris ornare in. Proin ac magna eleifend, fermentum augue ac, faucibus est. Integer a elit in turpis congue faucibus eu id odio. Etiam et nisl sodales, iaculis leo a, interdum augue. Fusce egestas sodales libero, eu porttitor ex consectetur in. Maecenas leo magna, sodales sit amet efficitur a, blandit nec purus.<\\/p>\"}}},\"8\":\"\"}}','2015-12-09 20:48:53','2015-12-09 20:48:53','58c90c6d-30a0-4486-b5dd-1ad6dbae0d05'),
	(85,8,5,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Locations\",\"slug\":\"locations\",\"postDate\":1447886412,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel sollicitudin sapien, in commodo nisl. Vestibulum id mi id magna tincidunt vehicula. Praesent accumsan erat non dui commodo, et congue purus sollicitudin. Praesent non sollicitudin lectus, in lobortis odio. Nulla eget porta neque, ut ultricies tortor. Aliquam gravida elit ligula, quis blandit velit ullamcorper sodales. Donec nec fermentum massa. In eu nulla a dolor laoreet egestas vel in nibh. Donec eget blandit massa, quis aliquam risus. Fusce urna dolor, maximus vitae ligula ultricies, porta sodales sapien. Nam ut ligula nisi. Sed nec risus et nisi faucibus bibendum non a ligula. Pellentesque eu turpis metus.<\\/p>\"}}},\"8\":\"\"}}','2015-12-09 20:49:35','2015-12-09 20:49:35','fbc8b2ce-8f2b-44b5-a5c4-5841a1b68fc4'),
	(86,15,9,1,'en_us',3,'','{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Sustainability Home\",\"slug\":\"sustainability\",\"postDate\":1447886655,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"8\":\"\"}}','2015-12-09 20:52:12','2015-12-09 20:52:12','4a8ddde3-bd54-492e-96ba-d4e310ee7a27'),
	(87,15,9,1,'en_us',4,'','{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Sustainability Home\",\"slug\":\"sustainability\",\"postDate\":1447886655,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"contentBody\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tincidunt semper massa a elementum. Aliquam molestie eros at nisi ultrices auctor. Nullam vestibulum luctus arcu id bibendum. Praesent quis nibh tellus. Phasellus bibendum scelerisque iaculis. Aenean volutpat nibh volutpat vestibulum semper. Integer malesuada sodales erat, faucibus eleifend nulla sodales sed. Quisque pellentesque quis leo nec fermentum. Quisque condimentum neque sit amet urna cursus, at molestie massa imperdiet.<\\/p><p><br>Nullam vel tempor diam. Ut ullamcorper suscipit ex sed blandit. Ut ac hendrerit orci. Vestibulum urna ante, hendrerit vitae ornare quis, aliquam nec neque. Suspendisse a tempus lacus, ut porta eros. Phasellus lectus lectus, tempus vitae metus sit amet, interdum aliquam augue. Sed id mollis leo, id ullamcorper mi. Maecenas ultrices vitae ex luctus vulputate. Proin dolor elit, hendrerit ac enim sit amet, iaculis laoreet nibh. Nunc id neque id elit vestibulum tempus. Proin vehicula mauris sed orci facilisis, ac ullamcorper neque consectetur.<\\/p>\"}}},\"8\":\"\"}}','2015-12-09 20:52:35','2015-12-09 20:52:35','aeb4d7c5-8cd2-4c4f-a8b4-dcf555ba026a'),
	(89,2,1,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"43\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h2><br>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h2>\"}}},\"8\":\"\"}}','2016-01-25 16:45:36','2016-01-25 16:45:36','dc348b19-7c00-4e36-8b0e-95a53fce3ca9'),
	(90,2,1,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"\",\"8\":\"\"}}','2016-01-25 16:52:41','2016-01-25 16:52:41','68ad1058-c31b-4938-8d11-6b3bb6e86af7'),
	(91,2,1,1,'en_us',8,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"\",\"8\":\"\"}}','2016-01-25 16:52:42','2016-01-25 16:52:42','c49f510d-e583-485e-9036-da8736c00d82'),
	(92,2,1,1,'en_us',9,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n<h2>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h2>\"}}},\"8\":\"\"}}','2016-01-25 16:53:01','2016-01-25 16:53:01','5e89f016-0490-454a-a86e-bf8bacd1fb0a'),
	(93,2,1,1,'en_us',10,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\"}}','2016-01-25 16:55:16','2016-01-25 16:55:16','e1b3c14f-9d05-4e50-a1ee-64f59e39d79d'),
	(109,4,3,1,'en_us',7,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"44\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>For more than 50 years, Friona Industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors: our livestock and our customers.<\\/p>\\r\\n\\r\\n<p>We built this company in the north Texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship. We answer that expectation through innovative beef production and a reputation for exceeding customer expectations.<\\/p>\\r\\n\\r\\n<p>At Friona Industries, quality means a fierce attention to detail from every employee on every job. To back that up, here is our Code of Conduct&nbsp;promise that our employees sign every year.<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 15:40:56','2016-01-27 15:40:56','76004753-0694-4c26-b25e-4235f1a012f2'),
	(110,4,3,1,'en_us',8,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"OUR MISSION:\"}},\"new2\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"DEDICATED EMPLOYEES. INNOVATIVE BEEF. SATISFIED CUSTOMER.\"}},\"44\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>For more than 50 years, Friona Industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors: our livestock and our customers.<\\/p>\\r\\n\\r\\n<p>We built this company in the north Texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship. We answer that expectation through innovative beef production and a reputation for exceeding customer expectations.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>At Friona Industries, quality means a fierce attention to detail from every employee on every job. To back that up, here is our Code of Conduct&nbsp;promise that our employees sign every year.<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 15:42:44','2016-01-27 15:42:44','54d0124b-ac1f-4c18-a304-a6b23aca481e'),
	(111,4,3,1,'en_us',9,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"72\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"OUR MISSION:\"}},\"73\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"DEDICATED EMPLOYEES. INNOVATIVE BEEF. SATISFIED CUSTOMER.\"}},\"44\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>For more than 50 years, Friona Industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors: our livestock and our customers.<\\/p>\\r\\n\\r\\n<p>We built this company in the north Texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship. We answer that expectation through innovative beef production and a reputation for exceeding customer expectations.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>At Friona Industries, quality means a fierce attention to detail from every employee on every job. To back that up, here is our Code of Conduct&nbsp;promise that our employees sign every year.<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 15:50:19','2016-01-27 15:50:19','4a0d93d9-9840-45ca-8842-356c96e477fe'),
	(112,14,7,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Careers\",\"slug\":\"careers\",\"postDate\":1447886515,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis rutrum dolor. Nulla maximus turpis at euismod pellentesque. Donec sit amet augue ut felis consectetur vestibulum. Nam hendrerit turpis eu lorem faucibus, ut ornare diam semper.&nbsp;<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 15:56:18','2016-01-27 15:56:18','4e2dbdf0-734b-4b6f-8c9c-7bc14cbe29d9'),
	(113,5,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"History\\/Legacy\",\"slug\":\"history\",\"postDate\":1447886340,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L. P. began as single small feedyard in Amarillo, TX in 1962 as the beef industry began to recognize the resources that existed in the Texas panhandle. Founding partners, like A.L. Black, were a group of cattle feeders who could see beyond the cattle to all the other businesses that cattle feeding supported: meat processing, feed manufacturing, and trucking, to name just a few.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>We have progressed to become one of the leading cattle feeding businesses in the world, led by visionaries such as Mr. Black and <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=kG-TbsbPTSw\\\" target=\\\"true\\\">James Herring<\\/a>, both winners of the <a href=\\\"http:\\/\\/ranchingheritage.org\\/golden-spur-award\\/\\\" target=\\\"true\\\">National Golden Spur Award<\\/a>, which honors outstanding achievement in ranching and livestock businesses. James Herring especially bolstered our business, and his work at Friona Industries led to <a href=\\\"https:\\/\\/hbr.org\\/search?term=friona\\\" target=\\\"true\\\">two Harvard School of Business Case Studies<\\/a>&nbsp;on management and marketing.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Most recently, Mr. Herring was honored with the Feeding Quality Forum\\u2019s <a href=\\\"http:\\/\\/www.agrimarketing.com\\/s\\/97970\\\" target=\\\"true\\\">2015 Industry Achievement Award<\\/a>.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Today, privately held Friona Industries has <a href=\\\"http:\\/\\/frionaindustries.sandboxdsm.com\\/locations#entry:8\\\">four state-of-the-art feedyards<\\/a>&nbsp;in north Texas, with a feeding capacity that ranks us in the top 10 feedyards worldwide. We continue to focus on a vertically aligned production system that creates a consistent, safe, tender and flavorful beef for branded product lines marketed in 2,300 retails stores in the U.S.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>As our leaders at Friona Industries look forward, we recognize the challenges that face us as we address our stewardship responsibilities of both livestock and the land, while delivering high quality beef to our customers.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Our mission statement communicates our belief that our customers seek quality beef products produced with the highest standards of stewardship. We continue our legacy of answering that expectation through innovative beef production and a reputation for exceeding customer expectations<\\/p>\"}}},\"8\":\"Friona Industries, L. P. began as single small feedyard in Amarillo, TX in 1962 in the Texas panhandle. \"}}','2016-01-27 15:57:30','2016-01-27 15:57:30','35353c84-a1f1-466a-865b-6efe03c84306'),
	(114,15,9,1,'en_us',5,'','{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Stewardship\",\"slug\":\"sustainability\",\"postDate\":1447886655,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"48\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stewardship is an ethic that embodies the responsible planning and management of resources. At Friona Industries, we take stewardship seriously. You can see that in our Code of Conduct&nbsp;as well as in the <a href=\\\"http:\\/\\/www.bqa.org\\/about\\/value-to-the-industry\\\" target=\\\"true\\\">Beef Quality Assurance<\\/a>&nbsp;practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>A large part of the beef industry\\u2019s job involves making sure that beef is safe and wholesome for consumers. We work together with our cattle, animal health, and feed suppliers to maintain high standards for the beef we produce every day.<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 16:00:41','2016-01-27 16:00:41','0851c0f8-6593-463b-ac64-4fcc2893ccb7'),
	(115,15,9,1,'en_us',6,'','{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Stewardship\",\"slug\":\"sustainability\",\"postDate\":1447886655,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"48\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stewardship is an ethic that embodies the responsible planning and management of resources. At Friona Industries, we take stewardship seriously. You can see that in our Code of Conduct&nbsp;as well as in the <a href=\\\"http:\\/\\/www.bqa.org\\/about\\/value-to-the-industry\\\" target=\\\"true\\\">Beef Quality Assurance<\\/a>&nbsp;practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>A large part of the beef industry\\u2019s job involves making sure that beef is safe and wholesome for consumers. We work together with our cattle, animal health, and feed suppliers to maintain high standards for the beef we produce every day.<\\/p>\"}}},\"8\":\"A large part of the beef industry\\u2019s job involves making sure that beef is safe and wholesome for consumers.\"}}','2016-01-27 16:01:56','2016-01-27 16:01:56','38679173-1f5f-42f9-b7f8-1fa030a8cc3c'),
	(116,19,10,1,'en_us',3,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Stewardship\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>The <a href=\\\"https:\\/\\/www.enoughmovement.com\\/learn\\/index.aspx\\\" target=\\\"true\\\">ENOUGH Movement<\\/a>&nbsp;is a global community working together to ensure that each person has access to nutritious, affordable food, both today and in the coming decades. The global effort is being led by <a href=\\\"https:\\/\\/www.enoughmovement.com\\/report\\/\\\" target=\\\"true\\\">Elanco<\\/a>, one of our business partners.<\\/p>\",\"8\":\"\"}}','2016-01-27 16:19:10','2016-01-27 16:19:10','20bbd50e-e09c-43e3-889a-4bf3e4ddfad5'),
	(117,17,10,1,'en_us',3,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Stewardship\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"19\",\"fields\":{\"1\":\"<p>Friona Industries is one of the founding members of the <a href=\\\"https:\\/\\/www.usrsb.org\\/about.aspx\\\" target=\\\"true\\\">U.S. Roundtable on Sustainable Beef<\\/a>&nbsp;(USRSB), which is a multi-stakeholder initiative developed to advance, support and communicate continuous improvement in sustainability of the U.S. beef value chain.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Our representative to the group is Blaine Rotramel, our commodity procurement manager at Friona Industries. \\u201cWe\\u2019re here to feed the world,\\u201d Rotramel says. \\u201cWe\\u2019re using our knowledge to look at better ways to do things in our business.\\u201d<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>USRSB focuses on four areas: Social (employees involved in the community), Economic (community impact), Environmental and Animal Health & Welfare.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries is an active supporter in each of those areas. For example, Animal Health & Welfare are addressed through many of the BQA activities&nbsp;at each of the feedyards; and a recent economic impact study showed the Randall County Feedyard provided $7 million in business activity within a 100-mile radius of the location.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>The USRSB is working in collaboration with the <a href=\\\"http:\\/\\/grsbeef.org\\/\\\" target=\\\"true\\\">Global Roundtable for Sustainable Beef<\\/a>&nbsp;(GRSB) and other sustainability initiatives whose aims are consistent with its mission and vison.<\\/p>\",\"8\":\"\"}}','2016-01-27 16:23:59','2016-01-27 16:23:59','dd9dc385-c3f0-41a0-acab-a9fd46ad5d31'),
	(118,17,10,1,'en_us',4,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Stewardship\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Friona Industries is one of the founding members of the <a href=\\\"https:\\/\\/www.usrsb.org\\/about.aspx\\\" target=\\\"true\\\">U.S. Roundtable on Sustainable Beef<\\/a>&nbsp;(USRSB), which is a multi-stakeholder initiative developed to advance, support and communicate continuous improvement in sustainability of the U.S. beef value chain.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Our representative to the group is Blaine Rotramel, our commodity procurement manager at Friona Industries. \\u201cWe\\u2019re here to feed the world,\\u201d Rotramel says. \\u201cWe\\u2019re using our knowledge to look at better ways to do things in our business.\\u201d<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>USRSB focuses on four areas: Social (employees involved in the community), Economic (community impact), Environmental and Animal Health & Welfare.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries is an active supporter in each of those areas. For example, Animal Health & Welfare are addressed through many of the BQA activities&nbsp;at each of the feedyards; and a recent economic impact study showed the Randall County Feedyard provided $7 million in business activity within a 100-mile radius of the location.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The USRSB is working in collaboration with the <a href=\\\"http:\\/\\/grsbeef.org\\/\\\" target=\\\"true\\\">Global Roundtable for Sustainable Beef<\\/a>&nbsp;(GRSB) and other sustainability initiatives whose aims are consistent with its mission and vison.<\\/p>\",\"8\":\"\"}}','2016-01-27 16:24:09','2016-01-27 16:24:09','8173a6e3-b586-4504-9338-3508d3c11384'),
	(119,17,10,1,'en_us',5,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Stewardship\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Friona Industries is one of the founding members of the <a href=\\\"https:\\/\\/www.usrsb.org\\/about.aspx\\\" target=\\\"true\\\">U.S. Roundtable on Sustainable Beef<\\/a>&nbsp;(USRSB), which is a multi-stakeholder initiative developed to advance, support and communicate continuous improvement in sustainability of the U.S. beef value chain.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Our representative to the group is Blaine Rotramel, our commodity procurement manager at Friona Industries. \\u201cWe\\u2019re here to feed the world,\\u201d Rotramel says. \\u201cWe\\u2019re using our knowledge to look at better ways to do things in our business.\\u201d<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>USRSB focuses on four areas: Social (employees involved in the community), Economic (community impact), Environmental and Animal Health & Welfare.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries is an active supporter in each of those areas. For example, Animal Health & Welfare are addressed through many of the BQA activities&nbsp;at each of the feedyards; and a recent economic impact study showed the Randall County Feedyard provided $7 million in business activity within a 100-mile radius of the location.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The USRSB is working in collaboration with the <a href=\\\"http:\\/\\/grsbeef.org\\/\\\" target=\\\"true\\\">Global Roundtable for Sustainable Beef<\\/a>&nbsp;(GRSB) and other sustainability initiatives whose aims are consistent with its mission and vison.<\\/p>\",\"8\":\"\"}}','2016-01-27 16:24:42','2016-01-27 16:24:42','1921a9df-43cc-45cb-94f9-d73e368f612d'),
	(120,14,7,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Careers\",\"slug\":\"careers\",\"postDate\":1447886515,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Working at Friona\"}},\"76\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The integrity of our highly motivated and dedicated employees and officers is one of the most valuable assets we have at Friona Industries. The foundation of our success and our ability to grow and take advantage of our resources and innovation is inspired by honesty and fairness through a culture of strong ethical behavior. <\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>New employees go through a training process so they understand how important their position is to cattle performance at the feedyard, as well as the financial performance of the company.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Each year, we ask our employees to agree to a Code of Conduct&nbsp;It explains our philosophical principles of employee conduct.<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 16:26:27','2016-01-27 16:26:27','f3c5f87f-3cf7-49ba-8958-0ddeb7925935'),
	(121,19,10,1,'en_us',4,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"ENOUGH\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":\"\",\"8\":\"\"}}','2016-01-27 16:32:43','2016-01-27 16:32:43','56c8c05e-841e-4b5b-bff8-a189fb943bd5'),
	(122,17,10,1,'en_us',6,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Animal Health\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":\"\",\"8\":\"\"}}','2016-01-27 16:33:04','2016-01-27 16:33:04','dbc9a8d4-dae5-4eea-a39f-8122d412be2f'),
	(124,14,7,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Careers\",\"slug\":\"careers\",\"postDate\":1447886515,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"78\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Working at Friona\"}},\"76\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The integrity of our highly motivated and dedicated employees and officers is one of the most valuable assets we have at Friona Industries. The foundation of our success and our ability to grow and take advantage of our resources and innovation is inspired by honesty and fairness through a culture of strong ethical behavior. <\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>New employees go through a training process so they understand how important their position is to cattle performance at the feedyard, as well as the financial performance of the company.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Each year, we ask our employees to agree to a Code of Conduct&nbsp;It explains our philosophical principles of employee conduct.<\\/p>\"}},\"new1\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Jobs at Friona\"}},\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>There are three general career areas at Friona Industries: Feedyard Management & Office Support, Feedyard Operations, and Feed Mill Operations. Every job at Friona is important to the overall care and performance of the cattle in our feedyards, as well as to the overall financial performance of the company.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona is always taking job applications&nbsp;for possible openings at our feedyards. Please click on the area below that you are interested in to see a general description and a printable job application that you can fill out and submit to Friona Industries.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Each year, we ask our employees to agree to a Code of Conduct.&nbsp;It explains our philosophical principles of employee conduct.<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 16:36:30','2016-01-27 16:36:30','f71f84f2-82e5-49ea-8ffc-cd1d7a962828'),
	(125,16,11,1,'en_us',5,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"new2\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L.P.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855 &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Key Contacts&nbsp;<\\/strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Brad Stout<br>Chief Operating Officer<\\/p><p>Clay Allen<br>Chief Financial Officer<\\/p><p>Russell Artho<br>Controller &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Cattle Procurement Manager&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jay Cortese<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Cattle Procurement Assistant Manager &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Brady Bressler<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Fed Cattle Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mark Hooker<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Assistant Fed Cattle Marketing Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Karli Schilling<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Commodity Procurement Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Blaine Rotramel<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Feedyard Operations Manager&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dell Volmer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Growyard & Pasture Cattle Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Eddie Derrick<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Manager of Animal Health&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tom Portillo, DVM<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 16:59:45','2016-01-27 16:59:45','39ca6427-1fa5-4a22-b17d-8caa15044d02'),
	(126,16,11,1,'en_us',6,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"83\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L.P.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855 &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Key Contacts&nbsp;<\\/strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brad Stout<br>Chief Operating Officer<\\/p>\\r\\n\\r\\n<p>Clay Allen<br>Chief Financial Officer<\\/p>\\r\\n\\r\\n<p>Russell Artho<br>Controller &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Jay Cortese<br><\\/p><p>Cattle Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brady Bressler<br><\\/p><p>Cattle Procurement Assistant Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>&nbsp; &nbsp; Mark Hooker<br><\\/p><p>Fed Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Assistant Fed Cattle Marketing Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Karli Schilling<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Commodity Procurement Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Blaine Rotramel<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Feedyard Operations Manager&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dell Volmer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Growyard & Pasture Cattle Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Eddie Derrick<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Manager of Animal Health&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tom Portillo, DVM<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 17:01:02','2016-01-27 17:01:02','5c5dabd4-eb78-42dd-b07a-eb28a7a566a3'),
	(127,16,11,1,'en_us',7,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"83\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L.P.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855 &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Key Contacts&nbsp;<\\/strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brad Stout<br>Chief Operating Officer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Clay Allen<br>Chief Financial Officer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Russell Artho<br>Controller &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Jay Cortese<br>Cattle Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brady Bressler<br>Cattle Procurement Assistant Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Mark Hooker<br>Fed Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Karli Schilling<br>Assistant Fed Cattle Marketing Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Blaine Rotramel<br>Commodity Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Dell Volmer<br>Feedyard Operations Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Eddie Derrick<br>Growyard & Pasture Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>&nbsp;Tom Portillo, DVM<br>Manager of Animal Health &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 17:32:39','2016-01-27 17:32:39','2fd403a4-ace1-4afd-9c62-f5b7f6516c10'),
	(128,16,11,1,'en_us',8,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"83\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L.P.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855 &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Key Contacts&nbsp;<\\/strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brad Stout<br>Chief Operating Officer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Clay Allen<br>Chief Financial Officer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Russell Artho<br>Controller &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Jay Cortese<br>Cattle Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brady Bressler<br>Cattle Procurement Assistant Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Mark Hooker<br>Fed Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Karli Schilling<br>Assistant Fed Cattle Marketing Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Blaine Rotramel<br>Commodity Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Dell Volmer<br>Feedyard Operations Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Eddie Derrick<br>Growyard & Pasture Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Tom Portillo, DVM<br>Manager of Animal Health &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\"}}},\"8\":\"\"}}','2016-01-27 17:34:33','2016-01-27 17:34:33','cd994b58-37a8-4301-a79b-e05dada84aee'),
	(129,84,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"News\",\"slug\":\"news\",\"postDate\":1453916235,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Here you will find the latest news and information about Friona Industries achievements and involvement with our communities.<\\/p><p><strong>2015<\\/strong><\\/p><p>Portillo Honored as AVC Consultant of the Year<\\/p><p><strong>2014<\\/strong><\\/p><p><a href=\\\"http:\\/\\/tscra.org\\/news_blog\\/2014\\/07\\/24\\/james-herring-to-receive-national-golden-spur-award\\/\\\" target=\\\"true\\\">James Herring to Receive National Golden Spur Award<\\/a><\\/p>\"}}},\"8\":\"\"}}','2016-01-27 17:37:15','2016-01-27 17:37:15','dc9ac65b-6457-4929-9e22-8005f8d18a78'),
	(147,18,10,1,'en_us',2,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Community\",\"slug\":\"community\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new2\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"U.S. Roundtable on Sustainable Beef\"}},\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries is one of the founding members of the <a href=\\\"https:\\/\\/www.usrsb.org\\/about.aspx\\\" target=\\\"_blank\\\">U.S. Roundtable on Sustainable Beef<\\/a>&nbsp;(USRSB), which is a multi-stakeholder initiative developed to advance, support and communicate continuous improvement in sustainability of the U.S. beef value chain.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Our representative to the group is Blaine Rotramel, our commodity procurement manager at Friona Industries. \\u201cWe\\u2019re here to feed the world,\\u201d Rotramel says. \\u201cWe\\u2019re using our knowledge to look at better ways to do things in our business.\\u201d<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>USRSB focuses on four areas: Social (employees involved in the community), Economic (community impact), Environmental and Animal Health & Welfare.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries is an active supporter in each of those areas. For example, Animal Health & Welfare are addressed through many of the <a href=\\\"http:\\/\\/frionaindustries.sandboxdsm.com\\/stewardship\\/animal-health#entry:17\\\">BQA activities<\\/a> at each of the feedyards; and a recent economic impact study showed the Randall County Feedyard provided $7 million in business activity within a 100-mile radius of the location.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>The USRSB is working in collaboration with the <a href=\\\"http:\\/\\/grsbeef.org\\/\\\" target=\\\"_blank\\\">Global Roundtable for Sustainable Beef<\\/a> (GRSB) and other sustainability initiatives whose aims are consistent with its mission and vision.<\\/p>\"}}},\"8\":\"\"}}','2016-02-01 20:36:54','2016-02-01 20:36:54','4444d5f4-b284-4983-9472-306036deec32'),
	(148,19,10,1,'en_us',5,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"ENOUGH\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Stewardship\"}},\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stewardship is an ethic that embodies the responsible planning and management of resources. At Friona Industries, we take stewardship seriously. You can see that in our Code of Conduct&nbsp;as well as in the <a href=\\\"http:\\/\\/www.bqa.org\\/about\\/value-to-the-industry\\\" target=\\\"_blank\\\">Beef Quality Assurance<\\/a><span><\\/span>&nbsp;practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>A large part of the beef industry\\u2019s job involves making sure that beef is safe and wholesome for consumers. We work together with our cattle, animal health, and feed suppliers to maintain high standards for the beef we produce every day.<\\/p>\"}}},\"8\":\"\"}}','2016-02-01 20:38:22','2016-02-01 20:38:22','da6d7aa9-500e-48c0-9bde-3c776ecb1ffa'),
	(149,15,9,1,'en_us',7,'','{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Stewardship\",\"slug\":\"sustainability\",\"postDate\":1453912143,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"48\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stewardship is an ethic that embodies the responsible planning and management of resources. At Friona Industries, we take stewardship seriously. You can see that in our <a href=\\\"http:\\/\\/\\/media\\/pdfs\\/FI-Ethics-Letter-January-2016-NewLogo-pd.pdf#asset:74\\\" target=\\\"_blank\\\">Code of Conduct<\\/a>&nbsp;as well as in the <a href=\\\"http:\\/\\/www.bqa.org\\/about\\/value-to-the-industry\\\" target=\\\"true\\\">Beef Quality Assurance<\\/a>&nbsp;practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>A large part of the beef industry\\u2019s job involves making sure that beef is safe and wholesome for consumers. We work together with our cattle, animal health, and feed suppliers to maintain high standards for the beef we produce every day.<\\/p>\"}}},\"8\":\"A large part of the beef industry\\u2019s job involves making sure that beef is safe and wholesome for consumers.\"}}','2016-02-01 20:39:37','2016-02-01 20:39:37','bd0ed54d-86ef-4737-a7b1-37ab8222bc6d'),
	(150,19,10,1,'en_us',6,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"ENOUGH\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"98\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Stewardship\"}},\"99\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The <a href=\\\"https:\\/\\/www.enoughmovement.com\\/learn\\/index.aspx\\\" target=\\\"_blank\\\">ENOUGH Movement<\\/a>&nbsp;is a global community working together to ensure that each person has access to nutritious, affordable food, both today and in the coming decades. The global effort is being led by <a href=\\\"https:\\/\\/www.enoughmovement.com\\/report\\/\\\" target=\\\"_blank\\\">Elanco<\\/a>, one of our business partners.<\\/p>\"}}},\"8\":\"\"}}','2016-02-01 20:41:44','2016-02-01 20:41:44','d1efa591-dd8f-43ae-a489-7cdf3eb57e35'),
	(151,19,10,1,'en_us',7,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"ENOUGH\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"99\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The <a href=\\\"https:\\/\\/www.enoughmovement.com\\/learn\\/index.aspx\\\" target=\\\"_blank\\\">ENOUGH Movement<\\/a>&nbsp;is a global community working together to ensure that each person has access to nutritious, affordable food, both today and in the coming decades. The global effort is being led by <a href=\\\"https:\\/\\/www.enoughmovement.com\\/report\\/\\\" target=\\\"_blank\\\">Elanco<\\/a>, one of our business partners.<\\/p>\"}}},\"8\":\"\"}}','2016-02-01 20:53:52','2016-02-01 20:53:52','eda22242-b94f-4246-b17d-b31814d3c48d'),
	(152,4,3,1,'en_us',10,'','{\"typeId\":\"3\",\"authorId\":null,\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1447967140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"72\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"OUR MISSION:\"}},\"73\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"DEDICATED EMPLOYEES. INNOVATIVE BEEF. SATISFIED CUSTOMER.\"}},\"44\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>For more than 50 years, Friona Industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors: our livestock and our customers.<\\/p>\\r\\n\\r\\n<p>We built this company in the north Texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship. We answer that expectation through innovative beef production and a reputation for exceeding customer expectations.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>At Friona Industries, quality means a fierce attention to detail from every employee on every job. To back that up, here is our <a href=\\\"http:\\/\\/\\/media\\/pdfs\\/FI-Ethics-Letter-January-2016-NewLogo-pd.pdf#asset:74\\\" target=\\\"_blank\\\">Code of Conduct<\\/a>&nbsp;promise that our employees sign every year.<\\/p>\"}}},\"8\":\"\"}}','2016-02-01 20:55:03','2016-02-01 20:55:03','26e8849e-71aa-4f6d-bf4e-935078ff69b8'),
	(154,2,1,1,'en_us',11,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"new1\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Test Headline\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}}}}}','2016-02-03 19:49:07','2016-02-03 19:49:07','50ccea8f-3715-4601-a1a2-5e5126575b90'),
	(155,2,1,1,'en_us',12,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"Test Headline\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}}}}}','2016-02-03 20:29:12','2016-02-03 20:29:12','b8acc423-cf20-4771-98e9-35117f6aeb21'),
	(156,2,1,1,'en_us',13,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"Test Headline\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}}}}}','2016-02-03 20:34:06','2016-02-03 20:34:06','c72ed762-ecfe-432c-b39f-dd1be0565e21'),
	(157,2,1,1,'en_us',14,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"Test Headline\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}},\"new1\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}}}}}','2016-02-03 20:40:13','2016-02-03 20:40:13','12260eb8-a80f-4a01-8660-95eb895b500e'),
	(158,2,1,1,'en_us',15,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"new1\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":\"\",\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.cnn.com\\/\",\"slideAsset\":\"\"}}}}}','2016-02-03 20:40:56','2016-02-03 20:40:56','cf24eb53-e575-4277-b864-626498c6a275'),
	(159,2,1,1,'en_us',16,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slider\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.cnn.com\\/\",\"slideAsset\":\"\"}}}}}','2016-02-03 20:41:08','2016-02-03 20:41:08','1f3cdbe1-6a14-4f59-8def-2c4683f981fb'),
	(160,2,1,1,'en_us',17,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":[\"74\"]}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.cnn.com\\/\",\"slideAsset\":\"\"}}}}}','2016-02-03 21:06:40','2016-02-03 21:06:40','4be13c0c-3fbf-41c1-b827-153ef8ce153a'),
	(161,2,1,1,'en_us',18,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.cnn.com\\/\",\"slideAsset\":\"\"}}}}}','2016-02-03 21:47:50','2016-02-03 21:47:50','e4f27223-5641-46d4-b128-0dfdfbc4fa52'),
	(162,2,1,1,'en_us',19,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}}}}}','2016-02-03 21:51:23','2016-02-03 21:51:23','352c9873-762b-401e-b9fe-6ac98115aa7e'),
	(163,2,1,1,'en_us',20,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}}}}}','2016-02-03 21:59:54','2016-02-03 21:59:54','306778ac-aea4-4051-a7e4-bc037c6c3f54'),
	(164,2,1,1,'en_us',21,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}},\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}}}}}','2016-02-03 22:42:59','2016-02-03 22:42:59','7f7745a0-a217-497a-9b99-6ce57e0edcec'),
	(165,2,1,1,'en_us',22,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"106\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}}}}}','2016-02-03 22:43:08','2016-02-03 22:43:08','159f50b7-1452-488b-8189-948ac0fb6538'),
	(166,114,10,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"USRSB\",\"slug\":\"usrsb\",\"postDate\":1454539589,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"USRSB\"}},\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta lectus sed felis ullamcorper, nec congue mi semper. Vestibulum eu vehicula massa. Aenean et justo sem. Proin ac lectus odio. Aliquam ut fringilla velit. Donec dignissim sapien nec ipsum semper maximus. Vestibulum in vestibulum nisl, vitae sollicitudin metus. Nullam a malesuada dui, faucibus blandit nisi. Quisque egestas nibh egestas enim dictum dictum. Nam ante nulla, tincidunt eu consequat quis, faucibus ac ante. Curabitur ornare facilisis dui, sed blandit ante sodales ac. Vestibulum et tincidunt neque, in consequat augue. Etiam consectetur nulla at elit aliquet vestibulum. In vestibulum nisl et vestibulum lacinia.<\\/p>\"}}},\"8\":\"\"}}','2016-02-03 22:46:29','2016-02-03 22:46:29','7eeb51d7-9a50-4f02-a553-61b2cb23ef88'),
	(167,2,1,1,'en_us',23,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"First Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Second Slide\",\"summary\":\"<p>&nbsp;Mauris laoreet felis a ligula malesuada, vel imperdiet ipsum suscipit. Suspendisse potenti.&nbsp;<\\/p>\",\"slideUrl\":\"http:\\/\\/www.sandboxww.com\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Third Slide\",\"summary\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<\\/p>\",\"slideUrl\":\"http:\\/\\/www.damonjentree.me\",\"slideAsset\":\"\"}}}}}','2016-02-03 22:49:45','2016-02-03 22:49:45','75a493fb-d2e8-4602-bbb2-71ff6f538b8c'),
	(168,14,7,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Careers\",\"slug\":\"careers\",\"postDate\":1447886515,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"78\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Working at Friona\"}},\"76\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The integrity of our highly motivated and dedicated employees and officers is one of the most valuable assets we have at Friona Industries. The foundation of our success and our ability to grow and take advantage of our resources and innovation is inspired by honesty and fairness through a culture of strong ethical behavior. <\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>New employees go through a training process so they understand how important their position is to cattle performance at the feedyard, as well as the financial performance of the company.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Each year, we ask our employees to agree to a Code of Conduct&nbsp;It explains our philosophical principles of employee conduct.<\\/p>\"}},\"80\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Jobs at Friona\"}},\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>There are three general career areas at Friona Industries: Feedyard Management & Office Support, Feedyard Operations, and Feed Mill Operations. Every job at Friona is important to the overall care and performance of the cattle in our feedyards, as well as to the overall financial performance of the company.<\\/p>\\r\\n\\r\\n<p>Friona is always taking job applications&nbsp;for possible openings at our feedyards. Please click on the area below that you are interested in to see a general description and a printable job application that you can fill out and submit to Friona Industries.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Each year, we ask our employees to agree to a Code of Conduct.&nbsp;It explains our philosophical principles of employee conduct.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><a href=\\\"\\/media\\/documents\\/careers\\/FI-Employment-Application.pdf#asset:82\\\">Completed applications<\\/a> can be mailed directly to the feedyard&nbsp;you are interested in working at or to Friona Industries, 500 S Taylor St, Suite 601, Amarillo, TX 79105, or it can be scanned and emailed to <a href=\\\"mailto:fi@frionaind.com\\\">fi@frionaind.com<\\/a><\\/p>\"}},\"new1\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Feedyard Management & Office Support\"}},\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Careers in this area are in charge of operations and employees at the feedyard, as well as billing and bookkeeping records. Employees in these positions may deal with the public, input suppliers, service providers and FI customers. They are responsible for accurate records and adhering to federal and state regulations, as well as safety policies.<\\/p>\"}},\"new3\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Feedyard Operations\"}},\"new4\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Careers in this area are in direct contact with the cattle in the feedyards. Employees in these positions may feed and move cattle, determine feeding schedules and rations, assess and treat cattle health issues, and work on yard maintenance.&nbsp;<\\/p>\"}},\"new5\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Feed Mill Operations\"}},\"new6\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Careers in this area are working with grain storage and processing equipment. Employees in these positions may handle commodities, operate mill equipment, and provide feed quality control.&nbsp;<\\/p>\\r\\n\\r\\n\"}}},\"8\":\"\"}}','2016-02-04 14:28:30','2016-02-04 14:28:30','687dddb6-ab91-476d-8c7a-a79e70bd9669'),
	(169,8,5,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Locations\",\"slug\":\"locations\",\"postDate\":1447886412,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"47\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The corporate office&nbsp;for Friona Industries is located in Amarillo, Texas. Additionally, we have four state-of-the-art feedyards in north Texas, with a total feeding capacity that ranks us in the top 10 feedyards worldwide.<\\/p>\\r\\n\\r\\n<p><a href=\\\"http:\\/\\/frionaindustries.sandboxdsm.com\\/contact#entry:16\\\">Friona Industries, L.P.<\\/a><\\/p><p>500 S. Taylor, Suite 601<br>Amarillo, TX 79101<\\/p><p>Friona Feedyard<\\/p><p>2370 FM 3140<br>Friona, TX 79035<\\/p><p>Littlefield Feedyard<\\/p><p>1640 FM 37<br>Amherst, TX 79312<\\/p><p>Randall County Feedyard<\\/p><p>15000 FM 2219<br>Amarillo, TX 79119<\\/p><p>Swisher County Feedyard<\\/p><p>6656 FM 214<br>Happy, TX 79042<br><\\/p><p><br><\\/p>\"}}},\"8\":\"\"}}','2016-02-04 15:01:05','2016-02-04 15:01:05','fd103610-39d0-430c-8d65-37ee364d86a5'),
	(170,8,5,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Locations\",\"slug\":\"locations\",\"postDate\":1447886412,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"\",\"8\":\"\"}}','2016-02-04 15:09:22','2016-02-04 15:09:22','da256538-5ac2-48c2-9c3d-b94510798969'),
	(171,7,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Affiliations\",\"slug\":\"affilliations\",\"postDate\":1447886340,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":\"\",\"8\":\"\"}}','2016-02-04 16:22:24','2016-02-04 16:22:24','90f4c07a-5055-4732-af65-cf1348eb6f17'),
	(172,16,11,1,'en_us',9,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"83\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L.P.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811 &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855 &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Key Contacts&nbsp;<\\/strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brad Stout<br>Chief Operating Officer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Clay Allen<br>Chief Financial Officer<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Russell Artho<br>Controller &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Jay Cortese<br>Cattle Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Brady Bressler<br>Cattle Procurement Assistant Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Mark Hooker<br>Fed Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Karli Schilling<br>Assistant Fed Cattle Marketing Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Blaine Rotramel<br>Commodity Procurement Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Dell Volmer<br>Feedyard Operations Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Eddie Derrick<br>Growyard & Pasture Cattle Manager &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Tom Portillo, DVM<br>Manager of Animal Health &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<\\/p>\"}}},\"42\":{\"new1\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Brad Stout\",\"position\":\"Chief Operating Officer\"}},\"new2\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Clay Allen\",\"position\":\"Chief Financial Officer\"}},\"new3\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Russell Artho    \",\"position\":\"Controller\"}},\"new4\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Jay Cortese\",\"position\":\"Cattle Procurement Manager\"}},\"new5\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Brady Bressler \",\"position\":\"Cattle Procurement Assistant Manager\"}},\"new6\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Mark Hooker\",\"position\":\"Fed Cattle Manager \"}},\"new7\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Karli Schilling\",\"position\":\"Assistant Fed Cattle Marketing Manager\"}},\"new8\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Blaine Rotramel   \",\"position\":\"Commodity Procurement Manager\"}},\"new9\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Dell Volmer\",\"position\":\"Feedyard Operations Manager \"}},\"new10\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Eddie Derrick \",\"position\":\"Growyard & Pasture Cattle Manager \"}},\"new11\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Tom Portillo\",\"position\":\"DVM Manager of Animal Health\"}}},\"8\":\"\"}}','2016-02-04 16:44:13','2016-02-04 16:44:13','47f00a7a-3908-4137-8337-368544db26f1'),
	(173,16,11,1,'en_us',10,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"83\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Friona Industries, L.P.<\\/p>\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568<\\/p>\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101<\\/p>\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811<\\/p>\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003<\\/p>\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855<\\/p>\"}}},\"42\":{\"123\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Brad Stout\",\"position\":\"Chief Operating Officer\"}},\"124\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Clay Allen\",\"position\":\"Chief Financial Officer\"}},\"125\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Russell Artho    \",\"position\":\"Controller\"}},\"126\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Jay Cortese\",\"position\":\"Cattle Procurement Manager\"}},\"127\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Brady Bressler \",\"position\":\"Cattle Procurement Assistant Manager\"}},\"128\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Mark Hooker\",\"position\":\"Fed Cattle Manager \"}},\"129\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Karli Schilling\",\"position\":\"Assistant Fed Cattle Marketing Manager\"}},\"130\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Blaine Rotramel   \",\"position\":\"Commodity Procurement Manager\"}},\"131\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Dell Volmer\",\"position\":\"Feedyard Operations Manager \"}},\"132\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Eddie Derrick \",\"position\":\"Growyard & Pasture Cattle Manager \"}},\"133\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Tom Portillo\",\"position\":\"DVM Manager of Animal Health\"}}},\"8\":\"\"}}','2016-02-04 16:44:59','2016-02-04 16:44:59','ba6754c4-eafe-424e-9f92-bd6c8d53aa56'),
	(174,2,1,1,'en_us',24,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"89\"],\"headline\":\"Beef Safety Starts with Cattle Procurement\",\"summary\":\"<p>Good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Dedicated Employees Make Friona Industries Successful\",\"summary\":\"<p>Our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Making a Positive Economic Impact\",\"summary\":\"<p>We use local services and buy local inputs for our four state-of-the-art feedyards<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}}}}}','2016-02-05 15:28:46','2016-02-05 15:28:46','de0c9aba-d4bc-46a7-8ea0-134011df7fc1'),
	(175,2,1,1,'en_us',25,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"89\"],\"headline\":\"Beef Safety Starts with Cattle Procurement\",\"summary\":\"<p>Good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Dedicated Employees Make Friona Industries Successful\",\"summary\":\"<p>Our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Making a Positive Economic Impact\",\"summary\":\"<p>We use local services and buy local inputs for our four state-of-the-art feedyards<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}}}}}','2016-02-05 15:30:42','2016-02-05 15:30:42','a53679ed-6a8d-4fea-a06c-c48efbc6ece1'),
	(176,2,1,1,'en_us',26,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"89\"],\"headline\":\"Beef Safety Starts with Cattle Procurement\",\"summary\":\"<p>Good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":\"\"}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Dedicated Employees Make Friona Industries Successful\",\"summary\":\"<p>Our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Making a Positive Economic Impact\",\"summary\":\"<p>We use local services and buy local inputs for our four state-of-the-art feedyards<\\/p>\",\"slideUrl\":\"http:\\/\\/frionaindustries.sandboxdsm.com\\/stewardship\\/usrsb\",\"slideAsset\":\"\"}}}}}','2016-02-05 15:31:25','2016-02-05 15:31:25','74475ea5-ac69-4950-8039-3aac662b8b7c'),
	(177,2,1,1,'en_us',27,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1447880571,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"50\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Our Mission<\\/p>\\r\\n\\r\\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.<\\/h1>\"}}},\"8\":\"\",\"36\":{\"111\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"89\"],\"headline\":\"Beef Safety Starts with Cattle Procurement\",\"summary\":\"<p>Good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"135\"]}},\"112\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"91\"],\"headline\":\"Dedicated Employees Make Friona Industries Successful\",\"summary\":\"<p>Our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef.&nbsp;<\\/p>\",\"slideUrl\":\"\",\"slideAsset\":[\"74\"]}},\"113\":{\"type\":\"slide\",\"enabled\":\"1\",\"fields\":{\"image\":[\"93\"],\"headline\":\"Making a Positive Economic Impact\",\"summary\":\"<p>We use local services and buy local inputs for our four state-of-the-art feedyards<\\/p>\",\"slideUrl\":\"http:\\/\\/frionaindustries.sandboxdsm.com\\/stewardship\\/usrsb\",\"slideAsset\":\"\"}}}}}','2016-02-05 15:32:53','2016-02-05 15:32:53','14c36330-6db1-4df1-aec3-9465e2479045'),
	(178,17,10,1,'en_us',7,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Animal Health\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Good health practices for our feedyard cattle start long before they arrive in the feedyard. That\\u2019s why we have established a <a href=\\\"\\/media\\/pdfs\\/BQA-Compliance-Position-Statement.pdf#asset:135\\\">compliance position statement<\\/a>&nbsp;that we share with each of our feeder cattle suppliers.<\\/p>\\r\\n\\r\\n<p>\\u201cOur model for animal health is to manage risk differently than what much of the industry does,\\u201d says Tom Portillo, DVM, and Friona Industries manager of animal health. \\u201cIf you look at the investment we have with cattle, we recognize there is value in cattle that are vaccinated and backgrounded appropriately before they arrive in our feedyards,\\u201d he says.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries is serious about this investment in cattle, and has two staff people assigned to working with backgrounders and checking the cattle management in their operations.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The attention to health doesn\\u2019t stop at the gate when cattle are delivered to one of the four state-of-the-art feedyards that Friona Industries owns in north Texas. \\u201cOur goal is to look at each animal individually every day,\\u201d Portillo says. This is accomplished by having pen riders work through each pen of cattle, no matter what the weather conditions are that day.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>\\u201cAnother important part of our animal health protocol is basic animal husbandry,\\u201d Portillo says. At Friona Industries, providing the basics of animal care through well-managed yards and dedicated employees is easy to implement because it is part of the day-to-day routine at each of the four feedyards.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p> &nbsp;<a href=\\\"file:\\/\\/\\/Z:\\/DM%20Server2\\/FRIONA\\/35463_Website\\/C1_Website\\/TEXT_IMAGES%20transfer\\/2016.02.04\\/Stewardship%20-%20Animal%20Health.docx#_msoanchor_1\\\">[DG1]<\\/a>links to BQA Compliance Position Statement, found in the Locations section<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p> &nbsp;<a href=\\\"file:\\/\\/\\/Z:\\/DM%20Server2\\/FRIONA\\/35463_Website\\/C1_Website\\/TEXT_IMAGES%20transfer\\/2016.02.04\\/Stewardship%20-%20Animal%20Health.docx#_msoanchor_2\\\">[DG2]<\\/a>link to the Portillo CV revised 07-30-15.pdf found in the Stewardship folder of the Copy file in Text_Images Transfer<\\/p>\"}}},\"8\":\"\"}}','2016-02-05 15:35:14','2016-02-05 15:35:14','2dc50cd4-4a00-4094-bc9e-0e423fbfe93f'),
	(179,17,10,1,'en_us',8,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Animal Health\",\"slug\":\"animal-health\",\"postDate\":1447886640,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"136\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Good health practices for our feedyard cattle start long before they arrive in the feedyard. That\\u2019s why we have established a <a href=\\\"\\/media\\/pdfs\\/BQA-Compliance-Position-Statement.pdf#asset:135:url\\\">compliance position statement<\\/a>&nbsp;that we share with each of our feeder cattle suppliers.<\\/p>\\r\\n\\r\\n<p>\\u201cOur model for animal health is to manage risk differently than what much of the industry does,\\u201d says Tom Portillo, DVM, and Friona Industries manager of animal health. \\u201cIf you look at the investment we have with cattle, we recognize there is value in cattle that are vaccinated and backgrounded appropriately before they arrive in our feedyards,\\u201d he says.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries is serious about this investment in cattle, and has two staff people assigned to working with backgrounders and checking the cattle management in their operations.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The attention to health doesn\\u2019t stop at the gate when cattle are delivered to one of the four state-of-the-art feedyards that Friona Industries owns in north Texas. \\u201cOur goal is to look at each animal individually every day,\\u201d Portillo says. This is accomplished by having pen riders work through each pen of cattle, no matter what the weather conditions are that day.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>\\u201cAnother important part of our animal health protocol is basic animal husbandry,\\u201d Portillo says. At Friona Industries, providing the basics of animal care through well-managed yards and dedicated employees is easy to implement because it is part of the day-to-day routine at each of the four feedyards.<\\/p>\"}}},\"8\":\"\"}}','2016-02-05 15:35:35','2016-02-05 15:35:35','1d180889-ca7f-46ba-ae1c-309f3aa5cd15'),
	(180,19,10,1,'en_us',8,'','{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"ENOUGH\",\"slug\":\"enough-movement\",\"postDate\":1447886700,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"99\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The <a href=\\\"https:\\/\\/www.enoughmovement.com\\/learn\\/index.aspx\\\" target=\\\"_blank\\\">ENOUGH Movement<\\/a>&nbsp;is a global community working together to ensure that each person has access to nutritious, affordable food, both today and in the coming decades. The global effort is being led by <a href=\\\"https:\\/\\/www.enoughmovement.com\\/report\\/\\\" target=\\\"_blank\\\">Elanco<\\/a>, one of our business partners.<\\/p><p>Providing food globally is very important, and we take the responsibility of providing food in our own backyard to those who are in need. That\\u2019s why we\\u2019ve partnered the Texas Cattle Feeders Association in support of both the Snack Pak 4 Kids and the High Plains Food Bank.<\\/p>\"}}},\"8\":\"\"}}','2016-02-05 15:36:36','2016-02-05 15:36:36','00785ff0-6958-4149-8e45-b136f5d7e52e'),
	(181,137,10,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"BQA\",\"slug\":\"bqa\",\"postDate\":1454686889,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"17\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>The public is looking for more transparency in the production of their food, including beef. That\\u2019s why Friona Industries has made a commitment to follow Beef Quality Assurance (BQA) protocols in their feedyards. \\u201cThis is a platform that is good for us, as we are committed to animal welfare,\\u201d says Tom Portillo, DVM, the manager of animal health at Friona Industries.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>BQA reflects a positive public image and instills consumer confidence in the beef industry. When producers implement the best management practices of a BQA program, they assure their market steers, heifers, cows, and bulls are the best they can be. <\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Friona Industries follows:<br><a href=\\\"http:\\/\\/www.bqa.org\\/Media\\/BQA\\/Docs\\/feedyard_assessment_062209_blank.pdf\\\" target=\\\"_blank\\\">BQA Assessments<\\/a>&nbsp;by Third Party Auditors;<br><a href=\\\"http:\\/\\/www.bqa.org\\/Media\\/BQA\\/Docs\\/cchg2015_final.pdf\\\" target=\\\"_blank\\\">Annual BQA Training<\\/a> for the Crews at Each Feedyard;<br>Judicious Use of <a href=\\\"http:\\/\\/www.bqa.org\\/Media\\/BQA\\/Docs\\/judiciousmicrobials.pdf\\\" target=\\\"_blank\\\">Antibiotics<\\/a>&nbsp;and<br><a href=\\\"http:\\/\\/www.bqa.org\\/Media\\/BQA\\/Docs\\/supplemental_guidelines_2014.pdf\\\" target=\\\"_blank\\\">Continuous Review<\\/a>&nbsp;of Practices.<\\/p>\"}}},\"8\":\"\"}}','2016-02-05 15:41:29','2016-02-05 15:41:29','1450f1a6-0547-4882-8bd0-3365e3446f2f'),
	(182,16,11,1,'en_us',11,'','{\"typeId\":\"11\",\"authorId\":null,\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1447967249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":{\"83\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"Contact Info and Key Contacts\"}},\"46\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p><a href=\\\"\\/media\\/pdfs\\/BQA-Compliance-Position-Statement.pdf#asset:135\\\">Sell Us Your Cattle<\\/a><\\/p><p>Friona Industries, L.P.<\\/p>\\r\\n\\r\\n<p><strong>Mailing Address<br><\\/strong>P.O. Box 15568<br>Amarillo, TX 79105-5568<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Physical Address<br><\\/strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Telephone<\\/strong><br>806-374-1811<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Fax<br><\\/strong>806-374-3003<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><strong>Cattle Dept.<br><\\/strong>806-220-2855<\\/p>\"}}},\"42\":{\"123\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Brad Stout\",\"position\":\"Chief Operating Officer\"}},\"124\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Clay Allen\",\"position\":\"Chief Financial Officer\"}},\"125\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Russell Artho    \",\"position\":\"Controller\"}},\"126\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Jay Cortese\",\"position\":\"Cattle Procurement Manager\"}},\"127\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Brady Bressler \",\"position\":\"Cattle Procurement Assistant Manager\"}},\"128\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Mark Hooker\",\"position\":\"Fed Cattle Manager \"}},\"129\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Karli Schilling\",\"position\":\"Assistant Fed Cattle Marketing Manager\"}},\"130\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Blaine Rotramel   \",\"position\":\"Commodity Procurement Manager\"}},\"131\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Dell Volmer\",\"position\":\"Feedyard Operations Manager \"}},\"132\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Eddie Derrick \",\"position\":\"Growyard & Pasture Cattle Manager \"}},\"133\":{\"type\":\"contact\",\"enabled\":\"1\",\"fields\":{\"fullName\":\"Tom Portillo\",\"position\":\"DVM Manager of Animal Health\"}}},\"8\":\"\"}}','2016-02-05 22:48:08','2016-02-05 22:48:08','5bdb1121-3550-4fdf-ae4b-9607bdacb1a1');

/*!40000 ALTER TABLE `entryversions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldgroups`;

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','2015-11-18 21:02:51','2015-11-18 21:02:51','c6ac20f0-b1c7-4aac-96db-175c601214ed'),
	(2,'Careers','2015-11-19 18:09:51','2015-11-19 18:09:51','69062c8b-f9ca-4bbb-b6d4-742e4a36d4fa'),
	(3,'Metadata','2015-11-30 16:04:22','2015-11-30 16:04:22','40d57f52-56bd-4aec-9bc2-b5da740e3308'),
	(4,'Locations','2015-11-30 20:03:06','2015-11-30 20:03:06','672cfc3c-74f4-499f-a593-1f64050e89e6'),
	(5,'News','2015-12-09 17:38:13','2015-12-09 17:38:13','8bacf9ca-976f-48d8-9487-1fd4a46e76bd');

/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayoutfields`;

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(33,35,25,7,0,1,'2015-11-30 16:05:45','2015-11-30 16:05:45','408b9610-a386-4a72-a6ae-3437fbc5e520'),
	(34,35,25,1,0,2,'2015-11-30 16:05:45','2015-11-30 16:05:45','11c30478-9b53-4f37-a628-6a58c96374af'),
	(35,35,25,3,0,3,'2015-11-30 16:05:45','2015-11-30 16:05:45','98eb0ec2-1076-4e88-ac08-932ed284f39f'),
	(36,35,25,6,0,4,'2015-11-30 16:05:45','2015-11-30 16:05:45','2112cee0-c1d4-4c7e-982d-1ed64b05c24a'),
	(37,35,26,8,0,1,'2015-11-30 16:05:45','2015-11-30 16:05:45','7b9dd096-6a37-4c50-8af7-8bb9fc191e48'),
	(198,110,132,17,0,1,'2015-12-09 20:30:35','2015-12-09 20:30:35','de2105b7-4b72-467d-9897-3bf02e42c10d'),
	(199,110,133,8,0,1,'2015-12-09 20:30:35','2015-12-09 20:30:35','beaa3d97-438d-4df5-bd1c-ad097d17f6c0'),
	(208,116,140,17,0,1,'2015-12-09 20:38:02','2015-12-09 20:38:02','2bc5fc67-8a62-40c8-a88b-57ad8d6c9af7'),
	(209,116,141,8,0,1,'2015-12-09 20:38:02','2015-12-09 20:38:02','74de4eb4-ad6a-4bc6-85c1-7253d09b1676'),
	(210,117,142,17,0,1,'2015-12-09 20:38:18','2015-12-09 20:38:18','a42d5963-da47-4a3d-9965-59d08b619a36'),
	(211,117,143,8,0,1,'2015-12-09 20:38:18','2015-12-09 20:38:18','89b29a1d-7801-4000-8e55-2ba31ce5c1b5'),
	(212,118,144,17,0,1,'2015-12-09 20:41:32','2015-12-09 20:41:32','8f17afc4-c6b0-4f51-9d04-12f629abfe29'),
	(213,118,145,8,0,1,'2015-12-09 20:41:32','2015-12-09 20:41:32','4602a3c3-1e1b-4523-b767-30e6d110acf0'),
	(218,121,150,17,0,1,'2015-12-09 20:49:26','2015-12-09 20:49:26','f77d7b39-2c84-4083-8737-dac140792435'),
	(219,121,151,8,0,1,'2015-12-09 20:49:26','2015-12-09 20:49:26','359efc20-1d90-46e6-98ef-12d7e796bd68'),
	(220,122,152,17,0,1,'2015-12-09 20:52:23','2015-12-09 20:52:23','5e4b6e91-e743-4740-963a-b03b93e62533'),
	(221,122,153,8,0,1,'2015-12-09 20:52:23','2015-12-09 20:52:23','c7a59cbe-2de3-41dc-a1fd-36f926303c9a'),
	(266,152,183,27,0,1,'2016-01-25 16:52:19','2016-01-25 16:52:19','afb1cc77-7441-4a82-8fb0-8074c88eef19'),
	(267,153,184,33,0,1,'2016-01-25 16:52:19','2016-01-25 16:52:19','48c64ab0-7c62-4406-bf5b-c161239a3747'),
	(268,154,185,28,0,1,'2016-01-25 16:52:19','2016-01-25 16:52:19','f9dbad50-b10a-424a-819e-ad59d1f6eb96'),
	(269,155,186,29,0,1,'2016-01-25 16:52:19','2016-01-25 16:52:19','2eb9e157-6c80-4fd7-b9d6-2d4a15c0add1'),
	(270,155,186,30,0,2,'2016-01-25 16:52:19','2016-01-25 16:52:19','c64c3abd-9295-432c-907d-bb51a4f3d5b8'),
	(271,156,187,31,0,1,'2016-01-25 16:52:19','2016-01-25 16:52:19','710c4854-baff-4bbf-8431-213e04e0b489'),
	(272,156,187,32,0,2,'2016-01-25 16:52:19','2016-01-25 16:52:19','17a43854-4a98-4637-afd0-42df54d780b2'),
	(273,156,187,34,0,3,'2016-01-25 16:52:19','2016-01-25 16:52:19','a5277e79-d0b9-45b8-bf69-48d0b0183ee9'),
	(274,157,188,25,0,1,'2016-01-25 16:59:45','2016-01-25 16:59:45','b07947fa-d207-405f-9624-7fc0f2d4d753'),
	(275,157,188,17,0,2,'2016-01-25 16:59:45','2016-01-25 16:59:45','c8cb149f-1bf6-4eb0-a55e-c28a23fee631'),
	(276,157,189,8,0,1,'2016-01-25 16:59:45','2016-01-25 16:59:45','6b38d042-180d-45e3-8c01-0ad2007791c0'),
	(281,162,194,17,0,1,'2016-01-27 16:32:20','2016-01-27 16:32:20','631ce618-aef1-4c4b-aaa6-d05b42bcfb98'),
	(282,162,195,8,0,1,'2016-01-27 16:32:20','2016-01-27 16:32:20','2c426b51-091b-4f0c-bb15-91659c18f72c'),
	(284,165,197,35,0,1,'2016-02-03 19:12:48','2016-02-03 19:12:48','16b48bbb-4e55-4c97-bea1-15dd9cc4ef18'),
	(285,165,198,8,0,1,'2016-02-03 19:12:48','2016-02-03 19:12:48','a6f4294d-7409-49eb-af79-e38547ed61eb'),
	(290,167,200,36,0,1,'2016-02-03 19:43:13','2016-02-03 19:43:13','94908664-5e1e-41ba-93a2-ac18fb5e245a'),
	(291,167,200,17,0,2,'2016-02-03 19:43:13','2016-02-03 19:43:13','7dd46e4a-8ed4-4a39-ad2f-b5e5aff39225'),
	(292,167,201,8,0,1,'2016-02-03 19:43:13','2016-02-03 19:43:13','2fa1b2c6-0563-4910-8a13-7210bd8ff2d0'),
	(318,173,207,41,1,1,'2016-02-03 22:43:57','2016-02-03 22:43:57','eeb68721-573f-437e-a2b4-fb8a5d06774f'),
	(319,173,207,37,1,2,'2016-02-03 22:43:57','2016-02-03 22:43:57','44aadd4e-fea2-4285-a187-1e47509a3a62'),
	(320,173,207,38,0,3,'2016-02-03 22:43:57','2016-02-03 22:43:57','65b01104-368c-4b48-9d84-a64057d23965'),
	(321,173,207,39,0,4,'2016-02-03 22:43:57','2016-02-03 22:43:57','c26b101e-2d2c-455e-8a33-bb72005366c8'),
	(322,173,207,40,0,5,'2016-02-03 22:43:57','2016-02-03 22:43:57','7180ae59-0890-4c19-806c-e0ed5df5fe57'),
	(325,175,209,17,0,1,'2016-02-04 16:41:40','2016-02-04 16:41:40','6c3e52b3-e111-4b42-9d9b-a4058db65751'),
	(326,175,210,42,0,1,'2016-02-04 16:41:40','2016-02-04 16:41:40','b27fb94b-a3f2-4789-afb1-a36b8966220b'),
	(327,175,211,8,0,1,'2016-02-04 16:41:40','2016-02-04 16:41:40','cc38a0cb-1972-4b22-a796-4a15c3a31b9c'),
	(328,176,212,43,0,1,'2016-02-04 16:45:27','2016-02-04 16:45:27','7bc88dc3-9208-40e2-bbff-03eb57a5165c'),
	(329,176,212,44,0,2,'2016-02-04 16:45:27','2016-02-04 16:45:27','a4b47e13-9c93-4685-ae8e-ba23f79aa89c'),
	(330,177,213,46,0,1,'2016-02-04 17:29:17','2016-02-04 17:29:17','cc14afda-0300-4e4f-a4ac-8b388497c4bc'),
	(331,177,213,47,0,2,'2016-02-04 17:29:17','2016-02-04 17:29:17','32d74acb-368b-4693-900f-378051569ffb'),
	(332,177,213,48,0,3,'2016-02-04 17:29:17','2016-02-04 17:29:17','17a080f5-e210-4d15-8dad-5951d396a3f0'),
	(333,178,214,1,0,1,'2016-02-04 17:30:16','2016-02-04 17:30:16','aa2e3940-01a0-4fc3-932e-a643fc5e8553'),
	(334,178,215,14,1,1,'2016-02-04 17:30:16','2016-02-04 17:30:16','18023240-5bf9-4273-a2ba-79b9a38b07c3'),
	(335,178,215,16,0,2,'2016-02-04 17:30:16','2016-02-04 17:30:16','5c6e6d4a-4425-4aad-8eb8-0fd047e1766c'),
	(336,178,215,15,0,3,'2016-02-04 17:30:16','2016-02-04 17:30:16','0820afa6-aac8-427f-866e-f8c8c715ba0b'),
	(337,178,216,45,0,1,'2016-02-04 17:30:16','2016-02-04 17:30:16','ddc4af47-b5a9-4847-8092-313d7c6405da'),
	(338,178,217,8,0,1,'2016-02-04 17:30:16','2016-02-04 17:30:16','3d95e07c-5561-460a-ae73-6a031bea2d51');

/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayouts`;

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;

INSERT INTO `fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Tag','2015-11-18 21:02:51','2015-11-18 21:02:51','8f503900-be7b-4679-b341-39228143205b'),
	(9,'Entry','2015-11-18 22:40:26','2015-11-18 22:40:26','7ec4aa60-aab1-4452-bf41-1909a6253e12'),
	(26,'Asset','2015-11-23 14:57:20','2015-11-23 14:57:20','d2378c48-7ca9-4206-bdda-151ff91913f0'),
	(35,'Entry','2015-11-30 16:05:45','2015-11-30 16:05:45','ac73502d-1f03-4fe2-9c63-185fc33fb144'),
	(64,'Asset','2015-12-09 16:59:21','2015-12-09 16:59:21','8bb6414d-3032-4723-a17a-413e88a04ecb'),
	(65,'Asset','2015-12-09 16:59:39','2015-12-09 16:59:39','7734ea09-590c-4f0d-9b7d-637f0d5d8348'),
	(110,'Entry','2015-12-09 20:30:35','2015-12-09 20:30:35','6bc53baf-7d55-4249-a942-4076b3138d83'),
	(116,'Entry','2015-12-09 20:38:02','2015-12-09 20:38:02','4cf8d361-4cab-4849-bd31-c80d581a79ff'),
	(117,'Entry','2015-12-09 20:38:18','2015-12-09 20:38:18','bf1b9e96-54f8-4f09-9243-bb959af5ef2a'),
	(118,'Entry','2015-12-09 20:41:32','2015-12-09 20:41:32','085501be-003c-4315-a3c6-8a089033c51f'),
	(121,'Entry','2015-12-09 20:49:26','2015-12-09 20:49:26','133feddc-e8a6-4fd9-8ac8-65e409a53105'),
	(122,'Entry','2015-12-09 20:52:23','2015-12-09 20:52:23','055fb37d-e936-46ee-b0cb-82f5343e97c3'),
	(152,'MatrixBlock','2016-01-25 16:52:19','2016-01-25 16:52:19','f4267304-4bcd-4e67-9c9d-301894052e01'),
	(153,'MatrixBlock','2016-01-25 16:52:19','2016-01-25 16:52:19','78fb9b91-bd91-4c6d-9fc8-69a9f07e4b93'),
	(154,'MatrixBlock','2016-01-25 16:52:19','2016-01-25 16:52:19','93c645ca-b565-4080-b6b0-42f6ee50fa1b'),
	(155,'MatrixBlock','2016-01-25 16:52:19','2016-01-25 16:52:19','73cd9c8e-f881-4f31-8638-7eb67a033b6a'),
	(156,'MatrixBlock','2016-01-25 16:52:19','2016-01-25 16:52:19','136ecf48-1c2a-4f23-8483-842488cdafd4'),
	(157,'Entry','2016-01-25 16:59:45','2016-01-25 16:59:45','51b0f3a9-549d-4e28-9bc8-869aca29d57f'),
	(159,'Asset','2016-01-27 15:52:23','2016-01-27 15:52:23','9e7f76ce-d57f-45fd-8243-6b5a84b25300'),
	(162,'Entry','2016-01-27 16:32:20','2016-01-27 16:32:20','5c0589eb-73e3-4d7e-819e-3c24b58b3a59'),
	(163,'Entry','2016-02-01 21:56:32','2016-02-01 21:56:32','f9277cb3-6a8b-444b-b943-a2e718c72ef0'),
	(165,'Entry','2016-02-03 19:12:48','2016-02-03 19:12:48','b5cabb03-cb05-45f0-aac1-c56893ab3b2a'),
	(167,'Entry','2016-02-03 19:43:13','2016-02-03 19:43:13','b32c93c9-3123-427a-9637-93e30c66b0ce'),
	(173,'MatrixBlock','2016-02-03 22:43:57','2016-02-03 22:43:57','e236eac6-ed3c-4a25-8156-439532516787'),
	(175,'Entry','2016-02-04 16:41:40','2016-02-04 16:41:40','c8b58b6d-34a6-4735-b4d5-1ff00a508360'),
	(176,'MatrixBlock','2016-02-04 16:45:27','2016-02-04 16:45:27','c045b984-d159-40b2-8cf7-3ff9042dbcb1'),
	(177,'MatrixBlock','2016-02-04 17:29:17','2016-02-04 17:29:17','0e292464-beb4-45b7-9c6c-7bc748a3038b'),
	(178,'Category','2016-02-04 17:30:16','2016-02-04 17:30:16','adcb8c64-9268-4157-b7f5-79350090796b');

/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayouttabs`;

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(25,35,'Career Details',1,'2015-11-30 16:05:45','2015-11-30 16:05:45','2f1281e1-b9d0-4d8c-bc7b-a326afff05d0'),
	(26,35,'Metadata',2,'2015-11-30 16:05:45','2015-11-30 16:05:45','e81afa96-a270-49f0-bfeb-bbdad1278823'),
	(132,110,'Content',1,'2015-12-09 20:30:35','2015-12-09 20:30:35','b1219de1-52af-4221-9d0c-3ce5d89d9dae'),
	(133,110,'Metadata',2,'2015-12-09 20:30:35','2015-12-09 20:30:35','7bb8d0ea-f41e-4ae1-94a2-d72f69d194d0'),
	(140,116,'Content',1,'2015-12-09 20:38:02','2015-12-09 20:38:02','e1586988-7290-42f4-8484-502419405c84'),
	(141,116,'Metadata',2,'2015-12-09 20:38:02','2015-12-09 20:38:02','bbbb0acd-fe81-4ed4-a0d4-3c301630d069'),
	(142,117,'Content',1,'2015-12-09 20:38:18','2015-12-09 20:38:18','ca33f8f9-89e0-4544-ad06-5f27f5dac87e'),
	(143,117,'Metadata',2,'2015-12-09 20:38:18','2015-12-09 20:38:18','2aa19205-1eaf-4c71-b27d-60df9dbdcbd1'),
	(144,118,'Default',1,'2015-12-09 20:41:32','2015-12-09 20:41:32','9c3e23e3-2863-4d26-b525-1c8700d181a0'),
	(145,118,'Metadata',2,'2015-12-09 20:41:32','2015-12-09 20:41:32','776e75df-c2c3-457d-beb4-192e9fe95ea4'),
	(150,121,'Content',1,'2015-12-09 20:49:26','2015-12-09 20:49:26','c19dbcf1-09fb-49d7-81b3-358f9f4ce908'),
	(151,121,'Metadata',2,'2015-12-09 20:49:26','2015-12-09 20:49:26','0b78d474-8a79-418a-8c37-4c324e4d0938'),
	(152,122,'Content',1,'2015-12-09 20:52:23','2015-12-09 20:52:23','803aff3c-da3e-47a3-9aa3-ec141eb81ccf'),
	(153,122,'Metadata',2,'2015-12-09 20:52:23','2015-12-09 20:52:23','386320b4-821c-472b-9330-3f8c1a75838a'),
	(183,152,'Content',1,'2016-01-25 16:52:19','2016-01-25 16:52:19','b428ab47-531f-426b-8d08-fc191c5d0772'),
	(184,153,'Content',1,'2016-01-25 16:52:19','2016-01-25 16:52:19','b3f14efb-1dcb-4caf-aa08-740e1702e306'),
	(185,154,'Content',1,'2016-01-25 16:52:19','2016-01-25 16:52:19','b502151d-dda4-4259-b0b7-ef4e70bb7c2e'),
	(186,155,'Content',1,'2016-01-25 16:52:19','2016-01-25 16:52:19','773442c1-d49d-4639-b132-9ae27ea46eb1'),
	(187,156,'Content',1,'2016-01-25 16:52:19','2016-01-25 16:52:19','db107fa1-324e-4b58-9f76-3b5b654c0340'),
	(188,157,'Content',1,'2016-01-25 16:59:45','2016-01-25 16:59:45','5efa7cc4-5035-4281-b617-fc001eb6381f'),
	(189,157,'Metadata',2,'2016-01-25 16:59:45','2016-01-25 16:59:45','c8290ba3-ee16-4916-b0e5-e6e6b054c3ab'),
	(194,162,'Content',1,'2016-01-27 16:32:20','2016-01-27 16:32:20','f9888b79-dfab-406c-9c9a-a98786481cb1'),
	(195,162,'Metadata',2,'2016-01-27 16:32:20','2016-01-27 16:32:20','2696ba96-038e-49ea-8404-26ed55c1fd94'),
	(197,165,'Default',1,'2016-02-03 19:12:48','2016-02-03 19:12:48','d9ee1c44-c5bf-4bef-b7b6-8f2343a618d0'),
	(198,165,'Metadata',2,'2016-02-03 19:12:48','2016-02-03 19:12:48','92f4466b-af1b-4acb-9ce4-49e5bcebcf6a'),
	(200,167,'Content',1,'2016-02-03 19:43:13','2016-02-03 19:43:13','ae55f911-ff16-4dac-bc7c-e58da3b00dca'),
	(201,167,'Metadata',2,'2016-02-03 19:43:13','2016-02-03 19:43:13','a4a02de6-a272-4420-9433-4f30dbb8e105'),
	(207,173,'Content',1,'2016-02-03 22:43:57','2016-02-03 22:43:57','31ec0cfd-74b0-484e-8f77-e33415f38722'),
	(209,175,'Content',1,'2016-02-04 16:41:40','2016-02-04 16:41:40','ef97a080-28ae-4ade-ab44-1acba0a305e7'),
	(210,175,'Key Contacts',2,'2016-02-04 16:41:40','2016-02-04 16:41:40','b6880d67-e54c-44b4-9593-b4c9027427ed'),
	(211,175,'Metadata',3,'2016-02-04 16:41:40','2016-02-04 16:41:40','00312653-8c5d-4e66-8add-40dafa2dea6a'),
	(212,176,'Content',1,'2016-02-04 16:45:27','2016-02-04 16:45:27','6ad92d66-bbd3-4046-b846-7f3563b36e81'),
	(213,177,'Content',1,'2016-02-04 17:29:17','2016-02-04 17:29:17','69503042-52a2-4d57-bb6f-caf5b0d02ad7'),
	(214,178,'Content',1,'2016-02-04 17:30:16','2016-02-04 17:30:16','1ed22b77-39f4-4357-8bd5-4c52deeaa393'),
	(215,178,'Location Details',2,'2016-02-04 17:30:16','2016-02-04 17:30:16','b20942db-5814-4d90-af04-aedd1e4df2db'),
	(216,178,'Contact Information',3,'2016-02-04 17:30:16','2016-02-04 17:30:16','1cc39eba-902e-4192-bfdb-e07db93f9c91'),
	(217,178,'Metadata',4,'2016-02-04 17:30:16','2016-02-04 17:30:16','6e23920a-ee95-458b-b4a8-716f804fdb79');

/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fields`;

CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_context_idx` (`context`),
  KEY `fields_groupId_fk` (`groupId`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;

INSERT INTO `fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2015-11-18 21:02:51','2015-11-18 21:02:51','6a207eaf-8dc7-49b4-a1fa-1a260d192a0e'),
	(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2015-11-18 21:02:51','2015-11-18 21:02:51','adb2a2ad-6c13-4303-94ba-d07e395c0f2d'),
	(3,2,'Career Location(s)','careerLocations','global','Choose which location this career should be related to. You can choose more than one location.',0,'Categories','{\"source\":\"group:1\",\"limit\":\"\",\"selectionLabel\":\"Add a location\"}','2015-11-19 18:12:26','2015-11-19 18:12:26','814b930e-c3b5-4437-968f-8f32e75331a4'),
	(4,1,'Email','email','global','Enter a valid email address.',0,'PlainText','{\"placeholder\":\"name@email.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-20 22:15:42','2015-11-20 22:15:42','93060498-f549-4e56-99cd-475329ec8d59'),
	(5,1,'Full Name','fullName','global','Enter this persons full name.',0,'PlainText','{\"placeholder\":\"John Smith\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-20 22:16:53','2015-11-20 22:17:37','d8c3ee90-df3b-4312-b181-cd6040a922c5'),
	(6,2,'Application','application','global','Upload the job application here.',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"pdf\"],\"limit\":\"1\",\"selectionLabel\":\"Add the application\"}','2015-11-23 14:58:37','2015-11-23 16:33:07','034c295c-3d73-4404-b354-d8b54a62dce7'),
	(7,2,'Summary','careerSummary','global','Provide a brief summary of this position.',0,'RichText','{\"configFile\":\"Simple.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-11-23 19:44:42','2015-12-09 17:41:51','1307d23c-6dbc-479b-b810-6967d63eb457'),
	(8,3,'Page Description','pageDescription','global','Best under 155 characters, the page description attribute provides concise explanations of the contents of the web page. Page descriptions are commonly used on search engine result pages (SERPs) to display preview snippets for a given page.',0,'PlainText','{\"placeholder\":\"Here is a description of the applicable page.\",\"maxLength\":\"155\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-30 16:05:06','2015-11-30 16:05:06','484d2d41-7b2e-4232-94aa-40d2386fc6ed'),
	(9,4,'Address1','address1','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-30 20:03:21','2015-12-01 15:27:44','e7d4dce6-cef1-4a27-b8a6-2cfa85c5a7a3'),
	(10,4,'City','city','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-30 20:03:27','2015-11-30 20:03:27','18749089-f0b1-464a-93f7-c321a18162c4'),
	(11,4,'State','state','global','',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"Alabama\",\"value\":\"AL\",\"default\":\"\"},{\"label\":\"Alaska\",\"value\":\"AK\",\"default\":\"\"},{\"label\":\"Arizona\",\"value\":\"AZ\",\"default\":\"\"},{\"label\":\"Arkansas\",\"value\":\"AR\",\"default\":\"\"},{\"label\":\"California\",\"value\":\"CA\",\"default\":\"\"},{\"label\":\"Colorado\",\"value\":\"CO\",\"default\":\"\"},{\"label\":\"Connecticut\",\"value\":\"CT\",\"default\":\"\"},{\"label\":\"Delaware\",\"value\":\"DE\",\"default\":\"\"},{\"label\":\"District of Columbia\",\"value\":\"DC\",\"default\":\"\"},{\"label\":\"Florida\",\"value\":\"FL\",\"default\":\"\"},{\"label\":\"Georgia\",\"value\":\"GA\",\"default\":\"\"},{\"label\":\"Hawaii\",\"value\":\"HI\",\"default\":\"\"},{\"label\":\"Idaho\",\"value\":\"ID\",\"default\":\"\"},{\"label\":\"Illinois\",\"value\":\"IL\",\"default\":\"\"},{\"label\":\"Indiana\",\"value\":\"IN\",\"default\":\"\"},{\"label\":\"Iowa\",\"value\":\"IA\",\"default\":\"\"},{\"label\":\"Kansas\",\"value\":\"KS\",\"default\":\"\"},{\"label\":\"Kentucky\",\"value\":\"KY\",\"default\":\"\"},{\"label\":\"Louisiana\",\"value\":\"LA\",\"default\":\"\"},{\"label\":\"Maine\",\"value\":\"ME\",\"default\":\"\"},{\"label\":\"Maryland\",\"value\":\"MD\",\"default\":\"\"},{\"label\":\"Massachusetts\",\"value\":\"MA\",\"default\":\"\"},{\"label\":\"Michigan\",\"value\":\"MI\",\"default\":\"\"},{\"label\":\"Minnesota\",\"value\":\"MN\",\"default\":\"\"},{\"label\":\"Mississippi\",\"value\":\"MS\",\"default\":\"\"},{\"label\":\"Missouri\",\"value\":\"MO\",\"default\":\"\"},{\"label\":\"Montana\",\"value\":\"MT\",\"default\":\"\"},{\"label\":\"Nebraska\",\"value\":\"NE\",\"default\":\"\"},{\"label\":\"Nevada\",\"value\":\"NV\",\"default\":\"\"},{\"label\":\"New Hampshire\",\"value\":\"NH\",\"default\":\"\"},{\"label\":\"New Jersey\",\"value\":\"NJ\",\"default\":\"\"},{\"label\":\"New Mexico\",\"value\":\"NM\",\"default\":\"\"},{\"label\":\"New York\",\"value\":\"NY\",\"default\":\"\"},{\"label\":\"North Carolina\",\"value\":\"NC\",\"default\":\"\"},{\"label\":\"North Dakota\",\"value\":\"ND\",\"default\":\"\"},{\"label\":\"Ohio\",\"value\":\"OH\",\"default\":\"\"},{\"label\":\"Oklahoma\",\"value\":\"OK\",\"default\":\"\"},{\"label\":\"Oregon\",\"value\":\"OR\",\"default\":\"\"},{\"label\":\"Pennsylvania\",\"value\":\"PA\",\"default\":\"\"},{\"label\":\"Rhode Island\",\"value\":\"RI\",\"default\":\"\"},{\"label\":\"South Carolina\",\"value\":\"SC\",\"default\":\"\"},{\"label\":\"South Dakota\",\"value\":\"SD\",\"default\":\"\"},{\"label\":\"Tennessee\",\"value\":\"TN\",\"default\":\"\"},{\"label\":\"Texas\",\"value\":\"TX\",\"default\":\"\"},{\"label\":\"Utah\",\"value\":\"UT\",\"default\":\"\"},{\"label\":\"Vermont\",\"value\":\"VT\",\"default\":\"\"},{\"label\":\"Virginia\",\"value\":\"VA\",\"default\":\"\"},{\"label\":\"Washington\",\"value\":\"WA\",\"default\":\"\"},{\"label\":\"West Virginia\",\"value\":\"WV\",\"default\":\"\"},{\"label\":\"Wisconsin\",\"value\":\"WI\",\"default\":\"\"},{\"label\":\"Wyoming\",\"value\":\"WY\",\"default\":\"\"}]}','2015-11-30 20:03:51','2015-11-30 20:03:51','44fd32dc-340a-4315-8096-35ace86b475a'),
	(12,4,'Zip','zip','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-30 20:03:56','2015-11-30 20:03:56','edaf03bf-da24-4d9d-a41d-da35f759b647'),
	(13,4,'Address2','address2','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-11-30 20:11:52','2015-11-30 20:11:52','28e0f27a-0d84-4d01-9dca-b2535566e193'),
	(14,2,'Address','address','global','',0,'SmartMap_Address','{\"layout\":{\"street1\":{\"width\":100,\"enable\":1},\"street2\":{\"width\":100,\"enable\":1},\"city\":{\"width\":50,\"enable\":1},\"state\":{\"width\":15,\"enable\":1},\"zip\":{\"width\":35,\"enable\":1},\"country\":{\"width\":100,\"enable\":0},\"lat\":{\"width\":50,\"enable\":0},\"lng\":{\"width\":50,\"enable\":0}}}','2015-12-01 15:32:03','2015-12-01 15:32:03','05283576-e71e-4098-be1b-b45e15bea604'),
	(15,4,'Primary Phone Number','primaryPhoneNumber','global','Enter the primary phone number for this location.',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-09 15:43:00','2015-12-09 15:45:32','a0774d67-a500-4560-aeed-5a08690edf7a'),
	(16,4,'Primary Email','primaryEmail','global','Enter the primary email address for this location.',0,'PlainText','{\"placeholder\":\"name@email.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-09 15:45:23','2015-12-09 15:45:23','e9506de4-c8a2-416e-96f5-f474efe86d1c'),
	(17,1,'Content Builder','contentBuilder','global','Use this feature to build content. You can arrange the elements to create different layouts for each entry.',0,'Matrix','{\"maxBlocks\":null}','2015-12-09 16:46:06','2016-01-25 16:52:18','494b78ef-5a13-4c51-b0a2-a2319644d3f3'),
	(25,5,'Summary','newsSummary','global','Provide a brief summary of this article. This is what will show on the home page. If a summary is not provided, the system will grab the first paragraph of the main content.',0,'RichText','{\"configFile\":\"Simple.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-09 17:41:06','2015-12-09 17:43:18','492e4039-41b6-4829-8c07-33a4b6a29199'),
	(27,NULL,'Headline','headline','matrixBlockType:5','This will render a primary headline.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-09 19:38:25','2016-01-25 16:52:19','f9ffbfee-4adc-4dcb-848b-e67fba7a005e'),
	(28,NULL,'Body','bodyCopy','matrixBlockType:6','This is a main content block.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-09 19:38:25','2016-01-25 16:52:19','72a7cf30-f4af-4716-90ae-6af833dcae74'),
	(29,NULL,'Image','contentImage','matrixBlockType:7','Upload an image.',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"3\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2015-12-09 19:38:25','2016-01-25 16:52:19','6450efa8-0446-4e9a-a827-e3b88457305b'),
	(30,NULL,'Image Position','imagePosition','matrixBlockType:7','Choose how you would like to position this image within the entry.',0,'PositionSelect','{\"options\":[\"left\",\"center\",\"right\"]}','2015-12-09 19:38:25','2016-01-25 16:52:19','4f39b6b4-4b68-4270-aeb6-c7385d54b781'),
	(31,NULL,'Pull Quote','quote','matrixBlockType:8','',0,'RichText','{\"configFile\":\"Simple.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-09 19:38:25','2016-01-25 16:52:19','1db8bd67-f97f-4ab8-a1e4-632e14b566ba'),
	(32,NULL,'Quote Attribution','quoteAttribution','matrixBlockType:8','Enter the source of this quote.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-09 19:38:25','2016-01-25 16:52:19','02e459b4-b89a-4a16-b248-4cc344e51bed'),
	(33,NULL,'Sub Headline','subHeadline','matrixBlockType:9','This will render a sub headline.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-25 16:23:06','2016-01-25 16:52:19','1691f9c4-6973-4352-9d7c-7a64f4d71ab6'),
	(34,NULL,'Attribution Title','attributionTitle','matrixBlockType:8','Provide this person\'s title. Leave this field empty if you do not wish to include a title.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-25 16:35:39','2016-01-25 16:52:19','bf6e8863-97f1-46b5-b351-ecab327bf64b'),
	(35,5,'News Asset','newsAsset','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2016-02-03 19:12:28','2016-02-03 19:12:28','ff7d2f0e-15a1-4552-a955-a387b667c02d'),
	(36,1,'Slide Builder','slideBuilder','global','Use this feature to create the slider images & links on the home page.',0,'Matrix','{\"maxBlocks\":\"3\"}','2016-02-03 19:42:22','2016-02-03 22:43:57','d74aab94-45f4-4c19-a514-efa38076b3a0'),
	(37,NULL,'Headline','headline','matrixBlockType:10','This is the primary headline for this slide.',0,'PlainText','{\"placeholder\":\"MAKING A POSITIVE ECONOMIC IMPACT\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-02-03 19:42:22','2016-02-03 22:43:57','93935828-6f7f-45b3-9d42-d05e49d22ed2'),
	(38,NULL,'Summary','summary','matrixBlockType:10','Provide a brief summary of this article.',0,'RichText','{\"configFile\":\"Simple.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2016-02-03 19:42:23','2016-02-03 22:43:57','2240d4d1-482a-4103-a096-f43f4b10fba3'),
	(39,NULL,'External Slide Url','slideUrl','matrixBlockType:10','Enter the full url (including the http) of the page you want to link to.',0,'PlainText','{\"placeholder\":\"http:\\/\\/www.google.com\\/\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-02-03 19:42:23','2016-02-03 22:43:57','eb9e880b-92fd-42d4-8027-9ae48a9410a6'),
	(40,NULL,'Asset','slideAsset','matrixBlockType:10','Upload an asset that this article should link to.',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":[\"folder:5\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"4\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"pdf\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2016-02-03 19:42:23','2016-02-03 22:43:57','6a004636-edcc-4102-9acf-2facdb550aef'),
	(41,NULL,'Image','image','matrixBlockType:10','Upload an image for this slide.',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"3\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2016-02-03 20:25:57','2016-02-03 22:43:57','827402d4-8e18-4405-ab5a-6c828fd5cb6f'),
	(42,1,'Key Contacts','keyContacts','global','',0,'Matrix','{\"maxBlocks\":null}','2016-02-04 16:41:15','2016-02-04 16:45:27','47fdc157-cb4b-40f3-8c95-1fbf810de3b8'),
	(43,NULL,'Full Name','fullName','matrixBlockType:11','',0,'PlainText','{\"placeholder\":\"Jeffrey Gill\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-02-04 16:41:15','2016-02-04 16:45:27','3ab52381-f6e6-434a-aaa8-1167e11f5606'),
	(44,NULL,'Position','position','matrixBlockType:11','',0,'PlainText','{\"placeholder\":\"Chief Operating Officer\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-02-04 16:41:15','2016-02-04 16:45:27','14dd2942-105b-40d9-acbe-f445ccaae233'),
	(45,4,'Location Contact','locationContact','global','Use this feature to manage location contacts.',0,'Matrix','{\"maxBlocks\":null}','2016-02-04 17:29:17','2016-02-04 17:29:17','e16cb79c-3dc0-4984-87dc-75a79ef2d4a5'),
	(46,NULL,'Full Name','fullName','matrixBlockType:12','',0,'PlainText','{\"placeholder\":\"Angela Peraza\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-02-04 17:29:17','2016-02-04 17:29:17','0f01822c-d202-4d3a-b1fd-1a1db19ded14'),
	(47,NULL,'Position','position','matrixBlockType:12','',0,'PlainText','{\"placeholder\":\"Chief Operating Officer\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-02-04 17:29:17','2016-02-04 17:29:17','78200f72-dcf1-4b63-99a9-51c4a7772832'),
	(48,NULL,'Image','image','matrixBlockType:12','',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"3\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-02-04 17:29:17','2016-02-04 17:29:17','ef7add5b-7aa3-4e0e-86d8-970f5c633a43');

/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `globalsets`;

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info`;

CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `build` int(11) unsigned NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `releaseDate` datetime NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `track` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;

INSERT INTO `info` (`id`, `version`, `build`, `schemaVersion`, `releaseDate`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `track`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'2.5',2761,'2.5.11','2016-01-28 19:16:15',1,'Friona Industries','http://frionaindustries.sandboxdsm.com/','America/Chicago',1,0,'stable','2015-11-18 21:02:47','2016-02-04 16:39:13','72d7611f-9af2-4421-b559-5ad06457196d');

/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locales`;

CREATE TABLE `locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `locales` WRITE;
/*!40000 ALTER TABLE `locales` DISABLE KEYS */;

INSERT INTO `locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	('en_us',1,'2015-11-18 21:02:47','2015-11-18 21:02:47','4bbbc353-c6d2-4f38-a44c-960034d2718f');

/*!40000 ALTER TABLE `locales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixblocks`;

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;

INSERT INTO `matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(44,4,17,6,3,NULL,'2015-12-09 20:31:25','2016-02-01 20:55:03','db304a6c-d927-403e-9d64-ed9e6aa75407'),
	(46,16,17,6,2,NULL,'2015-12-09 20:48:53','2016-02-05 22:48:08','4064dca5-8ccd-4e37-a2d8-6f9d15aca600'),
	(48,15,17,6,1,NULL,'2015-12-09 20:52:35','2016-02-01 20:39:37','8e5abc83-97e5-4a6f-85c5-1a4590526bf1'),
	(50,2,17,6,1,NULL,'2016-01-25 16:53:01','2016-02-05 15:32:53','f69119df-102a-4e89-b72a-09bfa2a000cf'),
	(72,4,17,5,1,NULL,'2016-01-27 15:42:44','2016-02-01 20:55:03','5510fbd6-0393-45cb-8b6c-757d12b9da20'),
	(73,4,17,9,2,NULL,'2016-01-27 15:42:44','2016-02-01 20:55:03','59527156-b5c2-42ca-9aaa-739e747ca183'),
	(76,14,17,6,2,NULL,'2016-01-27 15:56:18','2016-02-04 14:28:30','c80f9744-461b-4d15-a3af-f444dcbf9688'),
	(77,5,17,6,1,NULL,'2016-01-27 15:57:30','2016-01-27 15:57:30','b12b7a71-d651-4390-982c-c91eaa6acfe7'),
	(78,14,17,5,1,NULL,'2016-01-27 16:26:27','2016-02-04 14:28:30','96160b29-189c-4770-9b85-018a75dafd54'),
	(80,14,17,9,3,NULL,'2016-01-27 16:36:30','2016-02-04 14:28:30','b92e54ea-4848-436f-8d34-edba8d99ad40'),
	(81,14,17,6,4,NULL,'2016-01-27 16:36:30','2016-02-04 14:28:30','957dc546-07f2-4ca3-9320-67b35eb15565'),
	(83,16,17,9,1,NULL,'2016-01-27 16:59:45','2016-02-05 22:48:08','1bca078d-fb23-4620-9b6b-b15058a9f4c4'),
	(85,84,17,6,1,NULL,'2016-01-27 17:37:15','2016-01-27 17:37:15','a6aced0f-6a6b-4156-9e84-c2ccb4f473ce'),
	(96,18,17,5,1,NULL,'2016-02-01 20:36:54','2016-02-01 20:36:54','b38db4ec-ae88-4957-b589-e88c13b2ff40'),
	(97,18,17,6,2,NULL,'2016-02-01 20:36:54','2016-02-01 20:36:54','973a5590-cc2d-4da5-ac1c-1a02d17228bc'),
	(99,19,17,6,1,NULL,'2016-02-01 20:38:22','2016-02-05 15:36:36','97e05741-d1ce-4c10-9926-c00bed72c170'),
	(111,2,36,10,1,NULL,'2016-02-03 19:49:07','2016-02-05 15:32:53','5eeb15fb-4099-4fda-ac23-3b05dadbc886'),
	(112,2,36,10,2,NULL,'2016-02-03 20:40:13','2016-02-05 15:32:53','3562fa0c-c08c-4ff9-9e26-ba0d1bf9d5e6'),
	(113,2,36,10,3,NULL,'2016-02-03 20:40:56','2016-02-05 15:32:53','3a4be530-cdcc-4df7-9bb4-d73ac165bbfd'),
	(115,114,17,5,1,NULL,'2016-02-03 22:46:29','2016-02-03 22:46:29','149b822d-7722-4364-8a78-9b5653914e6e'),
	(116,114,17,6,2,NULL,'2016-02-03 22:46:29','2016-02-03 22:46:29','5e6fbf29-f77c-47e5-8862-baa59fafe1d9'),
	(117,14,17,9,5,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','e3d41c82-2f4f-4b2d-a7bf-3629dd2db618'),
	(118,14,17,6,6,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','88176993-d08d-4129-9bf4-f6e216884a7c'),
	(119,14,17,9,7,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','6d9cae7a-c5a0-43d1-8a44-0dd52373c5f2'),
	(120,14,17,6,8,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','2388fa6e-1c6d-44a4-b974-f5d2b3da812d'),
	(121,14,17,9,9,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','0a17db85-4aa1-4d88-a1c7-4aee38ffb760'),
	(122,14,17,6,10,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','7a402e08-1f4f-4f70-b2f6-c773970d341b'),
	(123,16,42,11,1,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','fb315066-5962-404c-9aaf-75552225f47e'),
	(124,16,42,11,2,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','c0916edf-d120-4bb9-88f1-21bc6908b8dd'),
	(125,16,42,11,3,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','8fdfb385-e420-4d61-b2cb-e6890b97b102'),
	(126,16,42,11,4,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','fa860e65-1d1c-4ec9-be95-db4ada0fd53d'),
	(127,16,42,11,5,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','8d8a8dd8-0eec-4ba8-96c9-eeb85aeb1469'),
	(128,16,42,11,6,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','d416c95e-4b97-499d-b0a5-6345982901b9'),
	(129,16,42,11,7,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','4e0bcb76-7248-41b9-8f70-c64e65d829b0'),
	(130,16,42,11,8,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','c2ea1756-3b68-4279-bebe-6fb8f3d4f22a'),
	(131,16,42,11,9,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','84664264-2c2d-4ff8-801c-aeb9caec115c'),
	(132,16,42,11,10,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','89acc76b-d5f6-4aa2-a4d7-5a304426d15f'),
	(133,16,42,11,11,NULL,'2016-02-04 16:44:13','2016-02-05 22:48:08','31f475c5-3d66-489d-87f4-5d05d5ef9933'),
	(134,20,45,12,1,NULL,'2016-02-04 17:30:53','2016-02-05 22:38:20','0c51f43f-6219-417a-8a2c-9508fe42e76f'),
	(136,17,17,6,1,NULL,'2016-02-05 15:35:14','2016-02-05 15:35:35','92dc58f2-0189-4e4d-821f-35436f2330e0'),
	(138,137,17,6,1,NULL,'2016-02-05 15:41:29','2016-02-05 15:41:29','c7880cc9-cd2c-4d7d-8294-e1fe4002e7a6'),
	(176,20,45,12,2,NULL,'2016-02-05 22:32:51','2016-02-05 22:38:20','a4bcdd8e-f0a9-468f-9610-33c5c0ec99c3'),
	(177,20,45,12,3,NULL,'2016-02-05 22:32:51','2016-02-05 22:38:20','2086ad79-2a9e-47b8-abb2-82ffdd92d995'),
	(178,21,45,12,1,NULL,'2016-02-05 22:37:36','2016-02-05 22:37:36','4cf9c6e5-27e9-4d49-814c-45eb0e919361'),
	(179,21,45,12,2,NULL,'2016-02-05 22:37:36','2016-02-05 22:37:36','fec4318c-60b3-4b0d-9b9c-eb4cf8ce3182'),
	(180,21,45,12,3,NULL,'2016-02-05 22:37:36','2016-02-05 22:37:36','93e1c4e3-bcf2-4d25-87b3-c1f14e5f46d6'),
	(181,22,45,12,1,NULL,'2016-02-05 22:40:23','2016-02-05 22:40:23','08849e13-12dd-400b-9406-8538db5532e9'),
	(182,22,45,12,2,NULL,'2016-02-05 22:40:23','2016-02-05 22:40:23','2f95a4b7-dae7-48c8-9236-73818ed3351a'),
	(183,22,45,12,3,NULL,'2016-02-05 22:40:23','2016-02-05 22:40:23','95bf0c78-eba5-4d41-821a-adff25e34eb9'),
	(184,23,45,12,1,NULL,'2016-02-05 22:42:30','2016-02-05 22:42:30','76fa25d4-6057-4247-acd1-e757c939b9ad'),
	(185,23,45,12,2,NULL,'2016-02-05 22:42:30','2016-02-05 22:42:30','08671620-21a2-4903-8b8a-4aa0e7f4fd9c'),
	(186,23,45,12,3,NULL,'2016-02-05 22:42:30','2016-02-05 22:42:30','5bd93074-d978-4e2d-a982-25a064799911');

/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixblocktypes`;

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;

INSERT INTO `matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(5,17,152,'Headline','headline',1,'2015-12-09 19:38:25','2016-01-25 16:52:19','93cc234f-a6cd-481b-87a8-892ef336a97c'),
	(6,17,154,'Body','body',3,'2015-12-09 19:38:25','2016-01-25 16:52:19','9d939aaf-9134-4cde-b2a7-ab64a8b9d258'),
	(7,17,155,'Image','image',4,'2015-12-09 19:38:25','2016-01-25 16:52:19','85880de8-facf-400c-a738-91c9c4959721'),
	(8,17,156,'Pull Quote','pullQuote',5,'2015-12-09 19:38:25','2016-01-25 16:52:19','c80d9c98-272c-472d-8533-4f3bb70017cb'),
	(9,17,153,'Sub Headline','subHeadline',2,'2016-01-25 16:23:06','2016-01-25 16:52:19','89d6c0ad-2881-47ee-9295-0b65378b3b64'),
	(10,36,173,'Slide','slide',1,'2016-02-03 19:42:22','2016-02-03 22:43:57','c33c299f-3224-4be9-9296-5cacdd849e36'),
	(11,42,176,'Contact','contact',1,'2016-02-04 16:41:15','2016-02-04 16:45:27','dfbc3cfa-44c0-499d-84f9-52cac4030b0b'),
	(12,45,177,'Contact','contact',1,'2016-02-04 17:29:17','2016-02-04 17:29:17','a4172a8b-8d58-46ae-b01d-5a841d5a2d8e');

/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_contentbuilder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_contentbuilder`;

CREATE TABLE `matrixcontent_contentbuilder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_headline_headline` text COLLATE utf8_unicode_ci,
  `field_body_bodyCopy` text COLLATE utf8_unicode_ci,
  `field_image_imagePosition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_pullQuote_quote` text COLLATE utf8_unicode_ci,
  `field_pullQuote_quoteAttribution` text COLLATE utf8_unicode_ci,
  `field_subHeadline_subHeadline` text COLLATE utf8_unicode_ci,
  `field_pullQuote_attributionTitle` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_contentbuilder_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_contentbuilder_locale_idx` (`locale`),
  CONSTRAINT `matrixcontent_contentbuilder_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_contentbuilder_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_contentbuilder` WRITE;
/*!40000 ALTER TABLE `matrixcontent_contentbuilder` DISABLE KEYS */;

INSERT INTO `matrixcontent_contentbuilder` (`id`, `elementId`, `locale`, `field_headline_headline`, `field_body_bodyCopy`, `field_image_imagePosition`, `field_pullQuote_quote`, `field_pullQuote_quoteAttribution`, `field_subHeadline_subHeadline`, `field_pullQuote_attributionTitle`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,44,'en_us',NULL,'<p>For more than 50 years, Friona Industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors: our livestock and our customers.</p>\r\n\r\n<p>We built this company in the north Texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship. We answer that expectation through innovative beef production and a reputation for exceeding customer expectations.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>At Friona Industries, quality means a fierce attention to detail from every employee on every job. To back that up, here is our <a href=\"{asset:74:url}\" target=\"_blank\">Code of Conduct</a>&nbsp;promise that our employees sign every year.</p>',NULL,NULL,NULL,NULL,NULL,'2015-12-09 20:31:25','2016-02-01 20:55:03','6123c8f4-7f0d-44dd-8d45-e5555c74d132'),
	(4,46,'en_us',NULL,'<p><a href=\"{asset:135:url}\">Sell Us Your Cattle</a></p><p>Friona Industries, L.P.</p>\r\n\r\n<p><strong>Mailing Address<br></strong>P.O. Box 15568<br>Amarillo, TX 79105-5568</p>\r\n\r\n\r\n\r\n\r\n\r\n<p><strong>Physical Address<br></strong>500 S. Taylor<br>Suite 601 (LB 253)<br>Amarillo, TX 79101</p>\r\n\r\n\r\n\r\n\r\n\r\n<p><strong>Telephone</strong><br>806-374-1811</p>\r\n\r\n\r\n\r\n\r\n\r\n<p><strong>Fax<br></strong>806-374-3003</p>\r\n\r\n\r\n\r\n\r\n\r\n<p><strong>Cattle Dept.<br></strong>806-220-2855</p>',NULL,NULL,NULL,NULL,NULL,'2015-12-09 20:48:53','2016-02-05 22:48:08','199fb8bc-e798-4391-a202-82f294b6b695'),
	(6,48,'en_us',NULL,'<p>Stewardship is an ethic that embodies the responsible planning and management of resources. At Friona Industries, we take stewardship seriously. You can see that in our <a href=\"{asset:74:url}\" target=\"_blank\">Code of Conduct</a>&nbsp;as well as in the <a href=\"http://www.bqa.org/about/value-to-the-industry\" target=\"true\">Beef Quality Assurance</a>&nbsp;practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards.</p>\r\n\r\n\r\n\r\n<p>A large part of the beef industry’s job involves making sure that beef is safe and wholesome for consumers. We work together with our cattle, animal health, and feed suppliers to maintain high standards for the beef we produce every day.</p>',NULL,NULL,NULL,NULL,NULL,'2015-12-09 20:52:35','2016-02-01 20:39:37','84845a90-0443-4208-b237-5a24086e706a'),
	(8,50,'en_us',NULL,'<p>Our Mission</p>\r\n\r\n<h1>DEDICATED EMPLOYEES.<br>INNOVATIVE BEEF. SATISFIED CONSUMER.</h1>',NULL,NULL,NULL,NULL,NULL,'2016-01-25 16:53:01','2016-02-05 15:32:53','812633b8-5464-45ed-b2b0-cd353db26f24'),
	(20,72,'en_us','OUR MISSION:',NULL,NULL,NULL,NULL,NULL,NULL,'2016-01-27 15:42:44','2016-02-01 20:55:03','186e501f-511e-4949-8a7e-008c271309b2'),
	(21,73,'en_us',NULL,NULL,NULL,NULL,NULL,'DEDICATED EMPLOYEES. INNOVATIVE BEEF. SATISFIED CUSTOMER.',NULL,'2016-01-27 15:42:44','2016-02-01 20:55:03','6f45434f-306d-41d5-8310-0fe8425a8a36'),
	(22,76,'en_us',NULL,'<p>The integrity of our highly motivated and dedicated employees and officers is one of the most valuable assets we have at Friona Industries. The foundation of our success and our ability to grow and take advantage of our resources and innovation is inspired by honesty and fairness through a culture of strong ethical behavior. </p>\r\n\r\n\r\n\r\n<p>New employees go through a training process so they understand how important their position is to cattle performance at the feedyard, as well as the financial performance of the company.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Each year, we ask our employees to agree to a Code of Conduct&nbsp;It explains our philosophical principles of employee conduct.</p>',NULL,NULL,NULL,NULL,NULL,'2016-01-27 15:56:18','2016-02-04 14:28:30','696a5d7d-ca8f-4d3e-b3f4-0b9fc385e78f'),
	(23,77,'en_us',NULL,'<p>Friona Industries, L. P. began as single small feedyard in Amarillo, TX in 1962 as the beef industry began to recognize the resources that existed in the Texas panhandle. Founding partners, like A.L. Black, were a group of cattle feeders who could see beyond the cattle to all the other businesses that cattle feeding supported: meat processing, feed manufacturing, and trucking, to name just a few.</p>\r\n\r\n\r\n\r\n<p>We have progressed to become one of the leading cattle feeding businesses in the world, led by visionaries such as Mr. Black and <a href=\"https://www.youtube.com/watch?v=kG-TbsbPTSw\" target=\"true\">James Herring</a>, both winners of the <a href=\"http://ranchingheritage.org/golden-spur-award/\" target=\"true\">National Golden Spur Award</a>, which honors outstanding achievement in ranching and livestock businesses. James Herring especially bolstered our business, and his work at Friona Industries led to <a href=\"https://hbr.org/search?term=friona\" target=\"true\">two Harvard School of Business Case Studies</a>&nbsp;on management and marketing.</p>\r\n\r\n\r\n\r\n<p>Most recently, Mr. Herring was honored with the Feeding Quality Forum’s <a href=\"http://www.agrimarketing.com/s/97970\" target=\"true\">2015 Industry Achievement Award</a>.</p>\r\n\r\n\r\n\r\n<p>Today, privately held Friona Industries has <a href=\"{entry:8:url}\">four state-of-the-art feedyards</a>&nbsp;in north Texas, with a feeding capacity that ranks us in the top 10 feedyards worldwide. We continue to focus on a vertically aligned production system that creates a consistent, safe, tender and flavorful beef for branded product lines marketed in 2,300 retails stores in the U.S.</p>\r\n\r\n\r\n\r\n<p>As our leaders at Friona Industries look forward, we recognize the challenges that face us as we address our stewardship responsibilities of both livestock and the land, while delivering high quality beef to our customers.</p>\r\n\r\n\r\n\r\n<p>Our mission statement communicates our belief that our customers seek quality beef products produced with the highest standards of stewardship. We continue our legacy of answering that expectation through innovative beef production and a reputation for exceeding customer expectations</p>',NULL,NULL,NULL,NULL,NULL,'2016-01-27 15:57:30','2016-01-27 15:57:30','d09f6eba-92e2-4376-be02-7cb202835d49'),
	(24,78,'en_us','Working at Friona',NULL,NULL,NULL,NULL,NULL,NULL,'2016-01-27 16:26:27','2016-02-04 14:28:30','6917eda1-99cd-40df-aae5-17e376cdc3eb'),
	(25,80,'en_us',NULL,NULL,NULL,NULL,NULL,'Jobs at Friona',NULL,'2016-01-27 16:36:30','2016-02-04 14:28:30','49e9c0e9-95c5-4979-ae8c-309ac9a52095'),
	(26,81,'en_us',NULL,'<p>There are three general career areas at Friona Industries: Feedyard Management & Office Support, Feedyard Operations, and Feed Mill Operations. Every job at Friona is important to the overall care and performance of the cattle in our feedyards, as well as to the overall financial performance of the company.</p>\r\n\r\n<p>Friona is always taking job applications&nbsp;for possible openings at our feedyards. Please click on the area below that you are interested in to see a general description and a printable job application that you can fill out and submit to Friona Industries.</p>\r\n\r\n\r\n\r\n\r\n\r\n<p>Each year, we ask our employees to agree to a Code of Conduct.&nbsp;It explains our philosophical principles of employee conduct.</p>\r\n\r\n\r\n\r\n\r\n\r\n<p><a href=\"{asset:82:url}\">Completed applications</a> can be mailed directly to the feedyard&nbsp;you are interested in working at or to Friona Industries, 500 S Taylor St, Suite 601, Amarillo, TX 79105, or it can be scanned and emailed to <a href=\"mailto:fi@frionaind.com\">fi@frionaind.com</a></p>',NULL,NULL,NULL,NULL,NULL,'2016-01-27 16:36:30','2016-02-04 14:28:30','ed1ce6cc-5df7-4086-9d77-82ef7262e6b4'),
	(27,83,'en_us',NULL,NULL,NULL,NULL,NULL,'Contact Info and Key Contacts',NULL,'2016-01-27 16:59:45','2016-02-05 22:48:08','2f396544-8556-43b9-9039-ce9c8bcc60af'),
	(28,85,'en_us',NULL,'<p>Here you will find the latest news and information about Friona Industries achievements and involvement with our communities.</p><p><strong>2015</strong></p><p>Portillo Honored as AVC Consultant of the Year</p><p><strong>2014</strong></p><p><a href=\"http://tscra.org/news_blog/2014/07/24/james-herring-to-receive-national-golden-spur-award/\" target=\"true\">James Herring to Receive National Golden Spur Award</a></p>',NULL,NULL,NULL,NULL,NULL,'2016-01-27 17:37:15','2016-01-27 17:37:15','29db2357-3ed6-43cd-b3e4-c877a34a397a'),
	(31,96,'en_us','U.S. Roundtable on Sustainable Beef',NULL,NULL,NULL,NULL,NULL,NULL,'2016-02-01 20:36:54','2016-02-01 20:36:54','7dcdcea7-66bb-46fc-b20f-efb9c8441457'),
	(32,97,'en_us',NULL,'<p>Friona Industries is one of the founding members of the <a href=\"https://www.usrsb.org/about.aspx\" target=\"_blank\">U.S. Roundtable on Sustainable Beef</a>&nbsp;(USRSB), which is a multi-stakeholder initiative developed to advance, support and communicate continuous improvement in sustainability of the U.S. beef value chain.</p>\r\n\r\n\r\n\r\n<p>Our representative to the group is Blaine Rotramel, our commodity procurement manager at Friona Industries. “We’re here to feed the world,” Rotramel says. “We’re using our knowledge to look at better ways to do things in our business.”</p>\r\n\r\n\r\n\r\n<p>USRSB focuses on four areas: Social (employees involved in the community), Economic (community impact), Environmental and Animal Health & Welfare.</p>\r\n\r\n\r\n\r\n<p>Friona Industries is an active supporter in each of those areas. For example, Animal Health & Welfare are addressed through many of the <a href=\"{entry:17:url}\">BQA activities</a> at each of the feedyards; and a recent economic impact study showed the Randall County Feedyard provided $7 million in business activity within a 100-mile radius of the location.</p>\r\n\r\n\r\n\r\n<p>The USRSB is working in collaboration with the <a href=\"http://grsbeef.org/\" target=\"_blank\">Global Roundtable for Sustainable Beef</a> (GRSB) and other sustainability initiatives whose aims are consistent with its mission and vision.</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-01 20:36:54','2016-02-01 20:36:54','b6c55d24-1f50-4bf2-82b4-2b929a99457e'),
	(34,99,'en_us',NULL,'<p>The <a href=\"https://www.enoughmovement.com/learn/index.aspx\" target=\"_blank\">ENOUGH Movement</a>&nbsp;is a global community working together to ensure that each person has access to nutritious, affordable food, both today and in the coming decades. The global effort is being led by <a href=\"https://www.enoughmovement.com/report/\" target=\"_blank\">Elanco</a>, one of our business partners.</p><p>Providing food globally is very important, and we take the responsibility of providing food in our own backyard to those who are in need. That’s why we’ve partnered the Texas Cattle Feeders Association in support of both the Snack Pak 4 Kids and the High Plains Food Bank.</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-01 20:38:22','2016-02-05 15:36:36','9d86cda2-65f4-4172-80d6-da9f6082a82f'),
	(35,115,'en_us','USRSB',NULL,NULL,NULL,NULL,NULL,NULL,'2016-02-03 22:46:29','2016-02-03 22:46:29','fd6fc7b1-9513-4864-8ce7-b3b5474f6144'),
	(36,116,'en_us',NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta lectus sed felis ullamcorper, nec congue mi semper. Vestibulum eu vehicula massa. Aenean et justo sem. Proin ac lectus odio. Aliquam ut fringilla velit. Donec dignissim sapien nec ipsum semper maximus. Vestibulum in vestibulum nisl, vitae sollicitudin metus. Nullam a malesuada dui, faucibus blandit nisi. Quisque egestas nibh egestas enim dictum dictum. Nam ante nulla, tincidunt eu consequat quis, faucibus ac ante. Curabitur ornare facilisis dui, sed blandit ante sodales ac. Vestibulum et tincidunt neque, in consequat augue. Etiam consectetur nulla at elit aliquet vestibulum. In vestibulum nisl et vestibulum lacinia.</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-03 22:46:29','2016-02-03 22:46:29','c26354b8-cf96-4220-ba85-64cfa69f5326'),
	(37,117,'en_us',NULL,NULL,NULL,NULL,NULL,'Feedyard Management & Office Support',NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','5a97e89a-8f4f-4a2c-bce6-71a86f66b5f0'),
	(38,118,'en_us',NULL,'<p>Careers in this area are in charge of operations and employees at the feedyard, as well as billing and bookkeeping records. Employees in these positions may deal with the public, input suppliers, service providers and FI customers. They are responsible for accurate records and adhering to federal and state regulations, as well as safety policies.</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','ad4d5c91-005b-4cf0-bcd0-f91436220ef1'),
	(39,119,'en_us',NULL,NULL,NULL,NULL,NULL,'Feedyard Operations',NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','ff4772ee-7b91-4647-b462-f1ffcffc5dad'),
	(40,120,'en_us',NULL,'<p>Careers in this area are in direct contact with the cattle in the feedyards. Employees in these positions may feed and move cattle, determine feeding schedules and rations, assess and treat cattle health issues, and work on yard maintenance.&nbsp;</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','8b717460-8357-4667-bc7c-c381957f4603'),
	(41,121,'en_us',NULL,NULL,NULL,NULL,NULL,'Feed Mill Operations',NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','64a07f85-a018-4892-831e-478d23ac9ff2'),
	(42,122,'en_us',NULL,'<p>Careers in this area are working with grain storage and processing equipment. Employees in these positions may handle commodities, operate mill equipment, and provide feed quality control.&nbsp;</p>\r\n\r\n',NULL,NULL,NULL,NULL,NULL,'2016-02-04 14:28:30','2016-02-04 14:28:30','ffb6e705-c934-4128-9565-e034bcfd9029'),
	(43,136,'en_us',NULL,'<p>Good health practices for our feedyard cattle start long before they arrive in the feedyard. That’s why we have established a <a href=\"{asset:135:url}\">compliance position statement</a>&nbsp;that we share with each of our feeder cattle suppliers.</p>\r\n\r\n<p>“Our model for animal health is to manage risk differently than what much of the industry does,” says Tom Portillo, DVM, and Friona Industries manager of animal health. “If you look at the investment we have with cattle, we recognize there is value in cattle that are vaccinated and backgrounded appropriately before they arrive in our feedyards,” he says.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Friona Industries is serious about this investment in cattle, and has two staff people assigned to working with backgrounders and checking the cattle management in their operations.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>The attention to health doesn’t stop at the gate when cattle are delivered to one of the four state-of-the-art feedyards that Friona Industries owns in north Texas. “Our goal is to look at each animal individually every day,” Portillo says. This is accomplished by having pen riders work through each pen of cattle, no matter what the weather conditions are that day.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>“Another important part of our animal health protocol is basic animal husbandry,” Portillo says. At Friona Industries, providing the basics of animal care through well-managed yards and dedicated employees is easy to implement because it is part of the day-to-day routine at each of the four feedyards.</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-05 15:35:14','2016-02-05 15:35:35','1ce3b765-a29d-4889-9a64-c49229c6de4e'),
	(44,138,'en_us',NULL,'<p>The public is looking for more transparency in the production of their food, including beef. That’s why Friona Industries has made a commitment to follow Beef Quality Assurance (BQA) protocols in their feedyards. “This is a platform that is good for us, as we are committed to animal welfare,” says Tom Portillo, DVM, the manager of animal health at Friona Industries.</p>\r\n\r\n\r\n\r\n<p>BQA reflects a positive public image and instills consumer confidence in the beef industry. When producers implement the best management practices of a BQA program, they assure their market steers, heifers, cows, and bulls are the best they can be. </p>\r\n\r\n\r\n\r\n<p>Friona Industries follows:<br><a href=\"http://www.bqa.org/Media/BQA/Docs/feedyard_assessment_062209_blank.pdf\" target=\"_blank\">BQA Assessments</a>&nbsp;by Third Party Auditors;<br><a href=\"http://www.bqa.org/Media/BQA/Docs/cchg2015_final.pdf\" target=\"_blank\">Annual BQA Training</a> for the Crews at Each Feedyard;<br>Judicious Use of <a href=\"http://www.bqa.org/Media/BQA/Docs/judiciousmicrobials.pdf\" target=\"_blank\">Antibiotics</a>&nbsp;and<br><a href=\"http://www.bqa.org/Media/BQA/Docs/supplemental_guidelines_2014.pdf\" target=\"_blank\">Continuous Review</a>&nbsp;of Practices.</p>',NULL,NULL,NULL,NULL,NULL,'2016-02-05 15:41:29','2016-02-05 15:41:29','4b024b86-857a-4650-8066-e0740c5e2afd');

/*!40000 ALTER TABLE `matrixcontent_contentbuilder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_keycontacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_keycontacts`;

CREATE TABLE `matrixcontent_keycontacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_contact_fullName` text COLLATE utf8_unicode_ci,
  `field_contact_position` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_keycontacts_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_keycontacts_locale_fk` (`locale`),
  CONSTRAINT `matrixcontent_keycontacts_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_keycontacts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_keycontacts` WRITE;
/*!40000 ALTER TABLE `matrixcontent_keycontacts` DISABLE KEYS */;

INSERT INTO `matrixcontent_keycontacts` (`id`, `elementId`, `locale`, `field_contact_fullName`, `field_contact_position`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,123,'en_us','Brad Stout','Chief Operating Officer','2016-02-04 16:44:13','2016-02-05 22:48:08','fb574ff3-07d7-4f7a-91ba-7e0e2123341e'),
	(2,124,'en_us','Clay Allen','Chief Financial Officer','2016-02-04 16:44:13','2016-02-05 22:48:08','ec96f4e4-5b95-4ce3-8687-6eccf9a155d9'),
	(3,125,'en_us','Russell Artho    ','Controller','2016-02-04 16:44:13','2016-02-05 22:48:08','53183737-6a91-45e3-80b3-f7779805ce2e'),
	(4,126,'en_us','Jay Cortese','Cattle Procurement Manager','2016-02-04 16:44:13','2016-02-05 22:48:08','8e4f491e-1be3-40f8-bf30-11bc17e2c199'),
	(5,127,'en_us','Brady Bressler ','Cattle Procurement Assistant Manager','2016-02-04 16:44:13','2016-02-05 22:48:08','cac7d86f-d98f-45b7-9814-314f7d0c1f09'),
	(6,128,'en_us','Mark Hooker','Fed Cattle Manager ','2016-02-04 16:44:13','2016-02-05 22:48:08','cf2ce1e3-8f05-4a79-836b-81abf63dc62c'),
	(7,129,'en_us','Karli Schilling','Assistant Fed Cattle Marketing Manager','2016-02-04 16:44:13','2016-02-05 22:48:08','6a303257-807b-42b8-9ebc-be8d8c2c8917'),
	(8,130,'en_us','Blaine Rotramel   ','Commodity Procurement Manager','2016-02-04 16:44:13','2016-02-05 22:48:08','af00d203-4be5-42b1-a98b-2df6c746dffd'),
	(9,131,'en_us','Dell Volmer','Feedyard Operations Manager ','2016-02-04 16:44:13','2016-02-05 22:48:08','bb28f07c-f35a-43ea-a781-4b03af4e852b'),
	(10,132,'en_us','Eddie Derrick ','Growyard & Pasture Cattle Manager ','2016-02-04 16:44:13','2016-02-05 22:48:08','8c315499-75a9-4fd8-ac69-af618cdb5b31'),
	(11,133,'en_us','Tom Portillo','DVM Manager of Animal Health','2016-02-04 16:44:13','2016-02-05 22:48:08','1694401f-d488-41e8-b85f-b3660877deb0');

/*!40000 ALTER TABLE `matrixcontent_keycontacts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_locationcontact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_locationcontact`;

CREATE TABLE `matrixcontent_locationcontact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_contact_fullName` text COLLATE utf8_unicode_ci,
  `field_contact_position` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_locationcontact_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_locationcontact_locale_fk` (`locale`),
  CONSTRAINT `matrixcontent_locationcontact_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_locationcontact_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_locationcontact` WRITE;
/*!40000 ALTER TABLE `matrixcontent_locationcontact` DISABLE KEYS */;

INSERT INTO `matrixcontent_locationcontact` (`id`, `elementId`, `locale`, `field_contact_fullName`, `field_contact_position`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,134,'en_us','Dave Link','Feedyard Manager','2016-02-04 17:30:53','2016-02-05 22:38:20','aa1e6a33-1f81-462c-ab34-b3f6ddffd238'),
	(2,176,'en_us','James Glover','Assistant Feedyard Manager','2016-02-05 22:32:51','2016-02-05 22:38:20','3e13583f-f076-4871-bd46-b6b1dffad18b'),
	(3,177,'en_us','Jayn Looper','Office Manager','2016-02-05 22:32:51','2016-02-05 22:38:20','4535a5ea-0260-4f51-9eb8-551d269c343d'),
	(4,178,'en_us','Ronnie Gonzales','Feedyard Manager','2016-02-05 22:37:36','2016-02-05 22:37:36','845a8a56-5904-4013-bb53-c09096a09dfd'),
	(5,179,'en_us','Jared Lee','Assistant Feedyard Manager','2016-02-05 22:37:36','2016-02-05 22:37:36','353cbc54-473f-4c23-b987-4045538089ff'),
	(6,180,'en_us','Tanya Griswold','Office Manager','2016-02-05 22:37:36','2016-02-05 22:37:36','537915f3-7f1d-41ad-8ba8-d884b94f69e5'),
	(7,181,'en_us','Jerrid Vincent','Feedyard Manager','2016-02-05 22:40:23','2016-02-05 22:40:23','2a26e179-3225-4a00-aff4-aa578d086769'),
	(8,182,'en_us','Barry Chew','Assistant Feedyard Manager','2016-02-05 22:40:23','2016-02-05 22:40:23','1a13d1f6-33dd-4044-8de7-0554d9449aa4'),
	(9,183,'en_us','Sue Doxon','Office Manager','2016-02-05 22:40:23','2016-02-05 22:40:23','8b84ea09-1faa-4387-8190-4bed58aa0390'),
	(10,184,'en_us','Trevor Peterson','Feedyard Manager','2016-02-05 22:42:30','2016-02-05 22:42:30','9c19d83d-4c82-47e0-8628-9e217a1f287b'),
	(11,185,'en_us','Clif Yeary','Assistant Feedyard Manager','2016-02-05 22:42:30','2016-02-05 22:42:30','8fca553d-68a7-4499-96e9-7bac9cb7368e'),
	(12,186,'en_us','Mary Young','Office Manager','2016-02-05 22:42:30','2016-02-05 22:42:30','937785e2-2678-4264-be67-7128eabe2c0a');

/*!40000 ALTER TABLE `matrixcontent_locationcontact` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_slidebuilder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_slidebuilder`;

CREATE TABLE `matrixcontent_slidebuilder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_slide_headline` text COLLATE utf8_unicode_ci,
  `field_slide_summary` text COLLATE utf8_unicode_ci,
  `field_slide_slideUrl` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_slidebuilder_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_slidebuilder_locale_idx` (`locale`),
  CONSTRAINT `matrixcontent_slidebuilder_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_slidebuilder_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_slidebuilder` WRITE;
/*!40000 ALTER TABLE `matrixcontent_slidebuilder` DISABLE KEYS */;

INSERT INTO `matrixcontent_slidebuilder` (`id`, `elementId`, `locale`, `field_slide_headline`, `field_slide_summary`, `field_slide_slideUrl`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,111,'en_us','Beef Safety Starts with Cattle Procurement','<p>Good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices.&nbsp;</p>','','2016-02-03 19:49:07','2016-02-05 15:32:53','1086e15a-54b2-458d-93e1-440dfc96405f'),
	(2,112,'en_us','Dedicated Employees Make Friona Industries Successful','<p>Our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef.&nbsp;</p>','','2016-02-03 20:40:13','2016-02-05 15:32:53','f244b898-5462-4a23-882a-ee43864db35d'),
	(3,113,'en_us','Making a Positive Economic Impact','<p>We use local services and buy local inputs for our four state-of-the-art feedyards</p>','http://frionaindustries.sandboxdsm.com/stewardship/usrsb','2016-02-03 20:40:56','2016-02-05 15:32:53','a62361e5-0388-4e95-9328-5f36d76f0e77');

/*!40000 ALTER TABLE `matrixcontent_slidebuilder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `migrations_version_unq_idx` (`version`),
  KEY `migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'m000000_000000_base','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','e27dbe20-24ff-4307-98c5-233162c599c1'),
	(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','833bf052-2756-4c4a-bc5a-6efe502fee9a'),
	(3,NULL,'m140815_000001_add_format_to_transforms','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','02bd9ff0-a0d3-42dd-afac-68af34fe5900'),
	(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','4d1847a4-0748-46c9-a6df-b0aa0d926836'),
	(5,NULL,'m140829_000001_single_title_formats','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','582c2475-9cfe-4c82-bf5c-32484729be0d'),
	(6,NULL,'m140831_000001_extended_cache_keys','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','745a2a08-074a-4419-b02d-cf0f0c4d64fd'),
	(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','f6375c8c-35a9-4859-9889-74adb7f32c2d'),
	(8,NULL,'m141008_000001_elements_index_tune','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','2f1eaf35-715b-48ef-bd91-b1b78fa9b625'),
	(9,NULL,'m141009_000001_assets_source_handle','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','2054eb56-9bb2-4e5c-aad2-706a9def1630'),
	(10,NULL,'m141024_000001_field_layout_tabs','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','2cf4dd63-2c92-4ac3-8b1b-e05230668249'),
	(11,NULL,'m141030_000001_drop_structure_move_permission','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','50ee8b60-0725-432d-a9c9-8b2abf3104af'),
	(12,NULL,'m141103_000001_tag_titles','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','052e8676-2928-4ba3-a7f6-b0a3b49f4210'),
	(13,NULL,'m141109_000001_user_status_shuffle','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','5c6eae7c-fc5c-4409-977a-36c4f2e3a069'),
	(14,NULL,'m141126_000001_user_week_start_day','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','06b4227b-50fe-454b-9715-ea1b939f06f5'),
	(15,NULL,'m150210_000001_adjust_user_photo_size','2015-11-18 21:02:47','2015-11-18 21:02:47','2015-11-18 21:02:47','49923fa4-fc15-451d-8b93-cdada0ca5ebb'),
	(21,4,'m140330_000000_smartMap_addCountrySubfield','2015-12-01 15:31:50','2015-12-01 15:31:50','2015-12-01 15:31:50','f64fe41c-fb30-4fc5-9f00-65db5c980bfd'),
	(22,4,'m140330_000001_smartMap_autofillCountryForExistingAddresses','2015-12-01 15:31:50','2015-12-01 15:31:50','2015-12-01 15:31:50','989aa7dc-b9b1-45e2-ae06-69343c22c919'),
	(23,4,'m140811_000001_smartMap_changeHandleToFieldId','2015-12-01 15:31:50','2015-12-01 15:31:50','2015-12-01 15:31:50','e432bf6a-323d-4122-872d-c49b83ecdf6f'),
	(24,4,'m150329_000000_smartMap_splitGoogleApiKeys','2015-12-01 15:31:50','2015-12-01 15:31:50','2015-12-01 15:31:50','026abda7-5b5b-476b-a102-16d10310a43f'),
	(25,4,'m150331_000000_smartMap_reorganizeGeolocationOptions','2015-12-01 15:31:50','2015-12-01 15:31:50','2015-12-01 15:31:50','9fbc6883-9f7b-4bf8-a31b-17926bf07a14'),
	(26,NULL,'m141030_000000_plugin_schema_versions','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','9cebfcbb-2cb8-4e69-b30f-0c20a1f136d7'),
	(27,NULL,'m150724_000001_adjust_quality_settings','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','0c4453ea-372c-4142-ac21-fa884a7bf5f4'),
	(28,NULL,'m150827_000000_element_index_settings','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','5df7f549-fdfa-449d-8765-888f3109d9de'),
	(29,NULL,'m150918_000001_add_colspan_to_widgets','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','c5054702-160e-47c6-923b-28a7d637143b'),
	(30,NULL,'m151007_000000_clear_asset_caches','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','bf68695e-278a-4781-b3fa-149bceb7af3e'),
	(31,NULL,'m151109_000000_text_url_formats','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','3f74e87a-0e4b-4e8d-ad0a-1be28f455685'),
	(32,NULL,'m151110_000000_move_logo','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','3aa8103c-3b6f-4a60-b433-5562c3afacce'),
	(33,NULL,'m151117_000000_adjust_image_widthheight','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','aec82f6c-a3f5-4827-bfa7-9d4ad4c43af6'),
	(34,NULL,'m151127_000000_clear_license_key_status','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','2a1dc501-5bcc-4069-bca8-deca02398a4b'),
	(35,NULL,'m151127_000000_plugin_license_keys','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','9860f469-e692-4aa9-a169-c52f0582e617'),
	(36,NULL,'m151130_000000_update_pt_widget_feeds','2015-12-04 17:00:43','2015-12-04 17:00:43','2015-12-04 17:00:43','68b4f1a4-1572-4f62-b03a-6dd79a3a320c');

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins`;

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;

INSERT INTO `plugins` (`id`, `class`, `version`, `schemaVersion`, `licenseKey`, `licenseKeyStatus`, `enabled`, `settings`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,'ElementApi','1.0',NULL,NULL,'unknown',1,NULL,'2015-11-20 20:09:58','2015-11-20 20:09:58','2016-02-05 17:33:51','15d65a1b-935a-41a3-874e-06ceed27f465'),
	(4,'SmartMap','2.3.2','2.3.0',NULL,'unknown',1,NULL,'2015-12-01 15:31:50','2015-12-01 15:31:50','2016-02-05 17:33:51','3e547042-2512-4cb0-b1f4-9eb0feee53f4'),
	(5,'Recaptcha','1.0',NULL,NULL,'unknown',1,'{\"siteKey\":\"6LfcmRYTAAAAAOF755Id6lQv2hfaqc6vCB01qC-_\",\"secretKey\":\"6LfcmRYTAAAAAHW8jr3uepuVY4q4W8fZXcOn0NKF\"}','2016-01-27 18:53:05','2016-01-27 18:53:05','2016-02-05 17:33:51','9490e058-b2e4-445f-b177-a82598b87305'),
	(11,'ContactForm','1.0',NULL,NULL,'unknown',1,'{\"toEmail\":\"dgentry@sandboxww.com\",\"adminSubject\":\"New message from Friona Industries\",\"guestSubject\":\"Hello from Friona Industries\",\"welcomeEmailMessage\":\"<p>Hi {{firstName}} &mdash; thank you for reaching out to us!<br>We wanted to drop you a quick note that we\'ve received your information and we\'ll be in touch very soon!<\\/p><p>Regards,<br>Friona Industries<\\/p>\"}','2016-02-03 18:53:39','2016-02-03 18:53:39','2016-02-05 17:33:51','c5a58535-ae32-4da1-bdf6-f1c5a6833f93');

/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rackspaceaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rackspaceaccess`;

CREATE TABLE `rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relations`;

CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `relations_sourceId_fk` (`sourceId`),
  KEY `relations_sourceLocale_fk` (`sourceLocale`),
  KEY `relations_targetId_fk` (`targetId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;

INSERT INTO `relations` (`id`, `fieldId`, `sourceId`, `sourceLocale`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(109,41,111,NULL,89,1,'2016-02-05 15:32:53','2016-02-05 15:32:53','199c738f-60d5-447c-b6a2-ed0bdf2f2224'),
	(110,40,111,NULL,135,1,'2016-02-05 15:32:53','2016-02-05 15:32:53','0a48d00a-1b75-4158-b569-cc0591d864b7'),
	(111,41,112,NULL,91,1,'2016-02-05 15:32:53','2016-02-05 15:32:53','db07a09b-906c-4b54-9246-e46e6866c2b7'),
	(112,40,112,NULL,74,1,'2016-02-05 15:32:53','2016-02-05 15:32:53','2d47ec1b-784d-48c3-8a2a-aae4945c86d0'),
	(113,41,113,NULL,93,1,'2016-02-05 15:32:53','2016-02-05 15:32:53','ad4cc1bb-96d2-4e93-b378-910eae12f5df'),
	(118,48,178,NULL,141,1,'2016-02-05 22:37:36','2016-02-05 22:37:36','c2167235-b2a4-4462-85ab-511346757155'),
	(119,48,179,NULL,160,1,'2016-02-05 22:37:36','2016-02-05 22:37:36','4ea359d1-7af6-470a-a885-887505a0dde3'),
	(120,48,180,NULL,161,1,'2016-02-05 22:37:36','2016-02-05 22:37:36','df45f93d-5ba5-455f-85b6-2b83aa7602d9'),
	(121,48,134,NULL,139,1,'2016-02-05 22:38:20','2016-02-05 22:38:20','153b3ae6-c70c-4112-8f94-b74cf6e6b21c'),
	(122,48,176,NULL,153,1,'2016-02-05 22:38:20','2016-02-05 22:38:20','2c62d2e8-c51d-4725-93e7-81845344c9ef'),
	(123,48,177,NULL,157,1,'2016-02-05 22:38:20','2016-02-05 22:38:20','7899e40d-ba0d-4e89-869a-53b4df9a7525'),
	(124,48,181,NULL,140,1,'2016-02-05 22:40:23','2016-02-05 22:40:23','0681fd92-0599-4a48-a4c3-704dadb2d321'),
	(125,48,182,NULL,164,1,'2016-02-05 22:40:23','2016-02-05 22:40:23','0e6b0ee7-7c69-40af-8496-67368a3359c4'),
	(126,48,183,NULL,169,1,'2016-02-05 22:40:23','2016-02-05 22:40:23','e6286e2e-9c73-4361-90eb-2ebc221dd733'),
	(127,48,184,NULL,173,1,'2016-02-05 22:42:30','2016-02-05 22:42:30','f4d9bc7d-c66e-40ab-a74d-bafae495c559'),
	(128,48,185,NULL,172,1,'2016-02-05 22:42:30','2016-02-05 22:42:30','42a665e4-81e7-429f-ae94-46f5e58f8273'),
	(129,48,186,NULL,174,1,'2016-02-05 22:42:30','2016-02-05 22:42:30','b92115f5-a7aa-4d9c-8ab2-fd6fa2d4e4e1');

/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `routes_locale_idx` (`locale`),
  CONSTRAINT `routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `searchindex`;

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`)
VALUES
	(144,'kind',0,'en_us',' image '),
	(144,'extension',0,'en_us',' jpg '),
	(144,'filename',0,'en_us',' feedyard 2 jpg '),
	(143,'title',0,'en_us',' feedyard 1 '),
	(143,'slug',0,'en_us',''),
	(143,'kind',0,'en_us',' image '),
	(143,'extension',0,'en_us',' jpg '),
	(82,'filename',0,'en_us',' fi employment application pdf '),
	(82,'extension',0,'en_us',' pdf '),
	(82,'kind',0,'en_us',' pdf '),
	(82,'slug',0,'en_us',' fi employment application '),
	(82,'title',0,'en_us',' fi employment application '),
	(74,'filename',0,'en_us',' fi ethics letter january 2016 newlogo pd pdf '),
	(74,'extension',0,'en_us',' pdf '),
	(74,'kind',0,'en_us',' pdf '),
	(74,'slug',0,'en_us',' fi ethics letter january 2016 new logo pd '),
	(74,'title',0,'en_us',' fi ethics letter january 2016 new logo pd '),
	(89,'filename',0,'en_us',' pasture cattle portrait jpg '),
	(89,'extension',0,'en_us',' jpg '),
	(89,'kind',0,'en_us',' image '),
	(89,'slug',0,'en_us',' pasture cattle portrait '),
	(89,'title',0,'en_us',' pasture cattle portrait '),
	(91,'filename',0,'en_us',' friona cowboy jpg '),
	(91,'extension',0,'en_us',' jpg '),
	(91,'kind',0,'en_us',' image '),
	(91,'slug',0,'en_us',' friona cowboy '),
	(91,'title',0,'en_us',' friona cowboy '),
	(93,'filename',0,'en_us',' randall cattle with mill jpg '),
	(93,'extension',0,'en_us',' jpg '),
	(93,'kind',0,'en_us',' image '),
	(93,'slug',0,'en_us',' randall cattle with mill '),
	(93,'title',0,'en_us',' randall cattle with mill '),
	(150,'filename',0,'en_us',' friona 3 jpg '),
	(149,'title',0,'en_us',' friona 2 '),
	(149,'slug',0,'en_us',''),
	(149,'kind',0,'en_us',' image '),
	(149,'extension',0,'en_us',' jpg '),
	(149,'filename',0,'en_us',' friona 2 jpg '),
	(148,'title',0,'en_us',' friona 1 '),
	(148,'slug',0,'en_us',''),
	(148,'kind',0,'en_us',' image '),
	(148,'extension',0,'en_us',' jpg '),
	(148,'filename',0,'en_us',' friona 1 jpg '),
	(147,'title',0,'en_us',' fibrand '),
	(147,'slug',0,'en_us',''),
	(147,'kind',0,'en_us',' image '),
	(147,'extension',0,'en_us',' jpg '),
	(147,'filename',0,'en_us',' fibrand jpg '),
	(146,'title',0,'en_us',' fi pasture 3 '),
	(146,'slug',0,'en_us',''),
	(146,'kind',0,'en_us',' image '),
	(146,'extension',0,'en_us',' jpg '),
	(146,'filename',0,'en_us',' fi pasture 3 jpg '),
	(145,'title',0,'en_us',' fi pasture 1 '),
	(145,'slug',0,'en_us',''),
	(145,'kind',0,'en_us',' image '),
	(145,'extension',0,'en_us',' jpg '),
	(145,'filename',0,'en_us',' fi pasture 1 jpg '),
	(144,'title',0,'en_us',' feedyard 2 '),
	(144,'slug',0,'en_us',''),
	(143,'filename',0,'en_us',' feedyard 1 jpg '),
	(142,'title',0,'en_us',' cattle at feeder '),
	(142,'slug',0,'en_us',''),
	(142,'kind',0,'en_us',' image '),
	(142,'extension',0,'en_us',' jpg '),
	(142,'filename',0,'en_us',' cattle at feeder jpg '),
	(20,'slug',0,'en_us',' friona feedyard '),
	(20,'title',0,'en_us',' friona feedyard '),
	(20,'field',1,'en_us',' sell your cattle to friona feedyard '),
	(136,'field',28,'en_us',' good health practices for our feedyard cattle start long before they arrive in the feedyard that s why we have established a compliance position statement that we share with each of our feeder cattle suppliers our model for animal health is to manage risk differently than what much of the industry does says tom portillo dvm and friona industries manager of animal health if you look at the investment we have with cattle we recognize there is value in cattle that are vaccinated and backgrounded appropriately before they arrive in our feedyards he says friona industries is serious about this investment in cattle and has two staff people assigned to working with backgrounders and checking the cattle management in their operations the attention to health doesn t stop at the gate when cattle are delivered to one of the four state of the art feedyards that friona industries owns in north texas our goal is to look at each animal individually every day portillo says this is accomplished by having pen riders work through each pen of cattle no matter what the weather conditions are that day another important part of our animal health protocol is basic animal husbandry portillo says at friona industries providing the basics of animal care through well managed yards and dedicated employees is easy to implement because it is part of the day to day routine at each of the four feedyards '),
	(20,'field',14,'en_us',' 2370 fm 3140 friona tx 79035 united states 34 63750330 102 63953410 6935 4469086397 '),
	(20,'field',16,'en_us',''),
	(20,'field',15,'en_us',''),
	(20,'field',5,'en_us',' john smith '),
	(20,'field',4,'en_us',' john frionaindustries com '),
	(20,'field',8,'en_us',''),
	(21,'slug',0,'en_us',' littlefield feedyard '),
	(21,'title',0,'en_us',' littlefield feedyard '),
	(21,'field',1,'en_us',' sell your cattle to littlefield feedyard '),
	(21,'field',14,'en_us',' 1640 fm 37 amherst tx 79312 united states 34 01098600 102 26028080 6919 3190037769 '),
	(21,'field',16,'en_us',''),
	(21,'field',15,'en_us',''),
	(21,'field',5,'en_us',''),
	(21,'field',4,'en_us',''),
	(21,'field',8,'en_us',''),
	(22,'slug',0,'en_us',' randall county feedyard '),
	(22,'title',0,'en_us',' randall county feedyard '),
	(22,'field',1,'en_us',' sell your cattle to randall county feedyard '),
	(132,'field',44,'en_us',' growyard pasture cattle manager '),
	(132,'slug',0,'en_us',''),
	(133,'field',43,'en_us',' tom portillo '),
	(133,'field',44,'en_us',' dvm manager of animal health '),
	(133,'slug',0,'en_us',''),
	(22,'field',14,'en_us',' 15000 fm 2219 amarillo tx 79119 united states 35 05951680 102 01210470 6896 529471462 '),
	(22,'field',16,'en_us',''),
	(22,'field',15,'en_us',''),
	(22,'field',5,'en_us',''),
	(22,'field',4,'en_us',''),
	(22,'field',8,'en_us',''),
	(23,'slug',0,'en_us',' swisher county cattle company '),
	(23,'title',0,'en_us',' swisher county cattle company '),
	(23,'field',1,'en_us',' sell your cattle to swisher county feedyard '),
	(129,'slug',0,'en_us',''),
	(130,'field',43,'en_us',' blaine rotramel '),
	(130,'field',44,'en_us',' commodity procurement manager '),
	(130,'slug',0,'en_us',''),
	(131,'field',43,'en_us',' dell volmer '),
	(131,'field',44,'en_us',' feedyard operations manager '),
	(131,'slug',0,'en_us',''),
	(132,'field',43,'en_us',' eddie derrick '),
	(23,'field',14,'en_us',' 6556 fm 214 happy tx 79042 united states 34 63136210 101 88916700 6893 1602266747 '),
	(23,'field',16,'en_us',''),
	(23,'field',15,'en_us',''),
	(23,'field',5,'en_us',''),
	(23,'field',4,'en_us',''),
	(23,'field',8,'en_us',''),
	(24,'slug',0,'en_us',' corporate office '),
	(24,'title',0,'en_us',' friona industries corporate office '),
	(24,'field',1,'en_us',' the corporate office for friona industries is located in amarillo texas additionally we have four state of the art feedyards in north texas with a total feeding capacity that ranks us in the top 10 feedyards worldwide '),
	(128,'field',44,'en_us',' fed cattle manager '),
	(128,'slug',0,'en_us',''),
	(129,'field',43,'en_us',' karli schilling '),
	(129,'field',44,'en_us',' assistant fed cattle marketing manager '),
	(24,'field',14,'en_us',' 500 s taylor st suite 601 amarillo tx 79101 united states 35 20810300 101 83630000 6885 4289304666 '),
	(24,'field',16,'en_us',' fi frionaind com '),
	(24,'field',15,'en_us',' 806 374 1811 '),
	(24,'field',5,'en_us',''),
	(24,'field',4,'en_us',''),
	(24,'field',8,'en_us',''),
	(2,'slug',0,'en_us',' homepage '),
	(2,'title',0,'en_us',' homepage '),
	(2,'field',17,'en_us',' our mission dedicated employees innovative beef satisfied consumer '),
	(2,'field',8,'en_us',''),
	(4,'slug',0,'en_us',' about us '),
	(4,'title',0,'en_us',' about us '),
	(4,'field',17,'en_us',' our mission dedicated employees innovative beef satisfied customer for more than 50 years friona industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors our livestock and our customers we built this company in the north texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship we answer that expectation through innovative beef production and a reputation for exceeding customer expectations at friona industries quality means a fierce attention to detail from every employee on every job to back that up here is our code of conduct promise that our employees sign every year '),
	(4,'field',8,'en_us',''),
	(5,'slug',0,'en_us',' history '),
	(5,'title',0,'en_us',' history legacy '),
	(5,'field',17,'en_us',' friona industries l p began as single small feedyard in amarillo tx in 1962 as the beef industry began to recognize the resources that existed in the texas panhandle founding partners like a l black were a group of cattle feeders who could see beyond the cattle to all the other businesses that cattle feeding supported meat processing feed manufacturing and trucking to name just a few we have progressed to become one of the leading cattle feeding businesses in the world led by visionaries such as mr black and james herring both winners of the national golden spur award which honors outstanding achievement in ranching and livestock businesses james herring especially bolstered our business and his work at friona industries led to two harvard school of business case studies on management and marketing most recently mr herring was honored with the feeding quality forum s 2015 industry achievement award today privately held friona industries has four state of the art feedyards in north texas with a feeding capacity that ranks us in the top 10 feedyards worldwide we continue to focus on a vertically aligned production system that creates a consistent safe tender and flavorful beef for branded product lines marketed in 2 300 retails stores in the u s as our leaders at friona industries look forward we recognize the challenges that face us as we address our stewardship responsibilities of both livestock and the land while delivering high quality beef to our customers our mission statement communicates our belief that our customers seek quality beef products produced with the highest standards of stewardship we continue our legacy of answering that expectation through innovative beef production and a reputation for exceeding customer expectations '),
	(5,'field',8,'en_us',' friona industries l p began as single small feedyard in amarillo tx in 1962 in the texas panhandle '),
	(7,'slug',0,'en_us',' affilliations '),
	(7,'title',0,'en_us',' affiliations '),
	(7,'field',17,'en_us',''),
	(7,'field',8,'en_us',''),
	(8,'slug',0,'en_us',' locations '),
	(8,'title',0,'en_us',' locations '),
	(8,'field',17,'en_us',''),
	(124,'field',44,'en_us',' chief financial officer '),
	(124,'slug',0,'en_us',''),
	(125,'field',43,'en_us',' russell artho '),
	(125,'field',44,'en_us',' controller '),
	(125,'slug',0,'en_us',''),
	(126,'field',43,'en_us',' jay cortese '),
	(126,'field',44,'en_us',' cattle procurement manager '),
	(126,'slug',0,'en_us',''),
	(127,'field',43,'en_us',' brady bressler '),
	(127,'field',44,'en_us',' cattle procurement assistant manager '),
	(127,'slug',0,'en_us',''),
	(128,'field',43,'en_us',' mark hooker '),
	(8,'field',8,'en_us',''),
	(14,'slug',0,'en_us',' careers '),
	(14,'title',0,'en_us',' careers '),
	(14,'field',17,'en_us',' working at friona the integrity of our highly motivated and dedicated employees and officers is one of the most valuable assets we have at friona industries the foundation of our success and our ability to grow and take advantage of our resources and innovation is inspired by honesty and fairness through a culture of strong ethical behavior new employees go through a training process so they understand how important their position is to cattle performance at the feedyard as well as the financial performance of the company each year we ask our employees to agree to a code of conduct it explains our philosophical principles of employee conduct jobs at friona there are three general career areas at friona industries feedyard management office support feedyard operations and feed mill operations every job at friona is important to the overall care and performance of the cattle in our feedyards as well as to the overall financial performance of the company friona is always taking job applications for possible openings at our feedyards please click on the area below that you are interested in to see a general description and a printable job application that you can fill out and submit to friona industries each year we ask our employees to agree to a code of conduct it explains our philosophical principles of employee conduct completed applications can be mailed directly to the feedyard you are interested in working at or to friona industries 500 s taylor st suite 601 amarillo tx 79105 or it can be scanned and emailed to fi frionaind com feedyard management office support careers in this area are in charge of operations and employees at the feedyard as well as billing and bookkeeping records employees in these positions may deal with the public input suppliers service providers and fi customers they are responsible for accurate records and adhering to federal and state regulations as well as safety policies feedyard operations careers in this area are in direct contact with the cattle in the feedyards employees in these positions may feed and move cattle determine feeding schedules and rations assess and treat cattle health issues and work on yard maintenance feed mill operations careers in this area are working with grain storage and processing equipment employees in these positions may handle commodities operate mill equipment and provide feed quality control '),
	(14,'field',8,'en_us',''),
	(15,'slug',0,'en_us',' sustainability '),
	(15,'title',0,'en_us',' stewardship '),
	(15,'field',17,'en_us',' stewardship is an ethic that embodies the responsible planning and management of resources at friona industries we take stewardship seriously you can see that in our code of conduct as well as in the beef quality assurance practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards a large part of the beef industry s job involves making sure that beef is safe and wholesome for consumers we work together with our cattle animal health and feed suppliers to maintain high standards for the beef we produce every day '),
	(15,'field',8,'en_us',' a large part of the beef industry s job involves making sure that beef is safe and wholesome for consumers '),
	(16,'slug',0,'en_us',' contact us '),
	(16,'title',0,'en_us',' contact us '),
	(16,'field',17,'en_us',' contact info and key contacts sell us your cattlefriona industries l p mailing addressp o box 15568amarillo tx 79105 5568 physical address500 s taylorsuite 601 lb 253 amarillo tx 79101 telephone806 374 1811 fax806 374 3003 cattle dept 806 220 2855 '),
	(16,'field',8,'en_us',''),
	(17,'slug',0,'en_us',' animal health '),
	(17,'title',0,'en_us',' animal health '),
	(17,'field',17,'en_us',' good health practices for our feedyard cattle start long before they arrive in the feedyard that s why we have established a compliance position statement that we share with each of our feeder cattle suppliers our model for animal health is to manage risk differently than what much of the industry does says tom portillo dvm and friona industries manager of animal health if you look at the investment we have with cattle we recognize there is value in cattle that are vaccinated and backgrounded appropriately before they arrive in our feedyards he says friona industries is serious about this investment in cattle and has two staff people assigned to working with backgrounders and checking the cattle management in their operations the attention to health doesn t stop at the gate when cattle are delivered to one of the four state of the art feedyards that friona industries owns in north texas our goal is to look at each animal individually every day portillo says this is accomplished by having pen riders work through each pen of cattle no matter what the weather conditions are that day another important part of our animal health protocol is basic animal husbandry portillo says at friona industries providing the basics of animal care through well managed yards and dedicated employees is easy to implement because it is part of the day to day routine at each of the four feedyards '),
	(17,'field',8,'en_us',''),
	(18,'slug',0,'en_us',' community '),
	(18,'title',0,'en_us',' community '),
	(18,'field',17,'en_us',' u s roundtable on sustainable beef friona industries is one of the founding members of the u s roundtable on sustainable beef usrsb which is a multi stakeholder initiative developed to advance support and communicate continuous improvement in sustainability of the u s beef value chain our representative to the group is blaine rotramel our commodity procurement manager at friona industries we re here to feed the world rotramel says we re using our knowledge to look at better ways to do things in our business usrsb focuses on four areas social employees involved in the community economic community impact environmental and animal health welfare friona industries is an active supporter in each of those areas for example animal health welfare are addressed through many of the bqa activities at each of the feedyards and a recent economic impact study showed the randall county feedyard provided $7 million in business activity within a 100 mile radius of the location the usrsb is working in collaboration with the global roundtable for sustainable beef grsb and other sustainability initiatives whose aims are consistent with its mission and vision '),
	(18,'field',8,'en_us',''),
	(19,'slug',0,'en_us',' enough movement '),
	(19,'title',0,'en_us',' enough '),
	(19,'field',17,'en_us',' the enough movement is a global community working together to ensure that each person has access to nutritious affordable food both today and in the coming decades the global effort is being led by elanco one of our business partners providing food globally is very important and we take the responsibility of providing food in our own backyard to those who are in need that s why we ve partnered the texas cattle feeders association in support of both the snack pak 4 kids and the high plains food bank '),
	(19,'field',8,'en_us',''),
	(160,'slug',0,'en_us',''),
	(155,'title',0,'en_us',' friona day '),
	(156,'filename',0,'en_us',' friona manager_160205_143952 jpg '),
	(156,'extension',0,'en_us',' jpg '),
	(156,'kind',0,'en_us',' image '),
	(156,'slug',0,'en_us',''),
	(156,'title',0,'en_us',' friona manager '),
	(157,'filename',0,'en_us',' friona office manager jpg '),
	(157,'extension',0,'en_us',' jpg '),
	(157,'kind',0,'en_us',' image '),
	(157,'slug',0,'en_us',''),
	(157,'title',0,'en_us',' friona office manager '),
	(158,'filename',0,'en_us',' friona1 jpg '),
	(158,'extension',0,'en_us',' jpg '),
	(158,'kind',0,'en_us',' image '),
	(158,'slug',0,'en_us',''),
	(158,'title',0,'en_us',' friona1 '),
	(159,'filename',0,'en_us',' jared randel manager_160205_143953 jpg '),
	(159,'extension',0,'en_us',' jpg '),
	(159,'kind',0,'en_us',' image '),
	(159,'slug',0,'en_us',''),
	(159,'title',0,'en_us',' jared randel manager '),
	(160,'filename',0,'en_us',' littlefield assistant manager jpg '),
	(160,'extension',0,'en_us',' jpg '),
	(160,'kind',0,'en_us',' image '),
	(155,'slug',0,'en_us',''),
	(151,'extension',0,'en_us',' jpg '),
	(151,'kind',0,'en_us',' image '),
	(151,'slug',0,'en_us',''),
	(151,'title',0,'en_us',' friona 4 '),
	(152,'filename',0,'en_us',' friona 5 jpg '),
	(152,'extension',0,'en_us',' jpg '),
	(152,'kind',0,'en_us',' image '),
	(152,'slug',0,'en_us',''),
	(152,'title',0,'en_us',' friona 5 '),
	(153,'filename',0,'en_us',' friona assistant manager jpg '),
	(153,'extension',0,'en_us',' jpg '),
	(153,'kind',0,'en_us',' image '),
	(153,'slug',0,'en_us',''),
	(153,'title',0,'en_us',' friona assistant manager '),
	(154,'filename',0,'en_us',' friona cowboy_160205_143951 jpg '),
	(154,'extension',0,'en_us',' jpg '),
	(154,'kind',0,'en_us',' image '),
	(154,'slug',0,'en_us',''),
	(154,'title',0,'en_us',' friona cowboy '),
	(155,'filename',0,'en_us',' friona day jpg '),
	(155,'extension',0,'en_us',' jpg '),
	(155,'kind',0,'en_us',' image '),
	(151,'filename',0,'en_us',' friona 4 jpg '),
	(139,'filename',0,'en_us',' friona manager jpg '),
	(139,'extension',0,'en_us',' jpg '),
	(139,'kind',0,'en_us',' image '),
	(139,'slug',0,'en_us',' friona manager '),
	(139,'title',0,'en_us',' friona manager '),
	(140,'extension',0,'en_us',' jpg '),
	(140,'kind',0,'en_us',' image '),
	(140,'slug',0,'en_us',' jared randel manager '),
	(140,'title',0,'en_us',' jared randel manager '),
	(141,'filename',0,'en_us',' ronnie gonzales manager jpg '),
	(141,'extension',0,'en_us',' jpg '),
	(141,'kind',0,'en_us',' image '),
	(141,'slug',0,'en_us',' ronnie gonzales manager '),
	(141,'title',0,'en_us',' ronnie gonzales manager '),
	(150,'extension',0,'en_us',' jpg '),
	(150,'kind',0,'en_us',' image '),
	(150,'slug',0,'en_us',''),
	(150,'title',0,'en_us',' friona 3 '),
	(84,'slug',0,'en_us',' news '),
	(84,'title',0,'en_us',' news '),
	(84,'field',17,'en_us',' here you will find the latest news and information about friona industries achievements and involvement with our communities 2015portillo honored as avc consultant of the year2014james herring to receive national golden spur award '),
	(84,'field',8,'en_us',''),
	(135,'kind',0,'en_us',' pdf '),
	(135,'slug',0,'en_us',' bqa compliance position statement '),
	(135,'title',0,'en_us',' bqa compliance position statement '),
	(135,'extension',0,'en_us',' pdf '),
	(135,'filename',0,'en_us',' bqa compliance position statement pdf '),
	(44,'slug',0,'en_us',''),
	(44,'field',28,'en_us',' for more than 50 years friona industries has relied on knowledgeable and dedicated employees to keep our focus on two key factors our livestock and our customers we built this company in the north texas plains by understanding that consumers want quality beef products produced with the highest standards of stewardship we answer that expectation through innovative beef production and a reputation for exceeding customer expectations at friona industries quality means a fierce attention to detail from every employee on every job to back that up here is our code of conduct promise that our employees sign every year '),
	(46,'slug',0,'en_us',''),
	(46,'field',28,'en_us',' sell us your cattlefriona industries l p mailing addressp o box 15568amarillo tx 79105 5568 physical address500 s taylorsuite 601 lb 253 amarillo tx 79101 telephone806 374 1811 fax806 374 3003 cattle dept 806 220 2855 '),
	(20,'field',45,'en_us',' dave link friona manager feedyard manager james glover friona assistant manager assistant feedyard manager jayn looper friona office manager office manager '),
	(134,'field',46,'en_us',' dave link '),
	(134,'field',47,'en_us',' feedyard manager '),
	(134,'field',48,'en_us',' friona manager '),
	(134,'slug',0,'en_us',''),
	(140,'filename',0,'en_us',' jared randel manager jpg '),
	(16,'field',42,'en_us',' brad stout chief operating officer clay allen chief financial officer russell artho controller jay cortese cattle procurement manager brady bressler cattle procurement assistant manager mark hooker fed cattle manager karli schilling assistant fed cattle marketing manager blaine rotramel commodity procurement manager dell volmer feedyard operations manager eddie derrick growyard pasture cattle manager tom portillo dvm manager of animal health '),
	(123,'field',43,'en_us',' brad stout '),
	(123,'field',44,'en_us',' chief operating officer '),
	(123,'slug',0,'en_us',''),
	(124,'field',43,'en_us',' clay allen '),
	(48,'slug',0,'en_us',''),
	(48,'field',28,'en_us',' stewardship is an ethic that embodies the responsible planning and management of resources at friona industries we take stewardship seriously you can see that in our code of conduct as well as in the beef quality assurance practices we use as a matter of course in planning our health protocols for each head of cattle that come to our feedyards a large part of the beef industry s job involves making sure that beef is safe and wholesome for consumers we work together with our cattle animal health and feed suppliers to maintain high standards for the beef we produce every day '),
	(50,'slug',0,'en_us',''),
	(50,'field',28,'en_us',' our mission dedicated employees innovative beef satisfied consumer '),
	(72,'slug',0,'en_us',''),
	(72,'field',27,'en_us',' our mission '),
	(73,'slug',0,'en_us',''),
	(73,'field',33,'en_us',' dedicated employees innovative beef satisfied customer '),
	(76,'slug',0,'en_us',''),
	(76,'field',28,'en_us',' the integrity of our highly motivated and dedicated employees and officers is one of the most valuable assets we have at friona industries the foundation of our success and our ability to grow and take advantage of our resources and innovation is inspired by honesty and fairness through a culture of strong ethical behavior new employees go through a training process so they understand how important their position is to cattle performance at the feedyard as well as the financial performance of the company each year we ask our employees to agree to a code of conduct it explains our philosophical principles of employee conduct '),
	(77,'slug',0,'en_us',''),
	(77,'field',28,'en_us',' friona industries l p began as single small feedyard in amarillo tx in 1962 as the beef industry began to recognize the resources that existed in the texas panhandle founding partners like a l black were a group of cattle feeders who could see beyond the cattle to all the other businesses that cattle feeding supported meat processing feed manufacturing and trucking to name just a few we have progressed to become one of the leading cattle feeding businesses in the world led by visionaries such as mr black and james herring both winners of the national golden spur award which honors outstanding achievement in ranching and livestock businesses james herring especially bolstered our business and his work at friona industries led to two harvard school of business case studies on management and marketing most recently mr herring was honored with the feeding quality forum s 2015 industry achievement award today privately held friona industries has four state of the art feedyards in north texas with a feeding capacity that ranks us in the top 10 feedyards worldwide we continue to focus on a vertically aligned production system that creates a consistent safe tender and flavorful beef for branded product lines marketed in 2 300 retails stores in the u s as our leaders at friona industries look forward we recognize the challenges that face us as we address our stewardship responsibilities of both livestock and the land while delivering high quality beef to our customers our mission statement communicates our belief that our customers seek quality beef products produced with the highest standards of stewardship we continue our legacy of answering that expectation through innovative beef production and a reputation for exceeding customer expectations '),
	(78,'slug',0,'en_us',''),
	(78,'field',27,'en_us',' working at friona '),
	(80,'slug',0,'en_us',''),
	(80,'field',33,'en_us',' jobs at friona '),
	(81,'slug',0,'en_us',''),
	(81,'field',28,'en_us',' there are three general career areas at friona industries feedyard management office support feedyard operations and feed mill operations every job at friona is important to the overall care and performance of the cattle in our feedyards as well as to the overall financial performance of the company friona is always taking job applications for possible openings at our feedyards please click on the area below that you are interested in to see a general description and a printable job application that you can fill out and submit to friona industries each year we ask our employees to agree to a code of conduct it explains our philosophical principles of employee conduct completed applications can be mailed directly to the feedyard you are interested in working at or to friona industries 500 s taylor st suite 601 amarillo tx 79105 or it can be scanned and emailed to fi frionaind com '),
	(83,'slug',0,'en_us',''),
	(83,'field',33,'en_us',' contact info and key contacts '),
	(85,'slug',0,'en_us',''),
	(85,'field',28,'en_us',' here you will find the latest news and information about friona industries achievements and involvement with our communities 2015portillo honored as avc consultant of the year2014james herring to receive national golden spur award '),
	(96,'slug',0,'en_us',''),
	(96,'field',27,'en_us',' u s roundtable on sustainable beef '),
	(97,'slug',0,'en_us',''),
	(97,'field',28,'en_us',' friona industries is one of the founding members of the u s roundtable on sustainable beef usrsb which is a multi stakeholder initiative developed to advance support and communicate continuous improvement in sustainability of the u s beef value chain our representative to the group is blaine rotramel our commodity procurement manager at friona industries we re here to feed the world rotramel says we re using our knowledge to look at better ways to do things in our business usrsb focuses on four areas social employees involved in the community economic community impact environmental and animal health welfare friona industries is an active supporter in each of those areas for example animal health welfare are addressed through many of the bqa activities at each of the feedyards and a recent economic impact study showed the randall county feedyard provided $7 million in business activity within a 100 mile radius of the location the usrsb is working in collaboration with the global roundtable for sustainable beef grsb and other sustainability initiatives whose aims are consistent with its mission and vision '),
	(99,'slug',0,'en_us',''),
	(99,'field',28,'en_us',' the enough movement is a global community working together to ensure that each person has access to nutritious affordable food both today and in the coming decades the global effort is being led by elanco one of our business partners providing food globally is very important and we take the responsibility of providing food in our own backyard to those who are in need that s why we ve partnered the texas cattle feeders association in support of both the snack pak 4 kids and the high plains food bank '),
	(1,'username',0,'en_us',' eljefe '),
	(1,'firstname',0,'en_us',''),
	(1,'lastname',0,'en_us',''),
	(1,'fullname',0,'en_us',''),
	(1,'email',0,'en_us',' damon mccormickcompany com '),
	(1,'slug',0,'en_us',''),
	(2,'field',36,'en_us',' bqa compliance position statement beef safety starts with cattle procurement pasture cattle portrait good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices fi ethics letter january 2016 new logo pd dedicated employees make friona industries successful friona cowboy our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef http frionaindustries sandboxdsm com stewardship usrsb making a positive economic impact randall cattle with mill we use local services and buy local inputs for our four state of the art feedyards '),
	(111,'field',37,'en_us',' beef safety starts with cattle procurement '),
	(111,'field',38,'en_us',' good health practices for our feedyard cattle start before they arrive in the feedyard with approved backgrounding practices '),
	(111,'field',39,'en_us',''),
	(111,'field',40,'en_us',' bqa compliance position statement '),
	(111,'slug',0,'en_us',''),
	(111,'field',41,'en_us',' pasture cattle portrait '),
	(112,'field',41,'en_us',' friona cowboy '),
	(112,'field',37,'en_us',' dedicated employees make friona industries successful '),
	(112,'field',38,'en_us',' our employees work tirelessly to provide the care and attention to detail that creates safe and nutritious beef '),
	(112,'field',39,'en_us',''),
	(112,'field',40,'en_us',' fi ethics letter january 2016 new logo pd '),
	(112,'slug',0,'en_us',''),
	(113,'field',41,'en_us',' randall cattle with mill '),
	(113,'field',37,'en_us',' making a positive economic impact '),
	(113,'field',38,'en_us',' we use local services and buy local inputs for our four state of the art feedyards '),
	(113,'field',39,'en_us',' http frionaindustries sandboxdsm com stewardship usrsb '),
	(113,'field',40,'en_us',''),
	(113,'slug',0,'en_us',''),
	(114,'field',17,'en_us',' usrsb lorem ipsum dolor sit amet consectetur adipiscing elit sed porta lectus sed felis ullamcorper nec congue mi semper vestibulum eu vehicula massa aenean et justo sem proin ac lectus odio aliquam ut fringilla velit donec dignissim sapien nec ipsum semper maximus vestibulum in vestibulum nisl vitae sollicitudin metus nullam a malesuada dui faucibus blandit nisi quisque egestas nibh egestas enim dictum dictum nam ante nulla tincidunt eu consequat quis faucibus ac ante curabitur ornare facilisis dui sed blandit ante sodales ac vestibulum et tincidunt neque in consequat augue etiam consectetur nulla at elit aliquet vestibulum in vestibulum nisl et vestibulum lacinia '),
	(114,'field',8,'en_us',''),
	(114,'slug',0,'en_us',' usrsb '),
	(114,'title',0,'en_us',' usrsb '),
	(115,'field',27,'en_us',' usrsb '),
	(115,'slug',0,'en_us',''),
	(116,'field',28,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit sed porta lectus sed felis ullamcorper nec congue mi semper vestibulum eu vehicula massa aenean et justo sem proin ac lectus odio aliquam ut fringilla velit donec dignissim sapien nec ipsum semper maximus vestibulum in vestibulum nisl vitae sollicitudin metus nullam a malesuada dui faucibus blandit nisi quisque egestas nibh egestas enim dictum dictum nam ante nulla tincidunt eu consequat quis faucibus ac ante curabitur ornare facilisis dui sed blandit ante sodales ac vestibulum et tincidunt neque in consequat augue etiam consectetur nulla at elit aliquet vestibulum in vestibulum nisl et vestibulum lacinia '),
	(116,'slug',0,'en_us',''),
	(117,'field',33,'en_us',' feedyard management office support '),
	(117,'slug',0,'en_us',''),
	(118,'field',28,'en_us',' careers in this area are in charge of operations and employees at the feedyard as well as billing and bookkeeping records employees in these positions may deal with the public input suppliers service providers and fi customers they are responsible for accurate records and adhering to federal and state regulations as well as safety policies '),
	(118,'slug',0,'en_us',''),
	(119,'field',33,'en_us',' feedyard operations '),
	(119,'slug',0,'en_us',''),
	(120,'field',28,'en_us',' careers in this area are in direct contact with the cattle in the feedyards employees in these positions may feed and move cattle determine feeding schedules and rations assess and treat cattle health issues and work on yard maintenance '),
	(120,'slug',0,'en_us',''),
	(121,'field',33,'en_us',' feed mill operations '),
	(121,'slug',0,'en_us',''),
	(122,'field',28,'en_us',' careers in this area are working with grain storage and processing equipment employees in these positions may handle commodities operate mill equipment and provide feed quality control '),
	(122,'slug',0,'en_us',''),
	(136,'slug',0,'en_us',''),
	(137,'field',17,'en_us',' the public is looking for more transparency in the production of their food including beef that s why friona industries has made a commitment to follow beef quality assurance bqa protocols in their feedyards this is a platform that is good for us as we are committed to animal welfare says tom portillo dvm the manager of animal health at friona industries bqa reflects a positive public image and instills consumer confidence in the beef industry when producers implement the best management practices of a bqa program they assure their market steers heifers cows and bulls are the best they can be friona industries follows bqa assessments by third party auditors annual bqa training for the crews at each feedyard judicious use of antibiotics andcontinuous review of practices '),
	(137,'field',8,'en_us',''),
	(137,'slug',0,'en_us',' bqa '),
	(137,'title',0,'en_us',' bqa '),
	(138,'field',28,'en_us',' the public is looking for more transparency in the production of their food including beef that s why friona industries has made a commitment to follow beef quality assurance bqa protocols in their feedyards this is a platform that is good for us as we are committed to animal welfare says tom portillo dvm the manager of animal health at friona industries bqa reflects a positive public image and instills consumer confidence in the beef industry when producers implement the best management practices of a bqa program they assure their market steers heifers cows and bulls are the best they can be friona industries follows bqa assessments by third party auditors annual bqa training for the crews at each feedyard judicious use of antibiotics andcontinuous review of practices '),
	(138,'slug',0,'en_us',''),
	(160,'title',0,'en_us',' littlefield assistant manager '),
	(161,'filename',0,'en_us',' littlefield office manager jpg '),
	(161,'extension',0,'en_us',' jpg '),
	(161,'kind',0,'en_us',' image '),
	(161,'slug',0,'en_us',''),
	(161,'title',0,'en_us',' littlefield office manager '),
	(162,'filename',0,'en_us',' pasture cattle portrait_160205_143954 jpg '),
	(162,'extension',0,'en_us',' jpg '),
	(162,'kind',0,'en_us',' image '),
	(162,'slug',0,'en_us',''),
	(162,'title',0,'en_us',' pasture cattle portrait '),
	(163,'filename',0,'en_us',' pasture heifer jpg '),
	(163,'extension',0,'en_us',' jpg '),
	(163,'kind',0,'en_us',' image '),
	(163,'slug',0,'en_us',''),
	(163,'title',0,'en_us',' pasture heifer '),
	(164,'filename',0,'en_us',' randal ass manager jpg '),
	(164,'extension',0,'en_us',' jpg '),
	(164,'kind',0,'en_us',' image '),
	(164,'slug',0,'en_us',''),
	(164,'title',0,'en_us',' randal ass manager '),
	(165,'filename',0,'en_us',' randall cattle 1 jpg '),
	(165,'extension',0,'en_us',' jpg '),
	(165,'kind',0,'en_us',' image '),
	(165,'slug',0,'en_us',''),
	(165,'title',0,'en_us',' randall cattle 1 '),
	(166,'filename',0,'en_us',' randall cattle 2 jpg '),
	(166,'extension',0,'en_us',' jpg '),
	(166,'kind',0,'en_us',' image '),
	(166,'slug',0,'en_us',''),
	(166,'title',0,'en_us',' randall cattle 2 '),
	(167,'filename',0,'en_us',' randall cattle with mill_160205_143955 jpg '),
	(167,'extension',0,'en_us',' jpg '),
	(167,'kind',0,'en_us',' image '),
	(167,'slug',0,'en_us',''),
	(167,'title',0,'en_us',' randall cattle with mill '),
	(168,'filename',0,'en_us',' randall cowboy jpg '),
	(168,'extension',0,'en_us',' jpg '),
	(168,'kind',0,'en_us',' image '),
	(168,'slug',0,'en_us',''),
	(168,'title',0,'en_us',' randall cowboy '),
	(169,'filename',0,'en_us',' randall office manager jpg '),
	(169,'extension',0,'en_us',' jpg '),
	(169,'kind',0,'en_us',' image '),
	(169,'slug',0,'en_us',''),
	(169,'title',0,'en_us',' randall office manager '),
	(170,'filename',0,'en_us',' randall yard cattle jpg '),
	(170,'extension',0,'en_us',' jpg '),
	(170,'kind',0,'en_us',' image '),
	(170,'slug',0,'en_us',''),
	(170,'title',0,'en_us',' randall yard cattle '),
	(171,'filename',0,'en_us',' ronnie gonzales manager_160205_143956 jpg '),
	(171,'extension',0,'en_us',' jpg '),
	(171,'kind',0,'en_us',' image '),
	(171,'slug',0,'en_us',''),
	(171,'title',0,'en_us',' ronnie gonzales manager '),
	(172,'filename',0,'en_us',' swisher ass manager jpg '),
	(172,'extension',0,'en_us',' jpg '),
	(172,'kind',0,'en_us',' image '),
	(172,'slug',0,'en_us',''),
	(172,'title',0,'en_us',' swisher ass manager '),
	(173,'filename',0,'en_us',' swisher manager jpg '),
	(173,'extension',0,'en_us',' jpg '),
	(173,'kind',0,'en_us',' image '),
	(173,'slug',0,'en_us',''),
	(173,'title',0,'en_us',' swisher manager '),
	(174,'filename',0,'en_us',' swisher office manager jpg '),
	(174,'extension',0,'en_us',' jpg '),
	(174,'kind',0,'en_us',' image '),
	(174,'slug',0,'en_us',''),
	(174,'title',0,'en_us',' swisher office manager '),
	(175,'filename',0,'en_us',' trucker jpg '),
	(175,'extension',0,'en_us',' jpg '),
	(175,'kind',0,'en_us',' image '),
	(175,'slug',0,'en_us',''),
	(175,'title',0,'en_us',' trucker '),
	(176,'field',46,'en_us',' james glover '),
	(176,'field',47,'en_us',' assistant feedyard manager '),
	(176,'field',48,'en_us',' friona assistant manager '),
	(176,'slug',0,'en_us',''),
	(177,'field',46,'en_us',' jayn looper '),
	(177,'field',47,'en_us',' office manager '),
	(177,'field',48,'en_us',' friona office manager '),
	(177,'slug',0,'en_us',''),
	(21,'field',45,'en_us',' ronnie gonzales ronnie gonzales manager feedyard manager jared lee littlefield assistant manager assistant feedyard manager tanya griswold littlefield office manager office manager '),
	(178,'field',46,'en_us',' ronnie gonzales '),
	(178,'field',47,'en_us',' feedyard manager '),
	(178,'field',48,'en_us',' ronnie gonzales manager '),
	(178,'slug',0,'en_us',''),
	(179,'field',46,'en_us',' jared lee '),
	(179,'field',47,'en_us',' assistant feedyard manager '),
	(179,'field',48,'en_us',' littlefield assistant manager '),
	(179,'slug',0,'en_us',''),
	(180,'field',46,'en_us',' tanya griswold '),
	(180,'field',47,'en_us',' office manager '),
	(180,'field',48,'en_us',' littlefield office manager '),
	(180,'slug',0,'en_us',''),
	(22,'field',45,'en_us',' jerrid vincent jared randel manager feedyard manager barry chew randal ass manager assistant feedyard manager sue doxon randall office manager office manager '),
	(181,'field',46,'en_us',' jerrid vincent '),
	(181,'field',47,'en_us',' feedyard manager '),
	(181,'field',48,'en_us',' jared randel manager '),
	(181,'slug',0,'en_us',''),
	(182,'field',46,'en_us',' barry chew '),
	(182,'field',47,'en_us',' assistant feedyard manager '),
	(182,'field',48,'en_us',' randal ass manager '),
	(182,'slug',0,'en_us',''),
	(183,'field',46,'en_us',' sue doxon '),
	(183,'field',47,'en_us',' office manager '),
	(183,'field',48,'en_us',' randall office manager '),
	(183,'slug',0,'en_us',''),
	(23,'field',45,'en_us',' trevor peterson swisher manager feedyard manager clif yeary swisher ass manager assistant feedyard manager mary young swisher office manager office manager '),
	(184,'field',46,'en_us',' trevor peterson '),
	(184,'field',47,'en_us',' feedyard manager '),
	(184,'field',48,'en_us',' swisher manager '),
	(184,'slug',0,'en_us',''),
	(185,'field',46,'en_us',' clif yeary '),
	(185,'field',47,'en_us',' assistant feedyard manager '),
	(185,'field',48,'en_us',' swisher ass manager '),
	(185,'slug',0,'en_us',''),
	(186,'field',46,'en_us',' mary young '),
	(186,'field',47,'en_us',' office manager '),
	(186,'field',48,'en_us',' swisher office manager '),
	(186,'slug',0,'en_us','');

/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_name_unq_idx` (`name`),
  UNIQUE KEY `sections_handle_unq_idx` (`handle`),
  KEY `sections_structureId_fk` (`structureId`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'index',1,'2015-11-18 21:02:51','2015-11-18 21:02:51','ba972c0c-9d08-4822-b62c-b64f266dfbb4'),
	(2,8,'News','news','structure',1,'news/_entry',1,'2015-11-18 21:02:52','2016-01-27 20:41:22','04b3529e-8200-4de6-b5fd-46a1c15d7c2d'),
	(3,NULL,'About Us','aboutUs','single',1,'about',1,'2015-11-18 22:38:05','2015-11-19 21:05:40','4e35ec95-0ecc-4d42-895c-a670c777e0d4'),
	(4,1,'About','about','structure',1,'about/_entry',1,'2015-11-18 22:38:20','2015-11-19 21:05:51','1dbdb869-9ec4-447d-b0ec-a875e3a1259b'),
	(5,NULL,'Locations','locations','single',1,'locations',1,'2015-11-18 22:40:12','2015-11-18 22:40:12','61005bb5-af3c-4c38-af2a-73d99f8b7ad4'),
	(7,NULL,'Careers','careers','single',1,'careers',1,'2015-11-18 22:41:55','2015-11-18 22:41:55','43e3f57c-3721-42eb-801e-2fec6535d19f'),
	(8,3,'Career','career','structure',1,'careers/_entry',1,'2015-11-18 22:42:26','2015-11-19 18:10:31','2bc61775-e21a-40ec-bd5b-a395393c17d7'),
	(9,NULL,'Stewardship Home','stewardshipHome','single',1,'stewardship',1,'2015-11-18 22:43:52','2016-01-27 16:29:03','eb1d7064-5de4-43d8-b5e3-da4bb55ee1de'),
	(10,4,'Stewardship','stewardship','structure',1,'stewardship/_entry',1,'2015-11-18 22:44:09','2016-01-27 16:28:32','f54b0602-e20a-4eb3-92b3-8ae64ce6dec9'),
	(11,NULL,'Contact Us','contactUs','single',1,'contact',1,'2015-11-18 22:44:37','2015-11-19 21:07:29','96a73d2c-2879-4871-be77-c36d86c492cf'),
	(12,7,'Contact','contact','structure',1,'contact/_entry',1,'2015-11-19 21:07:47','2015-11-19 21:07:47','e55fd69f-483f-4fb9-9f3b-ae1a194d047a');

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections_i18n`;

CREATE TABLE `sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sections_i18n` WRITE;
/*!40000 ALTER TABLE `sections_i18n` DISABLE KEYS */;

INSERT INTO `sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',1,'__home__',NULL,'2015-11-18 21:02:51','2015-11-18 21:02:51','11599ad5-ad30-4c9e-8866-3326415a1919'),
	(2,2,'en_us',0,'news/{postDate.year}/{slug}','{parent.uri}/{slug}','2015-11-18 21:02:52','2016-01-27 20:41:22','4e06cc16-c910-4dbc-ac61-7a037ee20b4a'),
	(3,3,'en_us',0,'about',NULL,'2015-11-18 22:38:05','2015-11-19 21:05:40','902d9031-df31-4204-ac9b-955a8eb403b5'),
	(4,4,'en_us',0,'about/{slug}','{parent.uri}/{slug}','2015-11-18 22:38:20','2015-11-19 21:05:51','273f788b-1ae5-494c-b5f3-7da89b871963'),
	(5,5,'en_us',0,'locations',NULL,'2015-11-18 22:40:12','2015-11-18 22:40:12','f273b19e-06bf-4f97-80cd-843798bd26e4'),
	(7,7,'en_us',0,'careers',NULL,'2015-11-18 22:41:55','2015-11-18 22:41:55','93e2c4d8-0211-40c3-958e-16e2275184ca'),
	(8,8,'en_us',0,'careers/{slug}','{parent.uri}/{slug}','2015-11-18 22:42:26','2015-11-19 18:10:31','022809ed-77a0-4c8c-b883-f02279121f9a'),
	(9,9,'en_us',0,'stewardship',NULL,'2015-11-18 22:43:52','2016-01-27 16:15:21','bd5c4c3c-c047-40b1-a405-83b364eb1d91'),
	(10,10,'en_us',0,'stewardship/{slug}','{parent.uri}/{slug}','2015-11-18 22:44:09','2016-01-27 16:14:54','3a83dcc6-1648-4bc8-b16b-32ccd8a3b634'),
	(11,11,'en_us',0,'contact',NULL,'2015-11-18 22:44:37','2015-11-19 21:07:29','f60b94c3-6a06-4b7a-94ba-daa38519d286'),
	(12,12,'en_us',0,'contact/{slug}','{parent.uri}/{slug}','2015-11-19 21:07:47','2015-11-19 21:07:47','d057b2be-6207-4586-81cd-50cc2604c3b8');

/*!40000 ALTER TABLE `sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_fk` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'fefe2fbad10b81ac9bd255d8e05a2e3ad6bdc12dczozMjoiMkpYSTlQMEdmY3FLbzFBWE1sSEUwVFpnOXpvY1JhS2MiOw==','2015-11-18 21:02:51','2015-11-18 21:02:51','b51ccb4e-e83c-4a32-9624-1d48f51c248c'),
	(2,1,'123e5ed198e9bcb4f5bd55b8c961997542dbdae6czozMjoiNjhKRGkxSGRoWnpzU2VHZmRMUWI3QXhwaGUwb09qemsiOw==','2015-11-18 22:30:27','2015-11-18 22:30:27','89a80459-af8f-4bc2-af51-2d9b819f6736'),
	(3,1,'2054e8c7fc022fe3adf137bf1d66cce593c014e7czozMjoiNGZQNH5xUHdKeU9BUEl+anZvdXNjTENlOH5sczg0U28iOw==','2015-11-19 18:02:59','2015-11-19 18:02:59','28bf980a-d500-4bc4-a2a3-0bd3dff7c9fa'),
	(4,1,'c0c507430838857a827156ced1ba431253dface2czozMjoiZGk5WDEzazNDek5GbFVWZlVTbmRhczcyQU83Z21oMUciOw==','2015-11-19 20:30:06','2015-11-19 20:30:06','36b5f64b-54d0-4e16-a09c-b2d23093ea0d'),
	(5,1,'41cc22d280705fb3457f0a38090c4434846ba1ecczozMjoib3VGS0lJSFI5VmtYVm1uTFNubXUyVHNnaFZhTV9YckIiOw==','2015-11-20 15:01:11','2015-11-20 15:01:11','8499e820-2ac2-46b3-900e-980a69adbfba'),
	(6,1,'cec104708b047d151b657494267658b1e135840bczozMjoia1R3NE5FY1MwVFJzRVhWVTlSczdlUE5oaXdnNTZobEciOw==','2015-11-20 17:07:02','2015-11-20 17:07:02','096cd3b9-5d7b-4792-8e24-7d1a975894a7'),
	(7,1,'4ec30e7b8ac0cb11abe2c0c23c7e5669b4600aafczozMjoifjZDQlZwNHBwflY3NjJGaGdjWTVOWjBNaWJnRDl3ODkiOw==','2015-11-23 14:44:14','2015-11-23 14:44:14','b956a8de-7633-4df1-be99-04234b49621c'),
	(8,1,'4aebc2b0fcea03f1d34eee9d735b7b617d1c255bczozMjoiOX5vQVhRalprNEZ3T3Q0a3J3YnBCNk5Kb0hrXzc4dDciOw==','2015-11-23 16:22:00','2015-11-23 16:22:00','8de844db-ad28-4efa-8129-cd261da7aa53'),
	(9,1,'cf5b64c08d4b390bf082e89e8432cc9370243ee7czozMjoiMHhaTXF6N1ViaUFhfmN0N0NFYUlNSW9jQUFYQ05KWWMiOw==','2015-11-24 15:00:30','2015-11-24 15:00:30','a2797890-5b7e-4a16-bc27-8437d61f7b59'),
	(10,1,'216c2bc49d53040aa9957b46f789d7aa357ff9feczozMjoiRm1MTWZzTEcxM19wcFY4RFIyZXhWZ0xmY1pSdE5uMXEiOw==','2015-11-30 14:46:02','2015-11-30 14:46:02','f23f10aa-f7ff-439b-ab0a-7fd10e54bb80'),
	(11,1,'56acd269018992a5468f0665e58e0823f2444731czozMjoiNFZMQVBhRl9OSThIeFdNZHpFVkNnRDlFcHoyT3dtWmQiOw==','2015-11-30 18:23:53','2015-11-30 18:23:53','f08fa3c0-f4bc-4301-a7a4-b8b3d89c3690'),
	(12,1,'018ecb3c6c51940bf61541e3f09615ce7c564178czozMjoiOTNNUE92bWFvSTdxOWFhSXByX0d1TUNtSUN1NjhFak8iOw==','2015-11-30 19:56:33','2015-11-30 19:56:33','35fa3cff-fff2-40b3-bf36-5c65d2fa058b'),
	(13,1,'0b5eaef571c4b88df72db08ce0fe3b4edcae3955czozMjoiSjdLTXk5OHFHUWtJR19NSVpnZE1Qa3poMHE0eElIUjQiOw==','2015-12-01 15:25:45','2015-12-01 15:25:45','855c4145-506d-4e4c-8a9a-42e743e9b68d'),
	(14,1,'9ddb62056cf4af4221dbddf937fcb26a5b6ba3caczozMjoiclg1M1hNc1RqV1RCbkp3S1lNUW9aeUljb3dnTDZJaDkiOw==','2015-12-04 17:00:19','2015-12-04 17:00:19','6593a7a9-8a1b-4165-b821-575fce50a15d'),
	(15,1,'54d83c55157a39fc1ec159c26958180d6a819b9eczozMjoidmFIdXZHMG1Lb3UxVFRzajNQYXVXb29IaXh4cFFFZG0iOw==','2015-12-07 14:49:14','2015-12-07 14:49:14','06acb24b-fcf7-4097-929a-7e8a884090e5'),
	(16,1,'be712c31da6ecf97b5b435e60da26917a6b34f66czozMjoiQ2lqRGRrckIxRDl1X19oQkhqT3dSWkNybXhVaTRHTzUiOw==','2015-12-08 04:10:18','2015-12-08 04:10:18','86f1e676-1b97-4439-a406-109b9e7a8fa3'),
	(18,1,'32ad6a37873cf6019043a4254d72b0f944bc9d1bczozMjoiZUlLWllUQlpMUnR6NklhNVpEdU9LTkJnOGtjT3BCMDEiOw==','2015-12-09 15:12:02','2015-12-09 15:12:02','ae528d18-d2c4-4042-a088-348d9eb54b6e'),
	(19,1,'3e7d8cef726a32e98fff485ac64c33522c47520cczozMjoifnRqOWdzNVFIVUtidl9mamlSeVF5M3pUen5DTXoxX1UiOw==','2015-12-10 04:26:51','2015-12-10 04:26:51','bdbca39e-bcdc-483c-8eec-42218a91f157'),
	(20,1,'af5476c3b60e92b305cb3e18e95b640bc709729aczozMjoiaF9mbmNWSmM5en45NVliN3lEMGczSFRMcmZ2bnFkZU8iOw==','2015-12-21 22:07:25','2015-12-21 22:07:25','7b80d34e-7bd7-41f5-87ba-838dc1c9b741'),
	(21,1,'e484b0132141bd863cb5e37580d54dbca31d51ccczozMjoiXzM3ejdyMFhyRzMwMjV+eEpOWW5BUjd4Tml4WDVIYW4iOw==','2015-12-28 13:57:46','2015-12-28 13:57:46','37826a88-5484-45a9-8b02-30848ced2c04'),
	(22,1,'47fd864d0e4a0a3f319b9a1e258e4cc9dbcc90cfczozMjoibDVXa2lpTWI5YmhDfjNQaXlZS19OX3NiSGhJeXN3UDYiOw==','2016-01-05 14:50:14','2016-01-05 14:50:14','a50fadf1-4753-4cca-bebd-e287689eeb89'),
	(23,1,'bd0e33bdd413795b8fc325e405d61cb585ff69fcczozMjoiZW1pMmhffkthMzdyRzgyVVllZFJ4MVpWSlhvUTJsfjUiOw==','2016-01-07 16:16:49','2016-01-07 16:16:49','769264b8-514c-440f-b614-52010966936e'),
	(24,1,'100f88f82c89120284e0f0c82925fae09ed81fcbczozMjoiMVVtdnNEd1pEb2Z5RGRJWnVYZUZFNnNLY2RNTjBlYzEiOw==','2016-01-15 14:50:39','2016-01-15 14:50:39','aea8cd4d-b9e2-425c-8a59-86c9e8467611'),
	(25,1,'5dc776eb88888f0b95dc9e76315bda1758a0c87aczozMjoiTVBST29xRVVRTF82ZFhZVURWRDdwZjg4X25QQ3dRaVEiOw==','2016-01-22 14:53:15','2016-01-22 14:53:15','91a1ea19-0297-454c-99af-0714a15ab268'),
	(26,1,'68518978a5c5690e7d5abe580278177e12f0b28aczozMjoiVlJ4WUFRRWVuS2s0VH5ON19YOWZMTWMyTjA2ckc5bTIiOw==','2016-01-25 16:19:50','2016-01-25 16:19:50','aaeb9d3f-70a9-41fa-a089-86f964520d83'),
	(27,1,'420649c2aa37ef6c1cc641e69bfbe507dc2b951eczozMjoiMExVQnBPNzl4UW9IZzBvX25pRmYyR2g5Uk9HdVNRQVciOw==','2016-01-25 17:37:41','2016-01-25 17:37:41','73cbe9a9-8e81-4781-a7ae-b3971890539f'),
	(28,1,'98ace00cfc1128b12452051f2754082ee65b66e2czozMjoielE2Rk1OTW1WdH44aEpLS35HdVVkOHdPeDJmWnpDcGUiOw==','2016-01-27 15:35:39','2016-01-27 15:35:39','ca87965c-023c-4c52-897b-dfb03262ff93'),
	(29,1,'b2e64beab7427e9b24bd3103228c7899a6d3c1ddczozMjoiSU01RnVfWFZ3MjE1amd4bjJaWW5UUWFCS0VNYkNGelIiOw==','2016-01-27 15:51:46','2016-01-27 15:51:46','208c9ab6-e3ad-4f9f-9021-0182496e8050'),
	(30,1,'c202bea8d5e7327d0b1553bb629eb54ccb81e652czozMjoicEcxZVc0dzNRc3I5SzV1M3BiSG5ZYTdBZW5TcmMxTkoiOw==','2016-01-27 17:53:20','2016-01-27 17:53:20','91f2cdb7-9253-4bb3-8b14-e5a0db513f5f'),
	(31,1,'31b47a24e773aa90d091e2202f6122e15233f6f5czozMjoiTDBDYldNVWh+dEV3cmF5UmZHQUowY3dQb1g2RXZuRUsiOw==','2016-01-27 20:23:20','2016-01-27 20:23:20','7be9b303-9797-49cb-82ff-5184a683a8c4'),
	(33,1,'eb0f93a82fe4bde4c4fa87a902aa3f53f3a64a07czozMjoiYmJWZzZOYUhDVlJrcmpSNWdRTFZFZjR3ZmwzM3E5UUgiOw==','2016-01-27 20:41:54','2016-01-27 20:41:54','8c37d50c-d608-4338-bff9-bd4337ff3395'),
	(34,1,'3d0052f896793e4c75b276a52cbd851f2ee41800czozMjoicFg5MmFzSGY1YzhRYnVIcX5xSnNmeGNuX0xMSGF2R3QiOw==','2016-02-01 16:17:23','2016-02-01 16:17:23','db408272-74b7-420c-92a9-2a6e4a10caef'),
	(35,1,'84e17f90aef2e7f9c9153444dde337e31a9c27cdczozMjoibW9xYWVBcHdVWVczQXJONl95N1ZyUXczM3djSmdaaG4iOw==','2016-02-01 19:38:13','2016-02-01 19:38:13','02c82f88-cf9b-4312-bfbd-f1c06ae5cd29'),
	(38,1,'f33b06ca9e9520bf22160e8284f6e6aa02205410czozMjoiZGlfTzU1WnZjclZseVdSUTRUdkNVQ3g4ZzhJdDROMWMiOw==','2016-02-01 21:45:00','2016-02-01 21:45:00','fd591086-87a7-48f9-99be-2320777b7430'),
	(39,1,'3ae76f43137396eadccf8e243b8e5a96ae3e2fa2czozMjoiSl9EdnRJUHdhSHYxNUxIdFI5MmVTaWVDYmZOVXNaQWoiOw==','2016-02-03 16:00:48','2016-02-03 16:00:48','08214cae-3ba9-4740-bd72-b3889f0c8e16'),
	(40,1,'715dff00bee536b08fece887a77e4248ab1c4122czozMjoiTktpY1hTazN3aXZpcElBaXg5Z2RnMHdSMjA0NUtSQUwiOw==','2016-02-03 18:44:05','2016-02-03 18:44:05','bb815582-a526-4df5-8f65-6ac1478f793e'),
	(41,1,'2bf1d31afbe392da6111b563e639b1dcd292530bczozMjoiYjNpcjNnSmpJMTZ+NDVRQTFaV0xwaDlaOGwyMWJRMGIiOw==','2016-02-03 22:49:23','2016-02-03 22:49:23','fb708941-b45e-479b-9751-c1bea41e52be'),
	(42,1,'0188403c175c8daa590a33076c30274585ad32e3czozMjoiMGhybkN0dUNEN1VJd1FNaUYyV0RHQTlqZlI2c29YUGIiOw==','2016-02-04 16:38:30','2016-02-04 16:38:30','73329e08-707f-4d9a-9e60-b5eaad90bb23'),
	(43,1,'ee4b4d831d89ab26aa88bd26fe20de099ead775dczozMjoiU25xWmcwZ3E5djd5dGVidFpEMVczY0FQMXExd0FhdUkiOw==','2016-02-05 15:20:04','2016-02-05 15:20:04','7408a861-2ebe-48ff-b5de-062745922ba5'),
	(44,1,'4130f07ff6571e361fc9639b8ff09f7be9ba8916czozMjoic2pYd1JFbDJTMGZWRDNZaVZQdDFVRGtENGxLODNKdTUiOw==','2016-02-05 17:33:48','2016-02-05 17:33:48','9ac57ef9-3d2e-409f-a1ae-88a11e6e3949'),
	(45,1,'9487547cab124ea9223bd7ae91f1300b09c86072czozMjoid0kzdlJxaXp3NWpXUW1QWjV2UX44V0JFallFYk9mY2giOw==','2016-02-05 18:50:14','2016-02-05 18:50:14','18f0f7b1-8c22-4bdf-8ee9-941c12957816'),
	(46,1,'a6bb2708d5fe0c76513027f0c1fba04ba6727c97czozMjoiazNYT0c5Q19QYUJPRDZVSVVETjI2ZHVUaGc2VUh5c2giOw==','2016-02-05 20:20:42','2016-02-05 20:20:42','a5fa9912-de79-49eb-8de4-84a9e7902180');

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shunnedmessages`;

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table smartmap_addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smartmap_addresses`;

CREATE TABLE `smartmap_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `street1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(12,8) DEFAULT NULL,
  `lng` decimal(12,8) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `smartmap_addresses_elementId_fk` (`elementId`),
  KEY `smartmap_addresses_fieldId_fk` (`fieldId`),
  CONSTRAINT `smartmap_addresses_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `smartmap_addresses_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `smartmap_addresses` WRITE;
/*!40000 ALTER TABLE `smartmap_addresses` DISABLE KEYS */;

INSERT INTO `smartmap_addresses` (`id`, `elementId`, `fieldId`, `street1`, `street2`, `city`, `state`, `zip`, `country`, `lat`, `lng`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,20,14,'2370 FM 3140',NULL,'Friona','TX','79035','United States',34.63750330,-102.63953410,'2015-12-01 15:33:35','2016-02-05 22:38:20','7f4f82f8-3a4c-42e7-bb9f-047613392458'),
	(2,21,14,'1640 FM 37',NULL,'Amherst','TX','79312','United States',34.01098600,-102.26028080,'2015-12-01 15:33:59','2016-02-05 22:37:36','9147818f-ef26-4b0c-ba84-4e6df954c616'),
	(3,22,14,'15000 FM 2219',NULL,'Amarillo','TX','79119','United States',35.05951680,-102.01210470,'2015-12-01 15:34:33','2016-02-05 22:40:23','88e554c7-f627-4907-a742-7df85217c370'),
	(4,23,14,'6556 FM 214',NULL,'Happy','TX','79042','United States',34.63136210,-101.88916700,'2015-12-01 15:34:59','2016-02-05 22:42:30','caa8f5bf-a1b6-42f7-9ca9-8ea3e26d6537'),
	(5,24,14,'500 S. Taylor St','Suite 601','Amarillo','TX','79101','United States',35.20810300,-101.83630000,'2015-12-01 15:35:48','2016-02-04 15:08:21','9f095cf3-8515-4b05-9b60-7ab38f42cb26');

/*!40000 ALTER TABLE `smartmap_addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structureelements`;

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;

INSERT INTO `structureelements` (`id`, `structureId`, `elementId`, `root`, `lft`, `rgt`, `level`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,NULL,1,1,8,0,'2015-11-18 22:39:29','2015-11-18 22:39:29','0a629498-ac0d-419b-ae69-0492ede65155'),
	(2,1,5,1,2,3,1,'2015-11-18 22:39:29','2015-11-18 22:39:29','d168a8dc-fd27-43f5-a59e-9444212c1b65'),
	(4,1,7,1,4,5,1,'2015-11-18 22:39:48','2015-11-18 22:39:48','f7222cec-7e83-4db4-b087-5bfd01aec61d'),
	(11,4,NULL,11,1,12,0,'2015-11-18 22:44:50','2015-11-18 22:44:50','1b30d93a-cefc-4699-a5c3-bd215df1f2ec'),
	(12,4,17,11,6,7,1,'2015-11-18 22:44:50','2015-11-18 22:44:50','3c4f554d-3498-40a7-a567-3b5dbc1af913'),
	(13,4,18,11,2,3,1,'2015-11-18 22:44:55','2015-11-18 22:44:55','8183b377-ca59-4500-b889-40805daad70f'),
	(14,4,19,11,4,5,1,'2015-11-18 22:45:02','2015-11-18 22:45:02','b98c16ba-f2f4-4260-9b7e-383ef93b2bd9'),
	(15,5,NULL,15,1,12,0,'2015-11-19 18:05:27','2015-11-19 18:05:27','75d7c616-1f03-4dc0-a802-771277afc74d'),
	(16,5,20,15,4,5,1,'2015-11-19 18:05:27','2015-11-19 18:05:27','66eb8dc4-0f0b-4b53-a207-c0397acfc6b6'),
	(17,5,21,15,6,7,1,'2015-11-19 18:05:44','2015-11-19 18:05:44','e831f9dd-a4b7-4eb2-94b0-a0144a5d8d50'),
	(18,5,22,15,8,9,1,'2015-11-19 18:05:53','2015-11-19 18:05:53','589d11c9-7fc8-4d4f-b10b-6238145c8487'),
	(19,5,23,15,10,11,1,'2015-11-19 18:06:05','2015-11-19 18:06:05','2996eae1-aa50-416d-b77e-f14118221e58'),
	(20,5,24,15,2,3,1,'2015-11-19 18:06:56','2015-11-19 18:06:56','5ed94356-1701-4c8b-bc83-3172951d9ccd'),
	(21,3,NULL,21,1,2,0,'2015-11-19 18:11:23','2015-11-19 18:11:23','2b7e62c7-dc74-4bdd-b651-b689aac9ec7c'),
	(25,7,NULL,25,1,2,0,'2015-11-19 21:08:17','2015-11-19 21:08:17','4af0a251-dea4-466c-a85c-9cd32e2d4b74'),
	(29,1,84,1,6,7,1,'2016-01-27 17:37:15','2016-01-27 17:37:15','e821ce07-5f46-4c16-8ea7-6ac7f06306d4'),
	(30,8,NULL,30,1,2,0,'2016-01-27 20:41:22','2016-01-27 20:41:22','bf668837-59e3-4984-8063-46d838cd6511'),
	(35,4,114,11,8,9,1,'2016-02-03 22:46:29','2016-02-03 22:46:29','fb213810-51e6-4c6a-b404-a1ca9dd654ba'),
	(36,4,137,11,10,11,1,'2016-02-05 15:41:29','2016-02-05 15:41:29','61d68cbb-80da-458b-b72f-b316cb72aff2');

/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structures`;

CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;

INSERT INTO `structures` (`id`, `maxLevels`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'2015-11-18 22:38:20','2015-11-19 21:05:51','14366f5a-7b56-46a9-9bcc-f88f983d8a17'),
	(3,NULL,'2015-11-18 22:42:26','2015-11-19 18:10:31','e4dbb82e-1e5d-4b6f-a4b6-7eb14f442955'),
	(4,NULL,'2015-11-18 22:44:09','2016-01-27 16:28:32','46a0fbd7-1dd1-4f9c-94ac-dfcc0b8d2670'),
	(5,NULL,'2015-11-19 18:04:55','2016-02-04 17:30:16','d4de2c73-5a5a-4f67-9402-53eda64a551c'),
	(7,NULL,'2015-11-19 21:07:47','2015-11-19 21:07:47','a7bd7a99-4f9a-4d5e-8a45-9ec943bc3773'),
	(8,NULL,'2016-01-27 20:41:22','2016-01-27 20:41:22','70a5d962-3ca3-49d6-974a-e8bb10fb22a5');

/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table systemsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `systemsettings`;

CREATE TABLE `systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `systemsettings` WRITE;
/*!40000 ALTER TABLE `systemsettings` DISABLE KEYS */;

INSERT INTO `systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'email','{\"template\":\"\",\"protocol\":\"smtp\",\"emailAddress\":\"damon@mccormickcompany.com\",\"senderName\":\"Friona Industries\",\"smtpAuth\":1,\"username\":\"mcinteractive\",\"password\":\"McAcce$$1\",\"smtpSecureTransportType\":\"none\",\"port\":\"587\",\"host\":\"smtp.sendgrid.net\",\"timeout\":\"30\"}','2015-11-18 21:02:51','2016-01-27 18:55:17','4f44244d-7dc1-4a37-99ea-bf36e3d33770');

/*!40000 ALTER TABLE `systemsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taggroups`;

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `taggroups_handle_unq_idx` (`handle`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;

INSERT INTO `taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default',1,'2015-11-18 21:02:51','2015-11-18 21:02:51','77c59385-d84b-4a5b-bb52-7818b0e7db94');

/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_fk` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tasks_root_idx` (`root`),
  KEY `tasks_lft_idx` (`lft`),
  KEY `tasks_rgt_idx` (`rgt`),
  KEY `tasks_level_idx` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecachecriteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecachecriteria`;

CREATE TABLE `templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecacheelements`;

CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecaches`;

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `templatecaches_locale_fk` (`locale`),
  CONSTRAINT `templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usergroups`;

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usergroups_users`;

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions`;

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions_usergroups`;

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions_users`;

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unq_idx` (`username`),
  UNIQUE KEY `users_email_unq_idx` (`email`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'eljefe',NULL,NULL,NULL,'damon@mccormickcompany.com','$2y$13$5xpP8Mok53IxOhsw63j8JuV2tN045xfqac6uVylMsGWHnv8yDy3Qm',NULL,0,1,0,0,0,0,0,'2016-02-05 20:20:42','65.158.43.178',NULL,NULL,'2015-12-03 15:26:12',NULL,NULL,NULL,NULL,0,'2015-11-18 21:02:49','2015-11-18 21:02:49','2016-02-05 20:20:42','2bf67b98-cdd0-4539-a226-93d5092b869f');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_fk` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;

INSERT INTO `widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'RecentEntries',1,NULL,NULL,1,'2015-11-18 21:02:55','2015-11-18 21:02:55','d37ae817-afc9-4998-842a-b7e8b9330127'),
	(2,1,'GetHelp',2,NULL,NULL,1,'2015-11-18 21:02:55','2015-11-18 21:02:55','cc015800-ec9b-4f82-85a9-b68f56e606c4'),
	(3,1,'Updates',3,NULL,NULL,1,'2015-11-18 21:02:55','2015-11-18 21:02:55','e75fa189-ff4b-437d-9bdb-bcbd8649475f'),
	(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',1,'2015-11-18 21:02:55','2015-12-04 17:00:43','da700b0d-ff89-4b85-a3ca-8934c274efde');

/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
