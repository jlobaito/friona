({
    'appDir': '../',
    'baseUrl': './',
    'dir': '../../../../web/resources/js',
    'mainConfigFile': '../app.config.js',
    'modules': [
        {
            'name': 'application'
        }
    ],
    'fileExclusionRegExp': /^(?:_build|(?:r|app.build)\.js)$/,
    'optimize': 'none',
    'removeCombined': true,
    'paths': {
        'jquery': 'vendor/jquery.min',
        'velocity': 'plugins/velocity.min',
        'jquery.validate': 'plugins/jquery.validate',
        'mustache': 'plugins/mustache'
    },
    'shim': {
        'velocity': {
            'deps': ['jquery']
        },
        'velocity.ui': {
            'deps': ['velocity']
        },
        'jquery.validate': {
            'deps': ['jquery']
        }
    }
})