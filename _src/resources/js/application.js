'use strict';

/**
 * Primary application module.
 *
 * @param {function} $ | jQuery library.
 * @param {object} ga | Google Analytics module.
 * @param {object} Mustache | Mustache plugin.
 * @param {object} careers | Careers module.
 * @param {object} weather | Weather module.
 *
 * @returns {object}
 *
 */

define([
    'jquery',
    'modules/analytics/google',
    'mustache',
    'modules/locations/careers',
    'modules/weather/yahoo',
    'jquery.validate',
    'velocity',
    'plugins/domReady!',
    'flexslider'
], function ($, ga, Mustache, careers, weather) {
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var initialize = function () {
        ga.initialize();
        careers.initialize();
        $('#carousel').flexslider({
            'animation': 'slide',
            'controlNav': false,
            'directionNav': false,
            'animationLoop': false,
            'itemWidth': 0,
            'itemMargin': 0,
            'asNavFor': '#slider',
            'move': 0,
            'slideshow': false
        });


        $('#slider').flexslider({
            'animation': 'fade',
            'controlNav': false,
            'animationLoop': false,
            'directionNav': false,
            'sync': '#carousel'

        });

        if ($('#weather').length) {
            /**
             * Weather initialization.
             *
             * @param {object} weather | Weather application module.
             * @returns {object}
             *
             */
            weather.getWeather($('#city').val(), $('#state').val(), function (weatherData) {
                // fancy stuff...
                if ($.isEmptyObject(weatherData)) {
                    console.info('--- ERROR: no weather data ---');
                } else {
                    var conditions = {
                        'city': weatherData.query.results.channel.location.city,
                        'region': weatherData.query.results.channel.location.region,
                        'temp': weatherData.query.results.channel.item.condition.temp
                    };

                    // fancy stuff...

                    requestTemplate('weather', function (template) {
                        console.log('requestTemplate called');
                        var html = $(template).filter('#weatherTpl').html();

                        console.info('requestTemplate result:');
                        console.info(html);

                        $('#weather').html(Mustache.render(html, conditions));
                    });
                }
            });
        }
    };

    /**
     * Makes the request to get a mustache tempalte.
     *
     * @param {string} template | The template name (filename).
     * @param {object} callback | Callback method (market data).
     * @returns {object}
     *
     */
    var requestTemplate = function (template, callback) {
        $.ajax({
            'type': 'get',
            'contentType': 'text/plain',
            'cache': false,
            'url': '/resources/js/templates/' + template + '.mst',
            'dataType': 'html',
            'timeout': 50000
        }).done(function (template) {
            callback(template);
        }).fail(function (error) {
            console.log('--------------------------------------');
            console.info('ERROR: requestTemplate');
            console.log(error);
            console.log('--------------------------------------');
        });
    };


    return {
        'initialize': initialize
    };
});