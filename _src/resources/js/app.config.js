'use strict';

/**
 * RequireJS configuration.
 *
 */
require.config({
    'baseUrl': '/resources/js/',
    'paths': {
        'jquery': 'vendor/jquery.min',
        'velocity': 'plugins/velocity.min',
        'jquery.validate': 'plugins/jquery.validate',
        'mustache': 'plugins/mustache',
        'flexslider' : 'plugins/flexslider'
    },
    'shim': {
        'velocity': {
            'deps': ['jquery']
        },
        'velocity.ui': {
            'deps': ['velocity']
        },
        'jquery.validate': {
            'deps': ['jquery']
        }
    }
});

define([], function () {
    /**
     * Application initialization.
     *
     * @param {object} application | Main application module.
     * @returns {object}
     *
     */
    require(['application'], function (application) {
        application.initialize();
    });
});