'use strict';

/**
 * Careers module.
 *
 * @param {function} $ | jQuery library.
 * @param {object} $ | Mustache plugin.
 *
 * @returns {object}
 *
 */

define([
    'jquery',
    'mustache'
], function ($, Mustache) {
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var initialize = function () {
        $('#locationCareers').change(function (event) {
            if ($(this).val()) {
                // fancy stuff...
                getCareers($(this).val(), function (careerData) {
                    // fancy stuff...
                    if ($.isEmptyObject(careerData)) {
                        console.info('--- no open positions ---');
                    } else {
                        var templateData = {
                            'positions': []
                        };

                        $.each(careerData.data, function (key, val) {
                            var career = {
                                'title': val.title,
                                'summary': val.summary,
                                'url': val.url
                            };

                            templateData.positions.push(career);
                        });

                        requestTemplate('careers', function (template) {
                            var html = $(template).filter('#careerTpl').html();
                            $('#openPositions').html(Mustache.render(html, templateData));
                        });
                    }
                });
            } else {
                // No location was chosen so update UI accordingly.
            }
        });
    };

    /**
     * Makes the request to get a mustache tempalte.
     *
     * @param {string} template | The template name (filename).
     * @param {object} callback | Callback method (market data).
     * @returns {object}
     *
     */
    var requestTemplate = function (template, callback) {
        $.ajax({
            'type': 'get',
            'contentType': 'text/plain',
            'cache': false,
            'url': '/resources/js/templates/' + template + '.mst',
            'dataType': 'html',
            'timeout': 50000
        }).done(function (template) {
            callback(template);
        }).fail(function (error) {
            console.log('--------------------------------------');
            console.info('ERROR: requestTemplate');
            console.log(error);
            console.log('--------------------------------------');
        });
    };

    /**
     * Get the open positions for the given location.
     *
     * @param location
     * @param callback
     *
     */
    var getCareers = function (location, callback) {
        var data = {
            'location': location
        };

        data[window.csrfTokenName] = window.csrfTokenValue;

        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': data,
            'url': '/careers.json',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            callback(response);
        }).fail(function (error) {
            // Total fail.
        });
    };

    return {
        'initialize': initialize,
        'getCareers': getCareers
    };
});