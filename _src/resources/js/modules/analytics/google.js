'use strict';

/**
 * Google analytics.
 *
 * @returns {object}
 *
 */

define([], function () {
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var initialize = function () {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-21285481-1', 'auto');

        renderPageView();
    };

    /**
     * Register a page view.
     *
     * @returns {void}
     *
     */
    function renderPageView() {
        ga('send', 'pageview');
    }

    return {
        'initialize': initialize
    };
});