'use strict';

/**
 * Yahoo weather.
 *
 * @returns {object}
 *
 */

define([
    'jquery'
], function ($) {
    /**
     * Get the weather via Yahoo API for the given city & state.
     *
     * @param city
     * @param state
     * @param callback
     *
     */
    var getWeather = function (city, state, callback) {
        var data = {
            'city': city,
            'state': state
        };

        data[window.csrfTokenName] = window.csrfTokenValue;

        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': data,
            'url': 'https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' + data["city"] + ', ' + data["state"] + '")&format=json',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            callback(response);
        }).fail(function (error) {
            // Total fail.
        });
    };

    return {
        'getWeather': getWeather
    };
});