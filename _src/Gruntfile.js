module.exports = function (grunt) {
    grunt.initConfig({
        'pkg': grunt.file.readJSON('package.json'),
        'concat': {
            'css': {
                'src': [
                    'resources/css/*.css'
                ],
                'dest': '../web/resources/css/production.css'
            }
        },
        'imagemin': {
            'dynamic': {
                'files': [{
                    'expand': true,
                    'cwd': '../web/resources/img/',
                    'src': ['*.{png,jpg,gif}'],
                    'dest': '../web/resources/img/'
                }]
            }
        },
        'sass': {
            'dist': {
                'src': [
                    'resources/sass/base.scss'
                ],
                'dest': '../web/resources/css/base.css'

            }
        },
        'watch': {
            'options': {
                'livereload': true,
            },
            'css': {
                'files': ['resources/sass/*.scss'],
                'tasks': ['sass'],
                'options': {
                    'spawn': false
                }
            },
            'html': {
                'files': ['../web/*.html', '../web/resources/css/*.css'],
                'options': {
                    livereload: true
                }
            },
            'images': {
                'files': ['../web/resources/img/*.{png,jpg,gif}', '../web/resources/img/*.{png,jpg,gif}'],
                'tasks': ['imagemin'],
                'options': {
                    'spawn': false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['concat', 'imagemin', 'sass', 'watch']);
};