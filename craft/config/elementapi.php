<?php

namespace Craft;

$slug = craft()->request->getRequiredPost('location');

$criteria = craft()->elements->getCriteria(ElementType::Category);
$criteria->group = 'Locations';
$criteria->slug = $slug;

$locations = $category = $criteria->find();

return [
    'endpoints' => [
        'careers.json' => [
            'elementType' => 'Entry',
            'criteria' => [
                'section' => 'career',
                'relatedTo' => ['targetElement' => $locations],
            ],
            'transformer' => function (EntryModel $entry) {
                return [
                    'title' => $entry->title,
                    'url' => $entry->url,
                    'summary' => (string)$entry->summary
                ];
            },
        ]
    ]
];