<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'cpTrigger' => 'backstage',
        'omitScriptNameInUrls' => true,
        'enableCsrfProtection' => true,
        'csrfTokenName' => 'CSRF'
    ),
    'local.frionaindustries.dev' => array(
        'devMode' => true,
        'siteUrl' => 'http://local.frionaindustries.dev/',
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => 'http://local.frionaindustries.dev/',
        )
    ),
    'frionaindustries.sandboxdsm.com' => array(
        'devMode' => true,
        'siteUrl' => 'http://frionaindustries.sandboxdsm.com/',
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => 'http://frionaindustries.sandboxdsm.com/'
        )
    ),
    'frionaindustries.com' => array(
        'siteUrl' => 'http://www.frionaindustries.com/',
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => 'http://www.frionaindustries.com/'
        )
    )
);
