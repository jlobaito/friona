

/**
 * Google maps module.
 *
 * @returns {object}
 *
 */

define([
    'plugins/async!https://maps.googleapis.com/maps/api/js?key=AIzaSyBnKm7kOwLHNZZS7PcVOPxyeL5_0mW2DNg'
], function () {
    var map = {};
    var mapOptions = {};
    var marker = {};
    var infoWindow = {};
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var initialize = function () {
        //mapOptions = {
        //    'center': new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
        //    'zoom': 17,
        //    'mapTypeId': google.maps.MapTypeId.ROADMAP
        //};
        //
        //renderMap();
        //renderMarker(position);
    };

    /**
     * Renders the map.
     *
     * @returns {void}
     *
     */
    function renderMap() {
        map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);
    }

    /**
     * Renders the map marker.
     *
     * @param {object} position | Position data.
     * @returns {void}
     *
     */
    function renderMarker(position) {
        var markerLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        marker = new google.maps.Marker({
            'position': markerLatLng,
            'map': map,
            'icon': '/assets/svg/marker.svg',
            'title': "TCF of Central Iowa"
        });

        infoWindow = new google.maps.InfoWindow({
            'disableAutoPan': true,
            'content': '<h4>TCF of Central Iowa</h4>'
            + '<p><strong>Hamilton\'s on Westown Parkway</strong><br>3601 Westown Pky West Des Moines, IA 50266</p>'
            + '<i class="fa fa-phone-square"></i> <a href="tel:+15152240078">(515) 224-0078</a>'
        });

        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(map, marker);
        });

        google.maps.event.addListener(map, 'idle', function () {
            infoWindow.open(map, marker);
        });
    }

    return {
        'initialize': initialize
    };
});