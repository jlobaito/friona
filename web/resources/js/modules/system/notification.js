

/**
 * Messages/notification module.
 * A single place for all application user notifications.
 *
 * @returns {object}
 *
 */
define([], function () {
    return {
        'firstNameRequired': '<i class="fa fa-exclamation-triangle"></i> First Name is required.',
        'lastNameRequired': '<i class="fa fa-exclamation-triangle"></i> Last Name is required.',
        'emailRequired': '<i class="fa fa-exclamation-triangle"></i> Email is required.',
        'emailInvalid': '<i class="fa fa-exclamation-triangle"></i> A valid email address is required.'
    }
});