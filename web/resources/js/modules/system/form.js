

/**
 * Base form module.
 *
 * @param {function} $ | jQuery
 * @returns {object}
 *
 */

define([
    'jquery'
], function ($) {
    /**
     * Renders feedback to the user for the given form element.
     *
     * @param {string} type | The type of feedback 'success' or 'error'.
     * @param {object} $field | The input element to update.
     * @param {string} message | The feedback message.
     * @returns {void}
     *
     */
    var renderFeedback = function (type, $field, message) {
        $field.next().remove();

        switch (type) {
            case 'error' :
                $field.after('<div class="has-error"><i class="fa fa-exclamation-triangle"></i> ' + message + '</div>');
                break;
            case 'success':
                $field.after('<div class="has-success"><i class="fa fa-exclamation-triangle"></i> ' + message + '</div>');
                break;
            default:
                break;
        }
    };

    /**
     * Clears the feedback for the given form element.
     *
     * @param {object} $field | The input element to update.
     * @returns {void}
     *
     */
    var clearFeedback = function ($field) {
        $field.next().remove();
    };

    return {
        'renderFeedback': renderFeedback,
        'clearFeedback': clearFeedback
    };
});