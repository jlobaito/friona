

/**
 * Application prefrences.
 * A single place for application prefrences/constants.
 *
 * @returns {object}
 *
 */
define([], function () {
    return {
        'duration': 200, // Default transition duration (miliseconds) for the velocity plugin.
        'location': { // Default geolocation.
            'defaults': true,
            'city': 'West Des Moines',
            'state': 'IA',
            'zip': '50131',
            'coords': {
                'latitude': 41.595246,
                'longitude': -93.754922
            },
            'country': {
                'longname': 'United States',
                'shortname': 'US'
            }
        }
    }
});